/* @f$Id: @ref diffusionExample "diffusionExample".cc 2012-08-30 14:00:00Z Dugan @f$ */
/* Author: Kevin Dugan, Texas A&M University, 2012 */

/*    @f$Id: @ref diffusionExample "diffusionExample".cc 2012-08-30 14:00:00Z Dugan @f$       */
/*                                                                */
/*    Copyright (C) 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011 by the deal.II authors */
/*                                                                */
/*    This file is subject to QPL and may not be  distributed     */
/*    without copyright and license information. Please refer     */
/*    to the file deal.II/doc/license.html for the  text  and     */
/*    further information on this license.                        */


#include <deal.II/grid/tria.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/fe/fe_q.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/numerics/vectors.h>
#include <deal.II/numerics/matrices.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/compressed_sparsity_pattern.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/numerics/data_out.h>
#include <fstream>
#include <iostream>

#include <deal.II/base/logstream.h>

using namespace dealii;


template <int dim, int ord>
class diffusionExample 
{
  public:
    diffusionExample ();
    ~diffusionExample ();
    void run ();
    
  private:
    void make_grid ();
    void setup_system ();
    void assemble_system ();
    void solve ();
    void refine_grid ();
    void output_results (const unsigned int cycle) const;

    Triangulation<dim>   triangulation;
    FE_Q<dim>            fe;
    DoFHandler<dim>      dof_handler;
    ConstraintMatrix     hanging_node_constraints;

    SparsityPattern      sparsity_pattern;
    SparseMatrix<double> system_matrix;

    Vector<double>       solution;
    Vector<double>       system_rhs;
};

template <int dim>
class RightHandSide : public Function<dim> 
{
  public:
    RightHandSide () : Function<dim>() {}
    
    virtual double value () const;
};


template <int dim>
double RightHandSide<dim>::value () const 
{
  double return_value = 1;

  return return_value;
}

template <int dim>
class Coefficient : public Function<dim>
{
  public:
    Coefficient () : Function<dim>() {}

    virtual double Diffusion_value (const Point<dim>   &p,
			  const unsigned int  component = 0) const;

    virtual void Diffusion_value_list (const std::vector<Point<dim> > &points,
			     std::vector<double>            &values,
			     const unsigned int              component = 0) const;

    virtual double Absorption_value (const Point<dim>   &p,
			  const unsigned int  component = 0) const;

    virtual void Absorption_value_list (const std::vector<Point<dim> > &points,
			     std::vector<double>            &values,
			     const unsigned int              component = 0) const;
};



template <int dim>
double Coefficient<dim>::Diffusion_value (const Point<dim> &p,
				const unsigned int) const
{
  double D = 1;

  if (p.square() < 0.5*0.5)
    return D;
  else
    return D;
}

template <int dim>
void Coefficient<dim>::Diffusion_value_list (const std::vector<Point<dim> > &points,
				   std::vector<double>            &values,
				   const unsigned int              component) const
{
  double D = 1;
  const unsigned int n_points = points.size();

  Assert (values.size() == n_points,
	  ExcDimensionMismatch (values.size(), n_points));

  Assert (component == 0,
	  ExcIndexRange (component, 0, 1));

  for (unsigned int i=0; i<n_points; ++i)
    {
      if (points[i].square() < 0.5*0.5)
	values[i] = D;
      else
	values[i] = D;
    }
}

template <int dim>
double Coefficient<dim>::Absorption_value (const Point<dim> &p,
				const unsigned int) const
{
  double Sa = 1;

  if (p.square() < 0.5*0.5)
    return Sa;
  else
    return Sa;
}

template <int dim>
void Coefficient<dim>::Absorption_value_list (const std::vector<Point<dim> > &points,
				   std::vector<double>            &values,
				   const unsigned int              component) const
{
  double Sa = 2;
  const unsigned int n_points = points.size();

  Assert (values.size() == n_points,
	  ExcDimensionMismatch (values.size(), n_points));

  Assert (component == 0,
	  ExcIndexRange (component, 0, 1));

  for (unsigned int i=0; i<n_points; ++i)
    {
      if (points[i].square() < 0.5*0.5)
	values[i] = Sa;
      else
	values[i] = Sa;
    }
}

template <int dim, int ord>
diffusionExample<dim,ord>::diffusionExample ()
                :
                fe (ord),
                dof_handler (triangulation)
{}

template <int dim, int ord>
diffusionExample<dim,ord>::~diffusionExample ()
{
  dof_handler.clear ();
}

template <int dim, int ord>
void diffusionExample<dim,ord>::make_grid ()
{
  GridGenerator::hyper_cube (triangulation, -10, 10);
  // Reflective Boundary Conditions
  triangulation.begin_active()->face(0)->set_boundary_indicator(1);
  triangulation.begin_active()->face(3)->set_boundary_indicator(1);
  triangulation.refine_global (1);

}

template <int dim, int ord>
void diffusionExample<dim,ord>::setup_system ()
{
  dof_handler.distribute_dofs (fe);

  solution.reinit (dof_handler.n_dofs());
  system_rhs.reinit (dof_handler.n_dofs());

  hanging_node_constraints.clear ();
  DoFTools::make_hanging_node_constraints (dof_handler,
                                           hanging_node_constraints);
  hanging_node_constraints.close ();

  CompressedSparsityPattern c_sparsity(dof_handler.n_dofs());
  DoFTools::make_sparsity_pattern (dof_handler, c_sparsity);
  hanging_node_constraints.condense (c_sparsity);
  sparsity_pattern.copy_from(c_sparsity);
  
  system_matrix.reinit (sparsity_pattern);
}



template <int dim, int ord>
void diffusionExample<dim,ord>::assemble_system () 
{  
  QGauss<dim>  quadrature_formula(ord+1);

  const RightHandSide<dim> right_hand_side;

  FEValues<dim> fe_values (fe, quadrature_formula, 
                           update_values   | update_gradients |
                           update_quadrature_points | update_JxW_values);

  const unsigned int   dofs_per_cell = fe.dofs_per_cell;
  const unsigned int   n_q_points    = quadrature_formula.size();

  FullMatrix<double>   cell_matrix (dofs_per_cell, dofs_per_cell);
  Vector<double>       cell_rhs (dofs_per_cell);

  std::vector<unsigned int> local_dof_indices (dofs_per_cell);

  const Coefficient<dim> D_coefficient;
  std::vector<double>    D_coefficient_values (n_q_points);
  const Coefficient<dim> Sa_coefficient;
  std::vector<double>    Sa_coefficient_values (n_q_points);

  typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler.begin_active(),
    endc = dof_handler.end();
  
  for (; cell!=endc; ++cell)
    {
      fe_values.reinit (cell);
      cell_matrix = 0;
      cell_rhs = 0;

      D_coefficient.Diffusion_value_list (fe_values.get_quadrature_points(),
			      D_coefficient_values);
      Sa_coefficient.Absorption_value_list (fe_values.get_quadrature_points(),
			      Sa_coefficient_values);

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point)
        for (unsigned int i=0; i<dofs_per_cell; ++i)
          {
            for (unsigned int j=0; j<dofs_per_cell; ++j)
              cell_matrix(i,j) += ((D_coefficient_values[q_point] *
                                   fe_values.shape_grad (i, q_point) *
                                   fe_values.shape_grad (j, q_point) +
                                   Sa_coefficient_values[q_point] *
                                   fe_values.shape_value (i, q_point) *
                                   fe_values.shape_value (j, q_point)) *
                                   fe_values.JxW (q_point));

            cell_rhs(i) += (fe_values.shape_value (i, q_point) *
                            right_hand_side.value () *
                            fe_values.JxW (q_point));
          }
      
      cell->get_dof_indices (local_dof_indices);
      for (unsigned int i=0; i<dofs_per_cell; ++i)
        {
          for (unsigned int j=0; j<dofs_per_cell; ++j)
            system_matrix.add (local_dof_indices[i],
                               local_dof_indices[j],
                               cell_matrix(i,j));
          
          system_rhs(local_dof_indices[i]) += cell_rhs(i);
        }
    }
  hanging_node_constraints.condense (system_matrix);
  hanging_node_constraints.condense (system_rhs);

  
  std::map<unsigned int,double> boundary_values;
  VectorTools::interpolate_boundary_values (dof_handler,
                                            0,
                                            ZeroFunction<dim>(),
                                            boundary_values);
  MatrixTools::apply_boundary_values (boundary_values,
                                      system_matrix,
                                      solution,
                                      system_rhs);
}



template <int dim, int ord>
void diffusionExample<dim,ord>::solve () 
{
  SolverControl           solver_control (1000, 1e-12);
  SolverCG<>              solver (solver_control);

  PreconditionSSOR<> preconditioner;
  preconditioner.initialize(system_matrix, 1.2);
  solver.solve (system_matrix, solution, system_rhs,
                preconditioner);

  hanging_node_constraints.distribute (solution);
}

template <int dim, int ord>
void diffusionExample<dim,ord>::refine_grid ()
{
  Vector<float> estimated_error_per_cell (triangulation.n_active_cells());

  KellyErrorEstimator<dim>::estimate (dof_handler,
                                      QGauss<dim-1>(ord+1),
                                      typename FunctionMap<dim>::type(),
                                      solution,
                                      estimated_error_per_cell);
  double total_error = estimated_error_per_cell.l2_norm();
  std::cout << "\tTotal Error: " << total_error << std::endl;

  GridRefinement::refine_and_coarsen_fixed_number (triangulation,
                                                   estimated_error_per_cell,
                                                   0.3, 0.03);
  triangulation.execute_coarsening_and_refinement ();
}

template <int dim, int ord>
void diffusionExample<dim,ord>::output_results (const unsigned int cycle) const
{
  Assert (cycle < 100, ExcNotImplemented());

  std::string filename = "grid-";
  if(cycle < 10)
    filename += "0";
  std::stringstream ss;
  ss << cycle;
  filename += ss.str();
  filename += ".eps";

  std::ofstream output (filename.c_str());

  GridOut grid_out;
  grid_out.write_eps (triangulation, output);
}




template <int dim, int ord>
void diffusionExample<dim,ord>::run () 
{
  for(unsigned int cycle = 0; cycle < 10; ++cycle){
    std::cout << "Cycle " << cycle << ":" << std::endl;
    if(cycle == 0){
      make_grid ();
    } else
      refine_grid ();
 
    std::cout << "\tNumber of active cells:\t"
              << triangulation.n_active_cells() << std::endl;
    setup_system ();

    std::cout << "\tNumber of degrees of freedom:\t"
              << dof_handler.n_dofs() << std::endl;

    assemble_system ();
    solve ();
    output_results (cycle);
  }

  DataOutBase::EpsFlags eps_flags;
  eps_flags.z_scaling = 4;

  DataOut<dim> data_out;
  data_out.set_flags (eps_flags);

  data_out.attach_dof_handler (dof_handler);
  data_out.add_data_vector (solution, "solution");
  data_out.build_patches ();

  std::ofstream output ("final-solution.gpl");
  data_out.write_gnuplot (output);
}



int main () 
{
  try
    {
      deallog.depth_console (0);

      diffusionExample<2,3> laplace_problem_2d;
      laplace_problem_2d.run ();
    }
  catch (std::exception &exc)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Exception on processing: " << std::endl
                << exc.what() << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;

      return 1;
    }
  catch (...)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Unknown exception!" << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
      return 1;
    }

  return 0;
}

