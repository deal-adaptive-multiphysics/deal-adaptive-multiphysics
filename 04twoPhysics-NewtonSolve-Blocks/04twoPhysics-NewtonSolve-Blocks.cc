/* @f$Id: @ref twoPhysics "twoPhysics".cc 2012-08-30 14:00:00Z Dugan @f$ */
/* Author: Kevin Dugan, Texas A&M University, 2012 */

/*    @f$Id: @ref twoPhysics "twoPhysics".cc 2012-08-30 14:00:00Z Dugan @f$       */
/*                                                                */
/*
    This file uses the DealII FEM library to solve a coupled neutronics and
    non-linear heat conduction problem. The problem domain is the region
    bound by [-10, 10]^n where n is the dimensions number (1, 2 or 3). The
    code builds the non-linear residual for use in Newton's Method.

    In this problem the two physics components are stored in a sparse matrix
    with each physic stored in a single block component of the larger matrix.
    GMRes is used for the linear Newton solve with Jacobi as a potential
    preconditioner.
*/

#include <deal.II/grid/tria.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/grid/grid_out.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/dofs/dof_renumbering.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_system.h>

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/logstream.h>

#include <deal.II/numerics/vectors.h>
#include <deal.II/numerics/matrices.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/error_estimator.h>

#include <deal.II/lac/block_vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/block_sparse_matrix.h>
#include <deal.II/lac/compressed_sparsity_pattern.h>
#include <deal.II/lac/solver_gmres.h>
#include <deal.II/lac/precondition.h>

#include <fstream>
#include <iostream>

namespace BlockPhysics {
using namespace dealii;


template <int dim, int ord>
class twoPhysics
{
  public:
    twoPhysics ();
    void run ();


  private:
    void make_grid ();
    void setup_system ();
    void assemble_system ();
    void linear_solve ();
    void newton_solve ();
    void refine_grid ();
    void output_results () const;
    void output_grid (const unsigned int cycle) const;
    double determine_step_length ();

    Triangulation<dim>     triangulation;
    FESystem<dim>          fe;
    DoFHandler<dim>        dof_handler;
    ConstraintMatrix       constraints;

    BlockSparsityPattern      sparsity_pattern;
    BlockSparseMatrix<double> system_matrix;

    BlockVector<double>       solution;
    BlockVector<double>       newton_update;
    BlockVector<double>       system_rhs;

    double Diffusion_Constant;
    double Absorption_Constant;
    double ThermalCond_Constant;
    double residual_norm;
    double update_norm;
};

template <int dim, int ord>
twoPhysics<dim, ord>::twoPhysics ()
                :
                fe (FE_Q<dim>(ord), 2),
                dof_handler (triangulation)
{}

// Set up the domain depending on the dimension <dim>
template <int dim, int ord>
void twoPhysics<dim, ord>::make_grid ()
{
  GridGenerator::hyper_cube (triangulation, -10, 10);
  triangulation.refine_global (4);
  std::cout << "\tNumber of active cells: "
            << triangulation.n_active_cells()
            << std::endl;
  std::cout << "\tTotal number of cells: "
            << triangulation.n_cells()
            << std::endl;
}

// Allocate memory for the sparse matrix and solution vectors. Each physic
// is stored in a block of the matrix.
template <int dim, int ord>
void twoPhysics<dim, ord>::setup_system ()
{
  system_matrix.clear ();

  dof_handler.distribute_dofs (fe);
  std::cout << "\tNumber of degrees of freedom: "
            << dof_handler.n_dofs()
            << std::endl;

  DoFRenumbering::component_wise (dof_handler);

  std::vector<unsigned int> dofs_per_component (2);
  DoFTools::count_dofs_per_component (dof_handler, dofs_per_component);
  const unsigned int n_phi =  dofs_per_component[0],
                     n_temp = dofs_per_component[1];

  solution.reinit (2);
  solution.block(0).reinit(n_phi);
  solution.block(1).reinit(n_temp);
  solution.collect_sizes();
  system_rhs.reinit(2);
  system_rhs.block(0).reinit(n_phi);
  system_rhs.block(1).reinit(n_temp);
  system_rhs.collect_sizes();
  newton_update.reinit(2);
  newton_update.block(0).reinit(n_phi);
  newton_update.block(1).reinit(n_temp);
  newton_update.collect_sizes();
  
  constraints.clear ();
  DoFTools::make_hanging_node_constraints (dof_handler,
                                           constraints);
  constraints.close ();

  {
    BlockCompressedSimpleSparsityPattern c_sparsity(2,2);
    c_sparsity.block(0,0).reinit(n_phi , n_phi );
    c_sparsity.block(0,1).reinit(n_phi , n_temp);
    c_sparsity.block(1,0).reinit(n_temp, n_phi );
    c_sparsity.block(1,1).reinit(n_temp, n_temp);
    c_sparsity.collect_sizes();

    DoFTools::make_sparsity_pattern (dof_handler, c_sparsity);
    constraints.condense (c_sparsity);
    sparsity_pattern.copy_from(c_sparsity);
  }

  system_matrix.reinit (sparsity_pattern);

  std::ofstream out("sparsity_pattern.gpl");
  sparsity_pattern.print_gnuplot (out);
}


template <int dim, int ord>
void twoPhysics<dim, ord>::assemble_system ()
{

  // Problem parameters
  double Diffusion_Constant = 2;
  double Absorption_Constant = 3;
  double ThermalDiff_Constant = 0.1;
  double Flux_Source = 30;
  double Thermal_Source = 3;

  system_matrix = 0;
  system_rhs = 0;

  QGauss<dim>  quadrature_formula(ord+1);
  FEValues<dim> fe_values (fe, quadrature_formula,
                         update_values | update_gradients | update_JxW_values);

  const unsigned int   dofs_per_cell = fe.dofs_per_cell;
  const unsigned int   n_q_points    = quadrature_formula.size();

  FullMatrix<double>   cell_matrix (dofs_per_cell, dofs_per_cell);
  Vector<double>       cell_rhs (dofs_per_cell);

  std::vector<unsigned int> local_dof_indices (dofs_per_cell);
  std::vector<double > phi_old_solution_values(n_q_points);
  std::vector<double > temp_old_solution_values(n_q_points);
  std::vector<Tensor<1,dim> > phi_old_solution_gradients(n_q_points);
  std::vector<Tensor<1,dim> > temp_old_solution_gradients(n_q_points);

  // Flux and temperature FE extractors
  const FEValuesExtractors::Scalar phi(0);
  const FEValuesExtractors::Scalar temp(1);

  typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler.begin_active(),
    endc = dof_handler.end();
  for (; cell!=endc; ++cell)
    {
      fe_values.reinit (cell);

      // Storing FE data from previous Newton iteration.
      fe_values[phi].get_function_values(solution, phi_old_solution_values);
      fe_values[temp].get_function_values(solution, temp_old_solution_values);
      fe_values[phi].get_function_gradients(solution, phi_old_solution_gradients);
      fe_values[temp].get_function_gradients(solution, temp_old_solution_gradients);

      cell_matrix = 0;
      cell_rhs = 0;

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
        // Non-linear thermal conductivity.
        double ThermalCond_Constant = 5;// + 2150/(200+ temp_old_solution_values[q_point]);
        for (unsigned int i=0; i<dofs_per_cell; ++i){
          for (unsigned int j=0; j<dofs_per_cell; ++j)
            cell_matrix(i,j) += ((Diffusion_Constant *
                                  fe_values[phi].gradient (i, q_point) *
                                  fe_values[phi].gradient (j, q_point) +
                                  Absorption_Constant *
                                  fe_values[phi].value(i, q_point) *
                                  fe_values[phi].value(j, q_point) -
                                  ThermalDiff_Constant *
                                  fe_values[temp].value(i, q_point) *
                                  fe_values[phi].value(j, q_point) +
                                  ThermalCond_Constant *
                                  fe_values[temp].gradient (i, q_point) *
                                  fe_values[temp].gradient (j, q_point)) *
                                  fe_values.JxW (q_point));
/*
          cell_rhs(i) -= ((Diffusion_Constant *
                          fe_values[phi].gradient (i, q_point) *
                          phi_old_solution_gradients[q_point] +
                          Absorption_Constant *
                          fe_values[phi].value (i, q_point) *
                          phi_old_solution_values[q_point] -
                          fe_values[phi].value (i, q_point) *
                          Flux_Source -
                          ThermalDiff_Constant *
                          fe_values[temp].value (i, q_point) *
                          phi_old_solution_values[q_point] +
                          ThermalCond_Constant *
                          fe_values[temp].gradient (i, q_point) *
                          temp_old_solution_gradients[q_point] -
                          fe_values[temp].value (i, q_point) *
                          Thermal_Source) *
                          fe_values.JxW (q_point));
*/
          cell_rhs(i) -= ((fe_values[phi].value (i, q_point) *
                          Flux_Source +
                          fe_values[temp].value (i, q_point) *
                          Thermal_Source) *
                          fe_values.JxW (q_point));
        }
      }
      cell->get_dof_indices (local_dof_indices);

      for (unsigned int i=0; i<dofs_per_cell; ++i)
        for (unsigned int j=0; j<dofs_per_cell; ++j)
          system_matrix.add (local_dof_indices[i],
                             local_dof_indices[j],
                             cell_matrix(i,j));

      for (unsigned int i=0; i<dofs_per_cell; ++i)
        system_rhs(local_dof_indices[i]) += cell_rhs(i);
    }

  system_matrix.vmult_add(system_rhs, solution);
  for(unsigned int i=0; i<system_rhs.size(); i++){
    system_rhs(i) = -system_rhs(i);
  }

  constraints.condense (system_matrix);
  constraints.condense (system_rhs);

  // Applying bouncary conditions
  std::map<unsigned int,double> boundary_values;
  VectorTools::interpolate_boundary_values (dof_handler,
                                            0,
                                            ZeroFunction<dim>(2),
                                            boundary_values);
  VectorTools::interpolate_boundary_values (dof_handler,
                                            1,
                                            ZeroFunction<dim>(2),
                                            boundary_values);
  MatrixTools::apply_boundary_values (boundary_values,
                                      system_matrix,
                                      solution,
                                      system_rhs);

  // Calculate Norm of the non-linear residual.
  residual_norm = system_rhs.l2_norm ();
}

// Linear Solver setup.
template <int dim, int ord>
void twoPhysics<dim, ord>::linear_solve ()
{
  SolverControl                       solver_control (10000, 1e-12);
  SolverGMRES<BlockVector<double> >   solver (solver_control);
//  PreconditionIdentity                preconditioner;
  PreconditionJacobi<BlockSparseMatrix<double> >
                                      preconditioner;
  preconditioner.initialize(system_matrix);

  solver.solve (system_matrix,
                newton_update,
                system_rhs,
                preconditioner);
  std::cout << "\t"
            << solver_control.last_step()
            << " GMRES iterations needed to obtain convergence."
            << std::endl;
  
  constraints.distribute (newton_update);

  double alpha = determine_step_length();
  solution.add (alpha, newton_update);
  update_norm = alpha*newton_update.l2_norm();
}

// Newton Solver
template <int dim, int ord>
void twoPhysics<dim, ord>::newton_solve()
{
  int iter = 1;
  do{
      std::cout << "   Newton Iteration " << iter << std::endl;
      assemble_system ();
      linear_solve ();
      std::cout << "\tResidual Norm: " << residual_norm << std::endl;
      std::cout << "\tUpdate Norm: " << update_norm << std::endl;
      iter++ ;
  } while (residual_norm > 1E-10 && update_norm > 1E-10);
}

// Damping coefficient for Newton's Method
template <int dim, int ord>
double twoPhysics<dim, ord>::determine_step_length()
{
  return 1;
}

// h-AMR
template <int dim, int ord>
void twoPhysics<dim, ord>::refine_grid ()
{
  Vector<float> estimated_error_per_cell (triangulation.n_active_cells());
  std::vector<bool> component_mask;
  component_mask.push_back(true);
  component_mask.push_back(false);
  KellyErrorEstimator<dim>::estimate (dof_handler,
                                      QGauss<dim-1>(ord+1),
                                      typename FunctionMap<dim>::type(),
                                      solution,
                                      estimated_error_per_cell,
                                      component_mask);
  GridRefinement::refine_and_coarsen_fixed_number (triangulation,
                                                   estimated_error_per_cell,
                                                   0.3, 0.03);
  triangulation.execute_coarsening_and_refinement ();
  std::cout << "\tNumber of active cells: "
            << triangulation.n_active_cells()
            << std::endl;
  std::cout << "\tTotal number of cells: "
            << triangulation.n_cells()
            << std::endl;
}

// Display solution in gnuplot data file
template <int dim, int ord>
void twoPhysics<dim, ord>::output_results () const
{
  std::vector<std::string> solution_names;
  solution_names.push_back("Flux");
  solution_names.push_back("Temp");

  std::vector<DataComponentInterpretation::DataComponentInterpretation>
    data_component_interpretation;
  data_component_interpretation.push_back(DataComponentInterpretation::component_is_scalar);
  data_component_interpretation.push_back(DataComponentInterpretation::component_is_scalar);

  DataOut<dim> data_out;
  data_out.attach_dof_handler (dof_handler);
  data_out.add_data_vector (solution, solution_names,
                            DataOut<dim>::type_dof_data,
                            data_component_interpretation);
  data_out.build_patches ();

  std::ofstream output ("solution.gpl");
  data_out.write_gnuplot (output);  
}

// Output adapted grid if in 2D
template <int dim, int ord>
void twoPhysics<dim, ord>::output_grid (const unsigned int cycle) const
{
  Assert (cycle < 100, ExcNotImplemented());

  std::string filename = "grid-";
  if(cycle < 10)
    filename += "0";
  std::stringstream ss;
  ss << cycle;
  filename += ss.str();
  filename += ".eps";

  std::ofstream output (filename.c_str());

  GridOut grid_out;
  grid_out.write_eps (triangulation, output);
}

// Problem driver
template <int dim, int ord>
void twoPhysics<dim, ord>::run ()
{
  for(unsigned int cycle = 0; cycle < 1; ++cycle){
    std::cout << "Cycle: " << cycle << std::endl;
    if(cycle == 0){
      make_grid ();
    } else{
      refine_grid ();
    }
    setup_system();
    newton_solve ();
    if(dim != 1){
      output_grid (cycle);
    }
  }
  output_results ();
}
} // End namespace


int main ()
{
  try{
    using namespace BlockPhysics;
    const int dimension = 1;
    const int polyOrder = 1;

    deallog.depth_console (0);

    twoPhysics<dimension,polyOrder> laplace_problem;
    laplace_problem.run ();
  } catch (std::exception &exc){
    std::cerr << std::endl << std::endl
              << "----------------------------------------------------"
              << std::endl;
    std::cerr << "Exception on processing: " << std::endl
              << exc.what() << std::endl
              << "Aborting!" << std::endl
              << "----------------------------------------------------"
              << std::endl;

    return 1;
  } catch (...){
    std::cerr << std::endl << std::endl
              << "----------------------------------------------------"
              << std::endl;
    std::cerr << "Unknown exception!" << std::endl
              << "Aborting!" << std::endl
              << "----------------------------------------------------"
              << std::endl;
    return 1;
  }
  

  return 0;
}

