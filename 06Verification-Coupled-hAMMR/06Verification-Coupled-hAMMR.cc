/* @f$Id: @ref twoPhysics "twoPhysics".cc 2012-08-30 14:00:00Z Dugan @f$ */
/* Author: Kevin Dugan, Texas A&M University, 2012 */

/*    @f$Id: @ref twoPhysics "twoPhysics".cc 2012-08-30 14:00:00Z Dugan @f$       */
/*                                                                */
/*
    This file uses the DealII FEM library to solve a coupled neutronics and
    non-linear heat conduction problem. The problem domain is the region
    bound by [-10, 10]^n where n is the dimensions number (1, 2 or 3). The
    code builds the non-linear residual for use in Newton's Method.

    In this problem the two physics components are stored in a sparse matrix
    with each physic stored in a single block component of the larger matrix.
    GMRes is used for the linear Newton solve with Jacobi as a potential
    preconditioner.
*/

#include <deal.II/grid/tria.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/tria_boundary_lib.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_tools.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/dofs/dof_renumbering.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_system.h>

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/convergence_table.h>

#include <deal.II/numerics/vectors.h>
#include <deal.II/numerics/matrices.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/numerics/vector_tools.h>

#include <deal.II/lac/block_vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/block_sparse_matrix.h>
#include <deal.II/lac/compressed_sparsity_pattern.h>
#include <deal.II/lac/solver_gmres.h>
#include <deal.II/lac/precondition.h>

#include <fstream>
#include <iostream>

namespace BlockPhysics {
using namespace dealii;

template <int dim>
class SolutionBase
{
  protected:
    static const double C_phi  = 2.5;
    static const double C_T    = 6;
    static const double D      = 2;
    static const double Sa     = 3;
    static const double FluxS  = 30;
    static const double HeatQ  = 3;
    static const double k0     = 5;
    static const double k1     = 2049.59; // Set to zero if linear problem.
    static const double k2     = 200;
    static const double kappa  = 0.1;
    static const double L      = 20;
};

template <int dim, int ord>
class twoPhysics : protected SolutionBase<dim>
{
  public:
    twoPhysics ();
    void run ();


  private:
    void make_grid ();
    void setup_system ();
    void init_system ();
    void assemble_system ();
    void assemble_off_diagonal (const typename DoFHandler<dim>::cell_iterator &physic1,
                                const typename DoFHandler<dim>::cell_iterator &physic2,
                                const FullMatrix<double> prolongation_matrix);
    void linear_solve ();
    void newton_solve ();
    void set_boundary_values ();
    void refine_grid ();
    void output_results () const;
    void output_grid (const unsigned int cycle) const;
    void print_grid_info ();
    void process_solution (const unsigned int cycle);
    double determine_step_length ();

    Triangulation<dim>     coarse_mesh;
    Triangulation<dim>     triangulation1;
    FE_Q<dim>              fe1;
    DoFHandler<dim>        dof_handler1;
    ConstraintMatrix       constraints1;
    Triangulation<dim>     triangulation2;
    FE_Q<dim>              fe2;
    DoFHandler<dim>        dof_handler2;
    ConstraintMatrix       constraints2;

    BlockSparsityPattern      sparsity_pattern;
    BlockSparseMatrix<double> system_matrix;

    BlockVector<double>       solution;
    BlockVector<double>       newton_update;
    BlockVector<double>       system_rhs;

    ConvergenceTable          convergence_tablePHI;
    ConvergenceTable          convergence_tableT;

    double residual_norm;
    double update_norm;
};

//===============================================================================
//===============================================================================
//===============================================================================
// MMS for PHI
template <int dim>
class SolutionPhi : public Function<dim>,
                    protected SolutionBase<dim>
{
  public:
    SolutionPhi () : Function<dim>() {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;
    virtual Tensor<1,dim> gradient (const Point<dim>  &p,
                                    const unsigned int component = 0) const;
};

template <int dim>
double SolutionPhi<dim>::value (const Point<dim> &p,
                             const unsigned int) const
{
  double return_value = this->C_phi;

  for(unsigned int i=0; i<dim; i++){
    return_value *= sin(p[i] * M_PI / this->L);
  }

  return return_value;
}

template <int dim>
Tensor<1,dim> SolutionPhi<dim>::gradient (const Point<dim>  &p,
                                          const unsigned int) const
{
  Tensor<1,dim> return_value;
  for(unsigned int i=0; i<dim; i++){
    return_value[i] = this->C_phi * (M_PI / this->L);
    for(unsigned int j=0; j<dim; j++){
      if(j == i){
        return_value[i] *= cos(M_PI * p[j] / this->L);
      } else {
        return_value[i] *= sin(M_PI * p[j] / this->L);
      }
    }
  }

  return return_value;
}

template <int dim>
class RightHandSidePhi : public Function<dim>,
                         protected SolutionBase<dim>
{
  public:
    RightHandSidePhi () : Function<dim>() {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;
};

template <int dim>
double RightHandSidePhi<dim>::value (const Point<dim>  &p,
                                     const unsigned int) const
{
  double return_value1 = this->C_phi;
  double return_value2 = 0.0;

  for(unsigned int i=0; i<dim; i++){
    return_value1 *= sin(p[i] * M_PI / this->L);
  }

  return_value2 = this->D * (M_PI / this->L) * (M_PI / this->L) * dim +
                  this->Sa;

  return return_value1 * return_value2 - this->FluxS;
}

// MMS for T
template <int dim>
class SolutionT : public Function<dim>,
                  protected SolutionBase<dim>
{
  public:
    SolutionT () : Function<dim>() {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;
    virtual Tensor<1,dim> gradient (const Point<dim>  &p,
                                    const unsigned int component = 0) const;
};

template <int dim>
double SolutionT<dim>::value (const Point<dim> &p,
                              const unsigned int) const
{
  double return_value = this->C_T;

  for(unsigned int i=0; i<dim; i++){
    return_value *= sin(p[i] * M_PI / this->L);
  }

  return return_value;
}

template <int dim>
Tensor<1,dim> SolutionT<dim>::gradient (const Point<dim>  &p,
                                        const unsigned int) const
{
  Tensor<1,dim> return_value;
  for(unsigned int i=0; i<dim; i++){
    return_value[i] = this->C_T * (M_PI / this->L);
    for(unsigned int j=0; j<dim; j++){
      if(j == i){
        return_value[i] *= cos(M_PI * p[j] / this->L);
      } else {
        return_value[i] *= sin(M_PI * p[j] / this->L);
      }
    }
  }

  return return_value;
}

template <int dim>
class RightHandSideT : public Function<dim>,
                       protected SolutionBase<dim>
{
  public:
    RightHandSideT () : Function<dim>() {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;
};

template <int dim>
double RightHandSideT<dim>::value (const Point<dim>  &p,
                                     const unsigned int) const
{
  if(dim == 1){
    return -1.0* this->HeatQ - this->C_phi * this->kappa * sin((M_PI*p[0])/this->L) + this->C_T * 1/(this->L*this->L) * (M_PI*M_PI) * sin((M_PI*p[0])/this->L) *
            (this->k0 +    this->k1 / (this->k2 + this->C_T * sin((M_PI*p[0])/this->L))) + (this->C_T*this->C_T) * 1/(this->L*this->L) * (M_PI*M_PI) * this->k1 *
             std::pow(cos((M_PI*p[0])/this->L),2.0) * 1/std::pow(this->k2 + this->C_T * sin((M_PI*p[0])/this->L),2.0);
  } else if(dim == 2){
    return -1.0* this->HeatQ - this->C_phi * this->kappa * sin((M_PI*p[0])/this->L)*sin((M_PI*p[1])/this->L) + this->C_T*1/(this->L*this->L)*(M_PI*M_PI) * 
            sin((M_PI*p[0])/this->L)*sin((M_PI*p[1])/this->L) * (this->k0 + this->k1/(this->k2 + this->C_T * sin((M_PI*p[0])/this->L) * sin((M_PI*p[1])/this->L))) * 
            2.0 + (this->C_T*this->C_T)*1/(this->L*this->L)*(M_PI*M_PI) * this->k1 * std::pow(cos((M_PI*p[0])/this->L),2.0) * std::pow(sin((M_PI*p[1])/this->L),2.0) * 
            1/std::pow(this->k2 + this->C_T * sin((M_PI*p[0])/this->L) * sin((M_PI*p[1])/this->L),2.0) + (this->C_T*this->C_T)*1/(this->L*this->L)*(M_PI*M_PI) * 
            this->k1 * std::pow(cos((M_PI*p[1])/this->L),2.0) * std::pow(sin((M_PI*p[0])/this->L),2.0) * 1/std::pow(this->k2 + this->C_T * sin((M_PI*p[0])/this->L) * 
            sin((M_PI*p[1])/this->L),2.0);
  } else if(dim == 3){
    return -1.0* this->HeatQ - this->C_phi * this->kappa * sin((M_PI*p[0])/this->L) * sin((M_PI*p[1])/this->L) * sin((M_PI*p[2])/this->L) + this->C_T * 1/(this->L*this->L) *
            (M_PI*M_PI) * sin((M_PI*p[0])/this->L) * sin((M_PI*p[1])/this->L) * sin((M_PI*p[2])/this->L) * (this->k0 + this->k1/(this->k2 + this->C_T *
            sin((M_PI*p[0])/this->L) * sin((M_PI*p[1])/this->L) * sin((M_PI*p[2])/this->L))) * 3.0 + (this->C_T*this->C_T)*1/(this->L*this->L) * (M_PI*M_PI) * this->k1 * 
            std::pow(cos((M_PI*p[0])/this->L),2.0) * std::pow(sin((M_PI*p[1])/this->L),2.0) * std::pow(sin((M_PI*p[2])/this->L),2.0) * 1/std::pow(this->k2 + this->C_T * 
            sin((M_PI*p[0])/this->L) * sin((M_PI*p[1])/this->L) * sin((M_PI*p[2])/this->L),2.0) + (this->C_T*this->C_T) * 1/(this->L*this->L) * (M_PI*M_PI) * this->k1 * 
            std::pow(cos((M_PI*p[1])/this->L),2.0) * std::pow(sin((M_PI*p[0])/this->L),2.0) * std::pow(sin((M_PI*p[2])/this->L),2.0) * 1/std::pow(this->k2 + this->C_T * 
            sin((M_PI*p[0])/this->L) * sin((M_PI*p[1])/this->L) * sin((M_PI*p[2])/this->L),2.0) + (this->C_T*this->C_T) * 1/(this->L*this->L) * (M_PI*M_PI) * this->k1 * 
            std::pow(cos((M_PI*p[2])/this->L),2.0) * std::pow(sin((M_PI*p[0])/this->L),2.0) * std::pow(sin((M_PI*p[1])/this->L),2.0) * 1/std::pow(this->k2 + this->C_T * 
            sin((M_PI*p[0])/this->L) * sin((M_PI*p[1])/this->L) * sin((M_PI*p[2])/this->L),2.0);
  }
}
//===============================================================================
//===============================================================================
//===============================================================================

template <int dim>
class BoundaryValuesPhi : public Function<dim>
{
  public:
    BoundaryValuesPhi () : Function<dim>() {}

    virtual double value (const Point<dim> &p,
                          const unsigned int component = 0) const;
};

template <int dim>
double BoundaryValuesPhi<dim>::value (const Point<dim> &,
                                   const unsigned int /*component*/) const
{
  return 0.0;
}

template <int dim>
class BoundaryValuesT : public Function<dim>
{
  public:
    BoundaryValuesT () : Function<dim>() {}

    virtual double value (const Point<dim> &p,
                          const unsigned int component = 0) const;
};

template <int dim>
double BoundaryValuesT<dim>::value (const Point<dim> &,
                                   const unsigned int /*component*/) const
{
  return 0.0;
}

template <int dim, int ord>
twoPhysics<dim, ord>::twoPhysics ()
                :
                fe1 (ord),
                dof_handler1 (triangulation1),
                fe2 (ord),
                dof_handler2 (triangulation2)
{}

// Set up the domain depending on the dimension <dim>
template <int dim, int ord>
void twoPhysics<dim, ord>::make_grid ()
{
  GridGenerator::hyper_cube (coarse_mesh, 0, this->L);
//  coarse_mesh.begin_active()->face(0)->set_boundary_indicator(3);
//  coarse_mesh.begin_active()->face(3)->set_boundary_indicator(3);
  coarse_mesh.refine_global(2);

  triangulation1.copy_triangulation (coarse_mesh);
  triangulation2.copy_triangulation (coarse_mesh);

}

template <int dim, int ord>
void twoPhysics<dim, ord>::print_grid_info ()
{
  std::cout << "  Flux" << std::endl;
  std::cout << "    Number of active cells: "
            << triangulation1.n_active_cells()
            << std::endl;
  std::cout << "    Total number of cells: "
            << triangulation1.n_cells()
            << std::endl;
  std::cout << "    Number of degrees of freedom: "
            << dof_handler1.n_dofs()
            << std::endl;

  std::cout << "  Temp" << std::endl;
  std::cout << "    Number of active cells: "
            << triangulation2.n_active_cells()
            << std::endl;
  std::cout << "    Total number of cells: "
            << triangulation2.n_cells()
            << std::endl;
  std::cout << "    Number of degrees of freedom: "
            << dof_handler2.n_dofs()
            << std::endl;
}

// Allocate memory for the sparse matrix and solution vectors. Each physic
// is stored in a block of the matrix.
template <int dim, int ord>
void twoPhysics<dim, ord>::setup_system ()
{
  system_matrix.clear ();

  dof_handler1.distribute_dofs (fe1);
  dof_handler2.distribute_dofs (fe2);

  const unsigned int n_phi =  dof_handler1.n_dofs(),
                     n_temp = dof_handler2.n_dofs();

  solution.reinit (2);
  solution.block(0).reinit(n_phi);
  solution.block(1).reinit(n_temp);
  solution.collect_sizes();
  system_rhs.reinit(2);
  system_rhs.block(0).reinit(n_phi);
  system_rhs.block(1).reinit(n_temp);
  system_rhs.collect_sizes();
  newton_update.reinit(2);
  newton_update.block(0).reinit(n_phi);
  newton_update.block(1).reinit(n_temp);
  newton_update.collect_sizes();
  
  constraints1.clear ();
  DoFTools::make_hanging_node_constraints (dof_handler1,
                                           constraints1);
  constraints1.close ();
  constraints2.clear ();
  DoFTools::make_hanging_node_constraints (dof_handler2,
                                           constraints2);
  constraints2.close ();

  {
    BlockCompressedSimpleSparsityPattern c_sparsity(2,2);
    c_sparsity.block(0,0).reinit(n_phi , n_phi );
    c_sparsity.block(0,1).reinit(n_phi , n_temp);
    c_sparsity.block(1,0).reinit(n_temp, n_phi );
    c_sparsity.block(1,1).reinit(n_temp, n_temp);
    c_sparsity.collect_sizes();

    DoFTools::make_sparsity_pattern (dof_handler1, c_sparsity.block(0,0) );
    DoFTools::make_sparsity_pattern (dof_handler2, c_sparsity.block(1,1) );
    DoFTools::make_sparsity_pattern (dof_handler1, dof_handler2, c_sparsity.block(0,1) );
    DoFTools::make_sparsity_pattern (dof_handler2, dof_handler1, c_sparsity.block(1,0) );
    constraints1.condense (c_sparsity);
    constraints2.condense (c_sparsity);
    sparsity_pattern.copy_from(c_sparsity);
  }

  system_matrix.reinit (sparsity_pattern);

  std::ofstream out("sparsity_pattern.gpl");
  sparsity_pattern.print_gnuplot (out);

}

template <int dim, int ord>
void twoPhysics<dim, ord>::init_system()
{
  // Initialize Solution as BV
  BoundaryValuesPhi<dim> BVP;
  BoundaryValuesT  <dim> BVT;
  Point<dim> p;

  solution.block(0) = BVP.value(p);
  solution.block(1) = BVT.value(p);

}

template <int dim, int ord>
void twoPhysics<dim, ord>::assemble_system ()
{
  system_matrix = 0;
  system_rhs = 0;

  QGauss<dim>  quadrature_formula(ord+1);
  FEValues<dim> fe_values1 (fe1, quadrature_formula,
                         update_values | update_gradients |
                         update_quadrature_points | update_JxW_values);
  FEValues<dim> fe_values2 (fe2, quadrature_formula,
                         update_values | update_gradients | 
                         update_quadrature_points | update_JxW_values);

  const unsigned int   dofs_per_cell_1 = fe1.dofs_per_cell;
  const unsigned int   dofs_per_cell_2 = fe2.dofs_per_cell;
  const unsigned int   n_q_points    = quadrature_formula.size();

  FullMatrix<double>   cell_matrix1 (dofs_per_cell_1, dofs_per_cell_1);
  Vector<double>       cell_rhs1 (dofs_per_cell_1);
  FullMatrix<double>   cell_matrix2 (dofs_per_cell_2, dofs_per_cell_2);
  Vector<double>       cell_rhs2 (dofs_per_cell_2);

  std::vector<unsigned int> local_dof_indices_1 (dofs_per_cell_1);
  std::vector<unsigned int> local_dof_indices_2 (dofs_per_cell_2);
  std::vector<double > phi_old_solution_values(n_q_points);
  std::vector<double > temp_old_solution_values(n_q_points);
  std::vector<Tensor<1,dim> > phi_old_solution_gradients(n_q_points);
  std::vector<Tensor<1,dim> > temp_old_solution_gradients(n_q_points);

  // Verification Params
  const RightHandSidePhi<dim> RHSphi;
  std::vector<double> rhsPhi_values (n_q_points);


  // Assemble Contribution of Physic 1 in block (0,0)
  typename DoFHandler<dim>::active_cell_iterator
    cell1 = dof_handler1.begin_active(),
    endc1 = dof_handler1.end();
  for (; cell1!=endc1; ++cell1)
    {
      fe_values1.reinit (cell1);

      // Storing FE data from previous Newton iteration.
      fe_values1.get_function_values(solution.block(0), phi_old_solution_values);
      fe_values1.get_function_gradients(solution.block(0), phi_old_solution_gradients);

      RHSphi.value_list (fe_values1.get_quadrature_points(),
                         rhsPhi_values);

      cell_matrix1 = 0;
      cell_rhs1 = 0;

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
        for (unsigned int i=0; i<dofs_per_cell_1; ++i){
          for (unsigned int j=0; j<dofs_per_cell_1; ++j)
            cell_matrix1(i,j) += ((this->D *
                                  fe_values1.shape_grad (i, q_point) *
                                  fe_values1.shape_grad (j, q_point) +
                                  this->Sa *
                                  fe_values1.shape_value(i, q_point) *
                                  fe_values1.shape_value(j, q_point)) *
                                  fe_values1.JxW (q_point));

          cell_rhs1(i) -= ((fe_values1.shape_value (i, q_point) *
                          (this->FluxS +
                           rhsPhi_values[q_point])) *
                          fe_values1.JxW (q_point));
        }
      }
      cell1->get_dof_indices (local_dof_indices_1);

      for (unsigned int i=0; i<dofs_per_cell_1; ++i)
        for (unsigned int j=0; j<dofs_per_cell_1; ++j)
          system_matrix.block(0,0).add (local_dof_indices_1[i],
                             local_dof_indices_1[j],
                             cell_matrix1(i,j));

      for (unsigned int i=0; i<dofs_per_cell_1; ++i)
        system_rhs.block(0)(local_dof_indices_1[i]) += cell_rhs1(i);
    }
  system_matrix.block(0,0).vmult_add(system_rhs.block(0), solution.block(0));


  // Verification Params
  const RightHandSideT<dim> RHSt;
  std::vector<double> rhsT_values (n_q_points);

  // Assemble Linear Contribution of Physic 2 in block (1,1)
  typename DoFHandler<dim>::active_cell_iterator
    cell2 = dof_handler2.begin_active(),
    endc2 = dof_handler2.end();
  for (; cell2!=endc2; ++cell2)
    {
      fe_values2.reinit (cell2);

      // Storing FE data from previous Newton iteration.
      fe_values2.get_function_values(solution.block(1), temp_old_solution_values);
      fe_values2.get_function_gradients(solution.block(1), temp_old_solution_gradients);

      RHSt.value_list (fe_values2.get_quadrature_points(),
                       rhsT_values);

      cell_matrix2 = 0;
      cell_rhs2 = 0;

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
        // Non-linear thermal conductivity.
        double ThermalCond_Constant = this->k0 + this->k1/ (this->k2 + temp_old_solution_values[q_point]);
        for (unsigned int i=0; i<dofs_per_cell_2; ++i){
          for (unsigned int j=0; j<dofs_per_cell_2; ++j)
            cell_matrix2(i,j) += ((ThermalCond_Constant *
                                  fe_values2.shape_grad (i, q_point) *
                                  fe_values2.shape_grad (j, q_point)) *
                                  fe_values2.JxW (q_point));

          cell_rhs2(i) -= ((fe_values2.shape_value (i, q_point) *
                          (this->HeatQ +
                          rhsT_values[q_point])) *
                          fe_values2.JxW (q_point));
        }
      }
      cell2->get_dof_indices (local_dof_indices_2);

      for (unsigned int i=0; i<dofs_per_cell_2; ++i)
        for (unsigned int j=0; j<dofs_per_cell_2; ++j)
          system_matrix.block(1,1).add (local_dof_indices_2[i],
                             local_dof_indices_2[j],
                             cell_matrix2(i,j));

      for (unsigned int i=0; i<dofs_per_cell_2; ++i)
        system_rhs.block(1)(local_dof_indices_2[i]) += cell_rhs2(i);
    }

  system_matrix.block(1,1).vmult_add(system_rhs.block(1), solution.block(1));

  // Assemble Non-Linear Contribution of Physic 2 in block (1,1)
  for (; cell2!=endc2; ++cell2)
    {
      fe_values2.reinit (cell2);

      // Storing FE data from previous Newton iteration.
      fe_values2.get_function_values(solution.block(1), temp_old_solution_values);
      fe_values2.get_function_gradients(solution.block(1), temp_old_solution_gradients);

      cell_matrix2 = 0;

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
        // Non-linear thermal conductivity.
        double d_ThermalCond_Constant_dT =  0  - this->k1/((this->k2 + temp_old_solution_values[q_point])*
                                                           (this->k2 + temp_old_solution_values[q_point]));
        for (unsigned int i=0; i<dofs_per_cell_2; ++i){
          for (unsigned int j=0; j<dofs_per_cell_2; ++j)
            cell_matrix2(i,j) += ((d_ThermalCond_Constant_dT *
                                  temp_old_solution_gradients[q_point]*
                                  fe_values2.shape_grad (i, q_point) *
                                  fe_values2.shape_value(j, q_point)) *
                                  fe_values2.JxW (q_point));
        }
      }
      cell2->get_dof_indices (local_dof_indices_2);

      for (unsigned int i=0; i<dofs_per_cell_2; ++i)
        for (unsigned int j=0; j<dofs_per_cell_2; ++j)
          system_matrix.block(1,1).add (local_dof_indices_2[i],
                             local_dof_indices_2[j],
                             cell_matrix2(i,j));
    }

  // Assemble Contribution of Coupling between Physic 1 & 2 in block (1,0)
  const std::list<std::pair<typename DoFHandler<dim>::cell_iterator,
    typename DoFHandler<dim>::cell_iterator> > cell_list =
    GridTools::get_finest_common_cells (dof_handler1, dof_handler2);

  typename std::list<std::pair<typename DoFHandler<dim>::cell_iterator,
    typename DoFHandler<dim>::cell_iterator> >::const_iterator
    cell_iter = cell_list.begin();
  
  for (; cell_iter!=cell_list.end(); ++cell_iter){
    FullMatrix<double> unit_matrix (dofs_per_cell_1, dofs_per_cell_2);
    for(unsigned int i=0; i<unit_matrix.m(); ++i)
      unit_matrix(i, i) = 1;

    assemble_off_diagonal (cell_iter->first, cell_iter->second, unit_matrix);
  }

  system_matrix.block(1,0).vmult_add (system_rhs.block(1), solution.block(0));

  for(unsigned int i=0; i<system_rhs.size(); i++){
    system_rhs(i) = -system_rhs(i);
  }

  // Applying bouncary conditions
  // Physic 1
  std::map<unsigned int,double> boundary_values1;
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            0,
                                            ZeroFunction<dim>(),
                                            boundary_values1);
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            1,
                                            ZeroFunction<dim>(),
                                            boundary_values1);
  MatrixTools::apply_boundary_values (boundary_values1,
                                      system_matrix.block(0,0),
                                      newton_update.block(0),
                                      system_rhs.block(0));
  // Physic 2
  std::map<unsigned int,double> boundary_values2;
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            0,
                                            ZeroFunction<dim>(),
                                            boundary_values2);
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            1,
                                            ZeroFunction<dim>(),
                                            boundary_values2);
  MatrixTools::apply_boundary_values (boundary_values2,
                                      system_matrix.block(1,1),
                                      newton_update.block(1),
                                      system_rhs.block(1));

  // Physic Coupling 1->2
  FullMatrix<double> Identity (solution.block(1).size(), solution.block(1).size());
  for(unsigned int i = 0; i < Identity.m(); i++)
    Identity(i, i) = 1.0;

  typedef std::map<unsigned int, double>::iterator it_type;
  for(it_type iterator = boundary_values2.begin(); iterator != boundary_values2.end(); iterator++){
    Identity( iterator->first, iterator->first) = 0.0;
  }

  FullMatrix<double> tmpBlock10, tmpMatrix;
  tmpBlock10.copy_from(system_matrix.block(1,0));
  tmpMatrix.copy_from(system_matrix.block(1,0));
  Identity.mmult(tmpMatrix, tmpBlock10);
  
  system_matrix.block(1,0).copy_from(tmpMatrix);

  // Calculate Norm of the non-linear residual.
  residual_norm = system_rhs.l2_norm ();

  set_boundary_values ();

}

template <int dim, int ord>
void twoPhysics<dim, ord>::assemble_off_diagonal(const typename DoFHandler<dim>::cell_iterator &physic1,
                                                 const typename DoFHandler<dim>::cell_iterator &physic2,
                                                 const FullMatrix<double> prolongation_matrix)
{
  if(!physic1->has_children() && !physic2->has_children() ){

      QGauss<dim>   quadrature_formula(ord+1);
      FEValues<dim> fe_values1 (fe1, quadrature_formula,
                             update_values | update_gradients | update_JxW_values);
      FEValues<dim> fe_values2 (fe2, quadrature_formula,
                             update_values | update_gradients | update_JxW_values);

      const unsigned int   dofs_per_cell_1 = fe1.dofs_per_cell;
      const unsigned int   dofs_per_cell_2 = fe2.dofs_per_cell;
      const unsigned int   n_q_points    = quadrature_formula.size();

      FullMatrix<double>   cell_matrix3 (dofs_per_cell_2, dofs_per_cell_1);
      std::vector<unsigned int> local_dof_indices_1 (dofs_per_cell_1);
      std::vector<unsigned int> local_dof_indices_2 (dofs_per_cell_2);

      fe_values1.reinit (physic1);
      fe_values2.reinit (physic2);

      cell_matrix3 = 0;

      if(physic1->level() > physic2->level() ){
        for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
          for (unsigned int i=0; i<dofs_per_cell_2; ++i){
            for (unsigned int j=0; j<dofs_per_cell_1; ++j)
              cell_matrix3(i,j) += (-fe_values1.shape_value(j, q_point) *
                                    fe_values2.shape_value(i, q_point) *
                                    this->kappa *
                                    fe_values1.JxW (q_point));
          }
        }
      } else {
        for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
          for (unsigned int i=0; i<dofs_per_cell_2; ++i){
            for (unsigned int j=0; j<dofs_per_cell_1; ++j)
              cell_matrix3(i,j) += (-fe_values1.shape_value(j, q_point) *
                                    fe_values2.shape_value(i, q_point) *
                                    this->kappa *
                                    fe_values2.JxW (q_point));
          }
        }
      }

      FullMatrix<double> tmpM (dofs_per_cell_1, dofs_per_cell_2);
      // Apply prolongation matrix to cell matrix
      if(physic1->level() > physic2->level() ){
        prolongation_matrix.Tmmult (tmpM, cell_matrix3);
      } else {
        cell_matrix3.mmult (tmpM, prolongation_matrix);
      }
      cell_matrix3 = tmpM;
      
      physic2->get_dof_indices (local_dof_indices_2);

      for (unsigned int i=0; i<dofs_per_cell_2; ++i){
        physic1->get_dof_indices (local_dof_indices_1);
        for (unsigned int j=0; j<dofs_per_cell_1; ++j){
          system_matrix.block(1,0).add (local_dof_indices_2[i],
                             local_dof_indices_1[j],
                             cell_matrix3(i,j));
        }
      }

    } else {

      for(unsigned int child=0; child<GeometryInfo<dim>::max_children_per_cell; child++){
        FullMatrix<double> new_matrix (fe1.dofs_per_cell, fe2.dofs_per_cell);
        prolongation_matrix.mmult (new_matrix, fe1.get_prolongation_matrix(child));
        if(physic1->has_children()){
          assemble_off_diagonal (physic1->child(child), physic2, new_matrix);
        } else {
          assemble_off_diagonal (physic1, physic2->child(child), new_matrix);
        }
      }
    }
}

template <int dim, int ord>
void twoPhysics<dim, ord>::set_boundary_values ()
{
  std::map<unsigned int, double> boundary_values1;
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            0,
                                            BoundaryValuesPhi<dim>(),
                                            boundary_values1);
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            1,
                                            BoundaryValuesPhi<dim>(),
                                            boundary_values1);
  for(std::map<unsigned int, double>::const_iterator
        p = boundary_values1.begin();
        p != boundary_values1.end(); ++p)
    solution.block(0)(p->first) = p->second;

  std::map<unsigned int, double> boundary_values2;
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            0,
                                            BoundaryValuesT<dim>(),
                                            boundary_values2);
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            1,
                                            BoundaryValuesT<dim>(),
                                            boundary_values2);
  for(std::map<unsigned int, double>::const_iterator
        p = boundary_values2.begin();
        p != boundary_values2.end(); ++p)
    solution.block(1)(p->first) = p->second;
}

// Linear Solver setup.
template <int dim, int ord>
void twoPhysics<dim, ord>::linear_solve ()
{
  SolverControl                       solver_control (1E6, 1e-12);
  SolverGMRES<BlockVector<double> >   solver (solver_control);
//  PreconditionIdentity                preconditioner;
  PreconditionJacobi<BlockSparseMatrix<double> >
                                      preconditioner;
  preconditioner.initialize(system_matrix);

  solver.solve (system_matrix,
                newton_update,
                system_rhs,
                preconditioner);
  std::cout << "    "
            << solver_control.last_step()
            << " GMRES iterations needed to obtain convergence."
            << std::endl;
  
  constraints1.distribute (newton_update.block(0));
  constraints2.distribute (newton_update.block(1));

  double alpha = determine_step_length();
  solution.add (alpha, newton_update);
  update_norm = alpha*newton_update.l2_norm();
}

// Newton Solver
template <int dim, int ord>
void twoPhysics<dim, ord>::newton_solve()
{
  int iter = 1;
  bool done = false;
  do{
      std::cout << "  Newton Iteration " << iter << std::endl;
      assemble_system ();
      linear_solve ();
      std::cout << "    Residual Norm: " << residual_norm << std::endl;
      std::cout << "    Update Norm: " << update_norm << std::endl;
      iter++ ;

      if(residual_norm < 1E-9 || update_norm < 1E-10)
        done = true;
      if(iter >= 50)
        done = true;
  } while (!done);
}

// Damping coefficient for Newton's Method
template <int dim, int ord>
double twoPhysics<dim, ord>::determine_step_length()
{
  return 1;
}

// h-AMR
template <int dim, int ord>
void twoPhysics<dim, ord>::refine_grid ()
{/*
  // Physic 1
  Vector<float> estimated_error_per_cell_1 (triangulation1.n_active_cells());
  KellyErrorEstimator<dim>::estimate (dof_handler1,
                                      QGauss<dim-1>(ord+1),
                                      typename FunctionMap<dim>::type(),
                                      solution.block(0),
                                      estimated_error_per_cell_1);
  GridRefinement::refine_and_coarsen_fixed_number (triangulation1,
                                                   estimated_error_per_cell_1,
                                                   0.3, 0.03);
  triangulation1.execute_coarsening_and_refinement ();


  // Physic 2
  Vector<float> estimated_error_per_cell_2 (triangulation2.n_active_cells());
  KellyErrorEstimator<dim>::estimate (dof_handler2,
                                      QGauss<dim-1>(ord+1),
                                      typename FunctionMap<dim>::type(),
                                      solution.block(1),
                                      estimated_error_per_cell_2);
  GridRefinement::refine_and_coarsen_fixed_number (triangulation2,
                                                   estimated_error_per_cell_2,
                                                   0.3, 0.03);
  triangulation2.execute_coarsening_and_refinement ();
*/
triangulation1.refine_global(1);
triangulation2.refine_global(1);
}

// Display solution in gnuplot data file
template <int dim, int ord>
void twoPhysics<dim, ord>::output_results () const
{
  std::vector<std::string> solution_names;
  solution_names.push_back("Flux");
  solution_names.push_back("Temp");
  {
    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler1);
    data_out.add_data_vector (solution.block(0), solution_names[0]);
    data_out.build_patches ();

    std::ofstream output ("solution-Flux.gpl");
    data_out.write_gnuplot (output);  
  }{
    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler2);
    data_out.add_data_vector (solution.block(1), solution_names[1]);
    data_out.build_patches ();

    std::ofstream output ("solution-Temp.gpl");
    data_out.write_gnuplot (output); 
  }

}

// Output adapted grid if in 2D
template <int dim, int ord>
void twoPhysics<dim, ord>::output_grid (const unsigned int cycle) const
{
  Assert (cycle < 100, ExcNotImplemented());
  if(dim == 2){
    std::string filename = "Flux-grid-";
    if(cycle < 10)
      filename += "0";
    std::stringstream ss;
    ss << cycle;
    filename += ss.str();
    filename += ".eps";

    std::ofstream output (filename.c_str());

    GridOut grid_out;
    grid_out.write_eps (triangulation1, output);
  }
  if(dim == 2){
    std::string filename = "Temp-grid-";
    if(cycle < 10)
      filename += "0";
    std::stringstream ss;
    ss << cycle;
    filename += ss.str();
    filename += ".eps";

    std::ofstream output (filename.c_str());

    GridOut grid_out;
    grid_out.write_eps (triangulation2, output);
  }
}

template <int dim, int ord>
void twoPhysics<dim, ord>::process_solution(const unsigned int cycle)
{
  {
  // Error estimate Phi
  Vector<float> difference_per_cell_1 (triangulation1.n_active_cells());
  VectorTools::integrate_difference (dof_handler1,
                                     solution.block(0),
                                     SolutionPhi<dim>(),
                                     difference_per_cell_1,
                                     QGauss<dim>(ord+2),
                                     VectorTools::L2_norm);
  const double L2_error = difference_per_cell_1.l2_norm();

  VectorTools::integrate_difference (dof_handler1,
                                     solution.block(0),
                                     SolutionPhi<dim>(),
                                     difference_per_cell_1,
                                     QGauss<dim>(ord+2),
                                     VectorTools::H1_seminorm);
  const double H1_error = difference_per_cell_1.l2_norm();
  
  convergence_tablePHI.add_value("cycle", cycle);
  convergence_tablePHI.add_value("cells", triangulation1.n_active_cells());
  convergence_tablePHI.add_value("dofs", dof_handler1.n_dofs());
  convergence_tablePHI.add_value("L2", L2_error);
  convergence_tablePHI.add_value("H1", H1_error);
  }{
  // Error Estimate T 
  Vector<float> difference_per_cell_2 (triangulation2.n_active_cells());
  VectorTools::integrate_difference (dof_handler2,
                                     solution.block(1),
                                     SolutionT<dim>(),
                                     difference_per_cell_2,
                                     QGauss<dim>(ord+2),
                                     VectorTools::L2_norm);
  const double L2_error = difference_per_cell_2.l2_norm();

  VectorTools::integrate_difference (dof_handler2,
                                     solution.block(1),
                                     SolutionT<dim>(),
                                     difference_per_cell_2,
                                     QGauss<dim>(ord+2),
                                     VectorTools::H1_seminorm);
  const double H1_error = difference_per_cell_2.l2_norm();
  
  convergence_tableT.add_value("cycle", cycle);
  convergence_tableT.add_value("cells", triangulation2.n_active_cells());
  convergence_tableT.add_value("dofs", dof_handler2.n_dofs());
  convergence_tableT.add_value("L2", L2_error);
  convergence_tableT.add_value("H1", H1_error);
  }
}

// Problem driver
template <int dim, int ord>
void twoPhysics<dim, ord>::run ()
{
  for(unsigned int cycle = 0; cycle < 4; ++cycle){
    std::cout << "Cycle: " << cycle << std::endl;
    if(cycle == 0){
      make_grid ();
    } else{
      refine_grid ();
    }
    setup_system();
    init_system();
    print_grid_info ();
    newton_solve ();
    output_grid (cycle);
    process_solution (cycle);
  }
  output_results ();
  
  convergence_tablePHI.set_precision("L2", 3);
  convergence_tablePHI.set_precision("H1", 3);
  convergence_tableT  .set_precision("L2", 3);
  convergence_tableT  .set_precision("H1", 3);

  convergence_tablePHI.set_scientific("L2", true);
  convergence_tablePHI.set_scientific("H1", true);
  convergence_tableT  .set_scientific("L2", true);
  convergence_tableT  .set_scientific("H1", true);

  convergence_tablePHI.set_tex_caption("cells", "\\# cells");
  convergence_tablePHI.set_tex_caption("dofs", "\\# dofs");
  convergence_tablePHI.set_tex_caption("L2", "L^2-error");
  convergence_tablePHI.set_tex_caption("H1", "H^1-error");
  convergence_tableT  .set_tex_caption("cells", "\\# cells");
  convergence_tableT  .set_tex_caption("dofs", "\\# dofs");
  convergence_tableT  .set_tex_caption("L2", "L^2-error");
  convergence_tableT  .set_tex_caption("H1", "H^1-error");

  convergence_tablePHI.evaluate_convergence_rates("L2", ConvergenceTable::reduction_rate_log2);
  convergence_tablePHI.evaluate_convergence_rates("H1", ConvergenceTable::reduction_rate_log2);
  convergence_tableT  .evaluate_convergence_rates("L2", ConvergenceTable::reduction_rate_log2);
  convergence_tableT  .evaluate_convergence_rates("H1", ConvergenceTable::reduction_rate_log2);

  std::cout << std::endl;
  convergence_tablePHI.write_text(std::cout);
  std::cout << std::endl;
  convergence_tableT  .write_text(std::cout);
}


} // End namespace


int main ()
{
  try{
    using namespace BlockPhysics;
    const int dimension = 1;
    const int polyOrder = 2;

    deallog.depth_console (0);

    twoPhysics<dimension,polyOrder> laplace_problem;
    laplace_problem.run ();
  } catch (std::exception &exc){
    std::cerr << std::endl << std::endl
              << "----------------------------------------------------"
              << std::endl;
    std::cerr << "Exception on processing: " << std::endl
              << exc.what() << std::endl
              << "Aborting!" << std::endl
              << "----------------------------------------------------"
              << std::endl;

    return 1;
  } catch (...){
    std::cerr << std::endl << std::endl
              << "----------------------------------------------------"
              << std::endl;
    std::cerr << "Unknown exception!" << std::endl
              << "Aborting!" << std::endl
              << "----------------------------------------------------"
              << std::endl;
    return 1;
  }
  

  return 0;
}

