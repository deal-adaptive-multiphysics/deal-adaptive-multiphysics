#ifndef EXACT_SOLNS_HH
#define EXACT_SOLNS_HH

using namespace dealii;
using namespace std;

//===============================================================================
//  MMS Solutions
//===============================================================================
// MMS for PHI
template <int dim>
class SolutionPhi : public Function<dim>
{
  public:
    SolutionPhi (const Parameters::AllParameters params) : Function<dim>(),
                                                           params(params) {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double SolutionPhi<dim>::value (const Point<dim> &p,
                             const unsigned int) const
{
  double return_value = params.C_phi;
  double x0[3] = {params.length/2.0, params.length/2.0, 0};

  // phi = C_phi * (-(x / L) + 1) * exp( (x - x0)^2 )
  for(unsigned int i=0; i<dim; i++){
    return_value *= ( -(p[i]/params.length)*(p[i]/params.length) + 1.0) * exp(-(p[i] - x0[i]) * (p[i] - x0[i]));
  }

  return return_value;
}

template <int dim>
class RightHandSidePhi : public Function<dim>
{
  public:
    RightHandSidePhi (const Parameters::AllParameters params) : Function<dim>(),
                                                                params(params) {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double RightHandSidePhi<dim>::value (const Point<dim>  &p,
                                     const unsigned int) const
{
  double value;
  double FluxSource = params.Flux_Source;
  double D = params.Diffusion_Const;
  double C_phi = params.C_phi;
  double L = params.length;
  double Sa = params.Absorption_Const;
  double x1, x2, x3;

  if(dim == 1){
    x1 = p[0];
    value = -FluxSource+D*(C_phi*1/(L*L)*exp(-pow(L*(1.0/2.0)-x1,2.0))*2.0-C_phi*exp(-pow(L*(1.0/2.0)-x1,2.0))*(1/(L*L)*(x1*x1)-1.0)*2.0+C_phi*exp(-pow(L*(1.0/2.0)-x1,2.0))*(1/(L*L)*(x1*x1)-1.0)*pow(L-x1*2.0,2.0)+C_phi*1/(L*L)*x1*exp(-pow(L*(1.0/2.0)-x1,2.0))*(L-x1*2.0)*4.0)-C_phi*Sa*exp(-pow(L*(1.0/2.0)-x1,2.0))*(1/(L*L)*(x1*x1)-1.0);
  }else if(dim == 2){
    x1 = p[0];
    x2 = p[1];
    value = -FluxSource-D*(C_phi*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(-2.0)+C_phi*1/(L*L)*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*(1/(L*L)*(x2*x2)-1.0)*2.0+C_phi*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*pow(L-x1*2.0,2.0)+C_phi*1/(L*L)*x1*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*(1/(L*L)*(x2*x2)-1.0)*(L-x1*2.0)*4.0)-D*(C_phi*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(-2.0)+C_phi*1/(L*L)*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*2.0+C_phi*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*pow(L-x2*2.0,2.0)+C_phi*1/(L*L)*x2*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(L-x2*2.0)*4.0)+C_phi*Sa*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0);
  }else if(dim == 3){
    x1 = p[0];
    x2 = p[1];
    x3 = p[2];
    value = -FluxSource-D*(C_phi*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*2.0-C_phi*1/(L*L)*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*2.0+C_phi*1/(L*L)*(x3*x3)*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*8.0-C_phi*(x3*x3)*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*4.0)+D*(C_phi*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*(-2.0)+C_phi*1/(L*L)*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*2.0+C_phi*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*pow(L-x1*2.0,2.0)+C_phi*1/(L*L)*x1*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*(L-x1*2.0)*4.0)+D*(C_phi*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*(-2.0)+C_phi*1/(L*L)*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x3*x3)-1.0)*2.0+C_phi*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*pow(L-x2*2.0,2.0)+C_phi*1/(L*L)*x2*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x3*x3)-1.0)*(L-x2*2.0)*4.0)-C_phi*Sa*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0);
  }

  return value;
}

// MMS for T
template <int dim>
class SolutionT : public Function<dim>
{
  public:
    SolutionT (const Parameters::AllParameters params) : Function<dim>(),
                                                         params(params) {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double SolutionT<dim>::value (const Point<dim> &p,
                              const unsigned int) const
{
  double return_value = params.C_T;
  double x0[3] = {-params.length/2.0, -params.length/2.0, params.length/2.0};

  // phi = C_phi * (-(x / L) + 1) * exp( (x - x0)^2 )
  for(unsigned int i=0; i<dim; i++){
    return_value *= ( -(p[i]/params.length)*(p[i]/params.length) + 1.0) * exp(-(p[i] - x0[i]) * (p[i] - x0[i]));
  }

  return return_value;
}

template <int dim>
class RightHandSideT : public Function<dim>
{
  public:
    RightHandSideT (const Parameters::AllParameters params) : Function<dim>(),
                                                              params(params) {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double RightHandSideT<dim>::value (const Point<dim>  &p,
                                     const unsigned int) const
{
  double value;
  double L = params.length;
  double C_phi = params.C_phi;
  double C_T = params.C_T;
  double HeatSource = params.Heat_Source;
  double kappa = params.kappa;
  double k0 = params.k0;
  double k1 = params.k1;
  double k2 = params.k2;
  double x1, x2, x3;

  if(dim == 1){
    x1 = p[0];
    value = -HeatSource-(k0+k1/(k2-C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*(1/(L*L)*(x1*x1)-1.0)))*(C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*(1/(L*L)*(x1*x1)-1.0)*2.0-C_T*1/(L*L)*exp(-pow(L*(1.0/2.0)+x1,2.0))*2.0-C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*(1/(L*L)*(x1*x1)-1.0)*pow(L+x1*2.0,2.0)+C_T*1/(L*L)*x1*exp(-pow(L*(1.0/2.0)+x1,2.0))*(L+x1*2.0)*4.0)+k1*1/pow(k2-C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*(1/(L*L)*(x1*x1)-1.0),2.0)*pow(C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*(1/(L*L)*(x1*x1)-1.0)*(L+x1*2.0)-C_T*1/(L*L)*x1*exp(-pow(L*(1.0/2.0)+x1,2.0))*2.0,2.0)+C_phi*kappa*exp(-pow(L*(1.0/2.0)-x1,2.0))*(1/(L*L)*(x1*x1)-1.0);
  } else if(dim == 2){
    x1 = p[0];
    x2 = p[1];
    value = -HeatSource+(k0+k1/(k2+C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)))*(C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*2.0-C_T*1/(L*L)*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x2*x2)-1.0)*2.0-C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*pow(L+x1*2.0,2.0)+C_T*1/(L*L)*x1*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x2*x2)-1.0)*(L+x1*2.0)*4.0)+(k0+k1/(k2+C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)))*(C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*2.0-C_T*1/(L*L)*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*2.0-C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*pow(L+x2*2.0,2.0)+C_T*1/(L*L)*x2*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(L+x2*2.0)*4.0)+k1*1/pow(k2+C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0),2.0)*pow(C_T*1/(L*L)*x1*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x2*x2)-1.0)*2.0-C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(L+x1*2.0),2.0)+k1*1/pow(k2+C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0),2.0)*pow(C_T*1/(L*L)*x2*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*2.0-C_T*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(L+x2*2.0),2.0)-C_phi*kappa*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0);
  } else if(dim == 3){
    x1 = p[0];
    x2 = p[1];
    x3 = p[2];
    value = -HeatSource+(k0+k1/(k2-C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)))*(C_T*1/(L*L)*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*2.0-C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*2.0+C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*pow(L+x1*2.0,2.0)-C_T*1/(L*L)*x1*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*(L+x1*2.0)*4.0)+(k0+k1/(k2-C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)))*(C_T*1/(L*L)*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x3*x3)-1.0)*2.0-C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*2.0+C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*pow(L+x2*2.0,2.0)-C_T*1/(L*L)*x2*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x3*x3)-1.0)*(L+x2*2.0)*4.0)+(k0+k1/(k2-C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)))*(C_T*1/(L*L)*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*2.0-C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*2.0+C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*pow(L-x3*2.0,2.0)+C_T*1/(L*L)*x3*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(L-x3*2.0)*4.0)+k1*pow(C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*(L+x1*2.0)-C_T*1/(L*L)*x1*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*2.0,2.0)*1/pow(k2-C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0),2.0)+k1*pow(C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*(L+x2*2.0)-C_T*1/(L*L)*x2*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x3*x3)-1.0)*2.0,2.0)*1/pow(k2-C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0),2.0)+k1*pow(C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0)*(L-x3*2.0)+C_T*1/(L*L)*x3*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*2.0,2.0)*1/pow(k2-C_T*exp(-pow(L*(1.0/2.0)-x3,2.0))*exp(-pow(L*(1.0/2.0)+x1,2.0))*exp(-pow(L*(1.0/2.0)+x2,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0),2.0)+C_phi*kappa*exp(-pow(L*(1.0/2.0)-x1,2.0))*exp(-pow(L*(1.0/2.0)-x2,2.0))*exp(-x3*x3)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(1/(L*L)*(x3*x3)-1.0);
  }

  return value;
}


#endif // EXACT_SOLNS_HH
