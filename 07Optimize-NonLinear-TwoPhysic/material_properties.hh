#ifndef MATERIAL_PROPERTIES_HH
#define MATERIAL_PROPERTIES_HH

using namespace dealii;

//===============================================================================
// Boundary Values
//===============================================================================

template <int dim>
class BoundaryValuesPhi : public Function<dim>
{
  public:
    BoundaryValuesPhi () : Function<dim>() {}

    virtual double value (const Point<dim> &p,
                          const unsigned int component = 0) const;
};

template <int dim>
double BoundaryValuesPhi<dim>::value (const Point<dim> &,
                                   const unsigned int /*component*/) const
{
  return 0.0;
}

template <int dim>
class BoundaryValuesT : public Function<dim>
{
  public:
    BoundaryValuesT () : Function<dim>() {}

    virtual double value (const Point<dim> &p,
                          const unsigned int component = 0) const;
};

template <int dim>
double BoundaryValuesT<dim>::value (const Point<dim> &,
                                   const unsigned int /*component*/) const
{
  return 0.0;
}

#endif // MATERIAL_PROPERTIES_HH
