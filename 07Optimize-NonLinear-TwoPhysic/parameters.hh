#ifndef PARAMETERS_HH
#define PARAMETERS_HH

#include <deal.II/base/parameter_handler.h>

using namespace dealii;
namespace Parameters
{

//==========================================
// Solver Parameters
//==========================================
struct Solver
{
  enum NonLinearSolverType {newton};
  NonLinearSolverType NLsolver;

  enum LinearSolverType {direct, gmres};
  LinearSolverType solver;

  enum OutputType { quiet, verbose };
  OutputType NLoutput;
  OutputType output;

  double linear_tol;
  int max_linear_iterations;

  double nonLinear_tol;
  double update_tol;
  int max_nonLinear_iterations;

  static void declare_parameters (ParameterHandler &params);
  void parse_parameters   (ParameterHandler &params);
};

void Solver::declare_parameters (ParameterHandler &params){

  params.enter_subsection("nonlinear solver");
  {
    params.declare_entry("output", "quiet",
                         Patterns::Selection("quiet|verbose"),
                         "State whether nonlinear output should be printed.\n"
                         "Choices are <quiet|verbose>.");
    params.declare_entry("nonlinear tolerance", "1E-9",
                         Patterns::Double(),
                         "NonLinear Tolerance");
    params.declare_entry("update tolerance", "1E-10",
                         Patterns::Double(),
                         "Update Tolerance");
    params.declare_entry("max nonlinear iterations", "50",
                         Patterns::Integer(),
                         "Maximum number of nonlinear iterations");
  }
  params.leave_subsection();

  params.enter_subsection("linear solver");
  {
    params.declare_entry("Solver", "direct",
                          Patterns::Selection("direct|gmres"),
                         "Which Linear Solver Method is used?\n"
                         "Choices are <direct|gmres>.");
    params.declare_entry("Linear tolerance", "1E-12",
                          Patterns::Double(),
                         "Linear Solver Tolerance");
    params.declare_entry("Max linear iterations", "1000000",
                          Patterns::Integer(),
                         "Maximum number of linear iterations");
  }
  params.leave_subsection();

}

void Solver::parse_parameters (ParameterHandler &params){

  params.enter_subsection("nonlinear solver");
  {
    const std::string op = params.get("output");
    if (op == "verbose") output = verbose;
    if (op == "quiet")   output = quiet;

    nonLinear_tol = params.get_double("nonlinear tolerance");
    update_tol = params.get_double("update tolerance");
    max_nonLinear_iterations = params.get_integer("max nonlinear iterations");
  }
  params.leave_subsection();

  params.enter_subsection("linear solver");
  {
    const std::string option = params.get("Solver");
    if (option == "direct") solver = direct;
    if (option == "gmres" ) solver = gmres;

    linear_tol = params.get_double("Linear tolerance");
    max_linear_iterations = params.get_integer("Max linear iterations");
  }
  params.leave_subsection();

}

//==========================================
// Problem Parameters
//==========================================
struct AllParameters : public Solver
{
  AllParameters ();

  enum RefinementType {global, hAMR};
  RefinementType refineScheme;

  enum TimeInfoType {quiet, verbose};
  TimeInfoType timeInfo;

  enum SolutionType {sin, exp, sint, expt, quadt};
  SolutionType exact_solution_type;

  unsigned int n_init_refinements;
  unsigned int cycles;
  bool fullChart;
  bool output_sparsity_pattern;

  unsigned int polynomial_degree;
  unsigned int n_quad_points;

  double length;
  double beta;

  double C_phi;
  double Diffusion_Const;
  double Absorption_Const;
  double Flux_Source;

  double C_T;
  double k0;
  double k1;
  double k2;
  double kappa;
  double Heat_Source;

  static void declare_parameters (ParameterHandler &prm);
  void parse_parameters (ParameterHandler &prm);
};

AllParameters::AllParameters ()
{}

void AllParameters::declare_parameters (ParameterHandler &prm)
{
  prm.enter_subsection("refinement");
  {
    prm.declare_entry("Refinement Type","global",
                       Patterns::Selection("global|hAMR"),
                      "State the type of refinement scheme.\n"
                      "Choices are <global|hAMR>.");
    prm.declare_entry("cycles","4",
                       Patterns::Integer(1),
                      "Number of Refinement Cycles");
  }
  prm.leave_subsection();

  prm.enter_subsection("Reporting");
  {
    prm.declare_entry("Full Conv Chart","false",
                       Patterns::Bool(),
                      "Whether to Print a full convergence chart or not");
    prm.declare_entry("Sparsity Pattern","false",
                       Patterns::Bool(),
                      "Output the sparsity pattern or not.");
    prm.declare_entry("Time Info","quiet",
                       Patterns::Selection("quiet|verbose"),
                      "Display Time info.\n"
                      "Choices are <quiet|verbose>.");
  }
  prm.leave_subsection();

  prm.enter_subsection("domain");
  {
    prm.declare_entry("length","20.0",
                       Patterns::Double(0),
                       "Length of Domain");
    prm.declare_entry("beta","10.0",
                       Patterns::Double(1),
                       "Parameter to determine shape of diffusive MMS");
  }
  prm.leave_subsection();

  prm.enter_subsection("neutronics properties");
  {
    prm.declare_entry("Flux Amplitude","2.5",
                       Patterns::Double(0),
                      "Maximum Flux for MMS");
    prm.declare_entry("Flux Source","30",
                       Patterns::Double(0),
                      "Source for Flux");
    prm.declare_entry("Diffusion Constant","2",
                       Patterns::Double(0),
                      "Diffusion Constant for Neutrons");
    prm.declare_entry("Absorption Constant","3",
                       Patterns::Double(0),
                      "Absorption Constant for Neutrons");
  }
  prm.leave_subsection();

  prm.enter_subsection("temperature properties");
  {
    prm.declare_entry("Temp Amplitude","6.0",
                       Patterns::Double(0),
                      "Maximum Temp for MMS");
    prm.declare_entry("Thermal Source","3.0",
                       Patterns::Double(0),
                      "Source to Drive Temperature");
    prm.declare_entry("Conductivity k0","5.0",
                       Patterns::Double(0),
                      "First parameter for conductivity");
    prm.declare_entry("Conductivity k1","2049.59",
                       Patterns::Double(0),
                      "Second parameter for conductivity\n"
                      "Set to Zero for Linear Problem");
    prm.declare_entry("Conductivity k2","200.0",
                       Patterns::Double(0),
                      "Third parameter for conductivity");
    prm.declare_entry("kappa","0.1",
                       Patterns::Double(0),
                      "Thermal Diffusivity");
  }
  prm.leave_subsection();

  prm.enter_subsection("Exact Solution Propterties");
  {
    prm.declare_entry("Exact Solution Type","sin",
                      Patterns::Selection("sin|exp|sint|expt|quadt"),
                      "State the type of exact solution.\n"
                      "Choices are <sin|exp|sint|expt|quadt>.");
  }
  prm.leave_subsection();

  Parameters::Solver::declare_parameters (prm);
}


void AllParameters::parse_parameters (ParameterHandler &prm)
{
  prm.enter_subsection("refinement");
  {
    const std::string option = prm.get("Refinement Type");
    if (option == "global") refineScheme = global;
    if (option == "hAMR")   refineScheme = hAMR;

    cycles = prm.get_integer("cycles");
  }
  prm.leave_subsection();

  prm.enter_subsection("Reporting");
  {
    fullChart = prm.get_bool("Full Conv Chart");
    output_sparsity_pattern = prm.get_bool("Sparsity Pattern");
    
    const std::string option = prm.get("Time Info");
    if (option == "quiet")   timeInfo = quiet;
    if (option == "verbose") timeInfo = verbose;
  }
  prm.leave_subsection();

  prm.enter_subsection("domain");
  {
    length = prm.get_double("length");
    beta = prm.get_double("beta");
  }
  prm.leave_subsection();

  prm.enter_subsection("neutronics properties");
  {
    C_phi = prm.get_double("Flux Amplitude");
    Flux_Source = prm.get_double("Flux Source");
    Diffusion_Const  = prm.get_double("Diffusion Constant");
    Absorption_Const = prm.get_double("Absorption Constant");
  }
  prm.leave_subsection();

  prm.enter_subsection("temperature properties");
  {
    C_T = prm.get_double("Temp Amplitude");
    Heat_Source = prm.get_double("Thermal Source");
    k0 = prm.get_double("Conductivity k0");
    k1 = prm.get_double("Conductivity k1");
    k2 = prm.get_double("Conductivity k2");
    kappa = prm.get_double("kappa");
  }
  prm.leave_subsection();

  prm.enter_subsection("Exact Solution Propterties");
  {
    const std::string option = prm.get("Exact Solution Type");
    if (option == "sin")  exact_solution_type = sin;
    if (option == "exp")  exact_solution_type = exp;
    if (option == "sint") exact_solution_type = sint;
    if (option == "expt") exact_solution_type = expt;
    if (option == "quadt") exact_solution_type = quadt;
  }
  prm.leave_subsection();

  Parameters::Solver::parse_parameters(prm);
}

}// end namespace

#endif // PARAMETERS_HH
