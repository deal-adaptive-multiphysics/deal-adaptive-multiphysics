#import libraries
import numpy, pylab
from pylab import *

#plot Flux profile
pylab.title("Flux")
pylab.xlabel("Domain")
pylab.ylabel("Solution")
data = numpy.loadtxt("exact-solution-Flux.gpl")
x = data[:, 0]
y= data[:, 1]
plot(x,y, "s", label="Exact")
legend()

data = numpy.loadtxt("solution-Flux.gpl")
x = data[:, 0]
y= data[:, 1]
plot(x,y, "o", label="Numerical")
legend(loc='upper left')

pylab.figure()
#plot Flux profile
pylab.title("Temp")
pylab.xlabel("Domain")
pylab.ylabel("Solution")
data = numpy.loadtxt("exact-solution-Temp.gpl")
x = data[:, 0]
y= data[:, 1]
plot(x,y, "s", label="Exact")
legend()

data = numpy.loadtxt("solution-Temp.gpl")
x = data[:, 0]
y= data[:, 1]
plot(x,y, "o", label="Numerical")
legend()

pylab.figure()
#plot Flux convergence
pylab.title("Flux Convergence")
pylab.xlabel("N-DoFs")
pylab.ylabel("Error")
data = numpy.loadtxt("convergence-global-Flux.dat")
x = data[:, 0]
y = data[:, 1]
plot(x,y, "-s", label="Global")
pylab.xscale('log')
pylab.yscale('log')
legend()

data = numpy.loadtxt("Convergence-Flux.gpl")
x = data[:, 0]
y= data[:, 1]
plot(x,y, "-s", label="hAMR")
legend()

pylab.figure()
#plot Flux convergence
pylab.title("Temp Convergence")
pylab.xlabel("N-DoFs")
pylab.ylabel("Error")
data = numpy.loadtxt("convergence-global-Temp.dat")
x = data[:, 0]
y = data[:, 1]
plot(x,y, "-s", label="Global")
pylab.xscale('log')
pylab.yscale('log')
legend()

data = numpy.loadtxt("Convergence-Temp.gpl")
x = data[:, 0]
y= data[:, 1]
plot(x,y, "-s", label="hAMR")
legend()

#finalize
show()
