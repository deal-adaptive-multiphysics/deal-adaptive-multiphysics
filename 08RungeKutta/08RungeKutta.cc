/* @f$Id: @ref twoPhysics "twoPhysics".cc 2012-08-30 14:00:00Z Dugan @f$ */
/* Author: Kevin Dugan, Texas A&M University, 2012 */

/*    @f$Id: @ref twoPhysics "twoPhysics".cc 2012-08-30 14:00:00Z Dugan @f$       */
/*                                                                */
/*
    This file uses the DealII FEM library to solve a coupled neutronics and
    non-linear heat conduction problem. The problem domain is the region
    bound by [-10, 10]^n where n is the dimensions number (1, 2 or 3). The
    code builds the non-linear residual for use in Newton's Method.

    In this problem the two physics components are stored in a sparse matrix
    with each physic stored in a single block component of the larger matrix.
    GMRes is used for the linear Newton solve with Jacobi as a potential
    preconditioner.
*/

#include <deal.II/grid/tria.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/tria_boundary_lib.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_tools.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/dofs/dof_renumbering.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_system.h>

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/convergence_table.h>
#include <deal.II/base/timer.h>

#include <deal.II/numerics/vectors.h>
#include <deal.II/numerics/matrices.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/solution_transfer.h>

#include <deal.II/lac/block_vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/block_sparse_matrix.h>
#include <deal.II/lac/compressed_sparsity_pattern.h>
#include <deal.II/lac/solver_gmres.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/sparse_direct.h>

#include <fstream>
#include <iostream>
#include "parameters.hh"
#include "exact_solns.hh"
#include "material_properties.hh"

namespace BlockPhysics {
using namespace dealii;

template <int dim, int ord>
class twoPhysics
{
  public:
    twoPhysics (const Parameters::AllParameters params);
    void run ();


  private:
    void make_grid ();
    void reset_grid ();
    void setup_system ();
    void init_system ();
    void assemble_mass_matrix ();
    void assemble_system (const unsigned int stage_i, const double time);
    void assemble_ss_system (const double time);
    void assemble_tr_system ();
    void assemble_phi (const double time);
    void assemble_temp (const double time);
    void assemble_off_diagonal (const typename DoFHandler<dim>::cell_iterator &physic1,
                                const typename DoFHandler<dim>::cell_iterator &physic2,
                                const FullMatrix<double> prolongation_matrix);
    void set_DC_Boundary ();
    void linear_solve ();
    void newton_solve (const unsigned int stage_i, const double time);
    void update_newton_norms ();
    void set_boundary_values ();
    void refine_grid ();
    void output_results () const;
    void output_grid (const unsigned int cycle) const;
    void print_grid_info ();
    void process_solution (const double time, const bool final);
    double determine_step_length ();

    Triangulation<dim>     coarse_mesh;
    Triangulation<dim>     triangulation1;
    FE_Q<dim>              fe1;
    DoFHandler<dim>        dof_handler1;
    ConstraintMatrix       constraints1;
    Triangulation<dim>     triangulation2;
    FE_Q<dim>              fe2;
    DoFHandler<dim>        dof_handler2;
    ConstraintMatrix       constraints2;

    BlockSparsityPattern      sparsity_pattern;
    BlockSparseMatrix<double> system_matrix;
    BlockSparseMatrix<double> mass_matrix;
    BlockSparseMatrix<double> stif_matrix;

    BlockVector<double>       solution;
    BlockVector<double>       old_solution;
    BlockVector<double>       newton_update;
    BlockVector<double>       system_rhs;
    std::vector<BlockVector<double> > previous_F;
    BlockVector<double> tmp_vector;

    ConvergenceTable          convergence_tablePHI;
    ConvergenceTable          convergence_tableT;

    double residual_norm;
    double update_norm;

    const Parameters::AllParameters parameters;
    bool Verbose_Output;
    bool timeInfo_Output;

    unsigned int n_stage;
    std::vector<std::vector<double> > A;
    std::vector<double> B, C;

    double current_time;
    double time_step;
};

template <int dim, int ord>
twoPhysics<dim, ord>::twoPhysics (const Parameters::AllParameters params)
                :
                fe1 (ord),
                dof_handler1 (triangulation1),
                fe2 (ord),
                dof_handler2 (triangulation2),
                parameters(params)
{
  if(parameters.output == Parameters::Solver::verbose) Verbose_Output = true;
  else Verbose_Output = false;

  if(parameters.timeInfo == Parameters::AllParameters::verbose) timeInfo_Output = true;
  else timeInfo_Output = false;

  init_butcher_tableau(method_time(parameters.time_step_method), n_stage, A, B, C);
  
  // Print Butcher Tableau to Screen
  printf("\n\tButcher Tableau:\n");
  for(unsigned int i=0; i<n_stage; i++){
    printf("\t%7.3f | ",C[i]);
    for(unsigned int j=0; j<n_stage; j++){
      printf("%7.3f ",A[i][j]);
    } printf("\n");
  } printf("\t ");
  for(unsigned int i=0; i<n_stage+1; i++){
    if(i==0) printf("_______|");
    else printf("________");
  } printf("\n");
  printf("\t        | ");
  for(unsigned int i=0; i<n_stage; i++){
    printf("%7.3f ",B[i]);
  } printf("\n\n");
  
}

// Set up the domain depending on the dimension <dim>
template <int dim, int ord>
void twoPhysics<dim, ord>::make_grid ()
{
  GridGenerator::hyper_cube (coarse_mesh, 0, parameters.length);
//  coarse_mesh.begin_active()->face(0)->set_boundary_indicator(3);
//  coarse_mesh.begin_active()->face(3)->set_boundary_indicator(3);

  for(unsigned int i=0; i<parameters.n_init_refinements; i++)
    coarse_mesh.refine_global(1);

  triangulation1.copy_triangulation (coarse_mesh);
  triangulation2.copy_triangulation (coarse_mesh);

}

template <int dim, int ord>
void twoPhysics<dim, ord>::reset_grid ()
{
triangulation1.clear ();
triangulation2.clear ();

  triangulation1.copy_triangulation (coarse_mesh);
  triangulation2.copy_triangulation (coarse_mesh);

  dof_handler1.initialize(triangulation1, fe1);
  dof_handler2.initialize(triangulation2, fe2);


}

template <int dim, int ord>
void twoPhysics<dim, ord>::print_grid_info ()
{
  std::cout << "\n  Flux" << std::endl;
  std::cout << "    Number of active cells: "
            << triangulation1.n_active_cells()
            << std::endl;
  std::cout << "    Total number of cells: "
            << triangulation1.n_cells()
            << std::endl;
  std::cout << "    Number of degrees of freedom: "
            << dof_handler1.n_dofs()
            << std::endl;

  std::cout << "  Temp" << std::endl;
  std::cout << "    Number of active cells: "
            << triangulation2.n_active_cells()
            << std::endl;
  std::cout << "    Total number of cells: "
            << triangulation2.n_cells()
            << std::endl;
  std::cout << "    Number of degrees of freedom: "
            << dof_handler2.n_dofs()
            << std::endl;
}

// Allocate memory for the sparse matrix and solution vectors. Each physic
// is stored in a block of the matrix.
template <int dim, int ord>
void twoPhysics<dim, ord>::setup_system ()
{
  system_matrix.clear ();
  mass_matrix.clear ();
  stif_matrix.clear ();

  dof_handler1.distribute_dofs (fe1);
  dof_handler2.distribute_dofs (fe2);

  DoFRenumbering::Cuthill_McKee(dof_handler1);
  DoFRenumbering::Cuthill_McKee(dof_handler2);

  const unsigned int n_phi =  dof_handler1.n_dofs(),
                     n_temp = dof_handler2.n_dofs();

  solution.reinit (2);
  solution.block(0).reinit(n_phi);
  solution.block(1).reinit(n_temp);
  solution.collect_sizes();
  system_rhs.reinit(2);
  system_rhs.block(0).reinit(n_phi);
  system_rhs.block(1).reinit(n_temp);
  system_rhs.collect_sizes();
  newton_update.reinit(2);
  newton_update.block(0).reinit(n_phi);
  newton_update.block(1).reinit(n_temp);
  newton_update.collect_sizes();
  old_solution.reinit (2);
  old_solution.block(0).reinit(n_phi);
  old_solution.block(1).reinit(n_temp);
  old_solution.collect_sizes();
  tmp_vector.reinit (2);
  tmp_vector.block(0).reinit(n_phi);
  tmp_vector.block(1).reinit(n_temp);
  tmp_vector.collect_sizes();

  previous_F = std::vector<BlockVector<double> > (n_stage, BlockVector<double> ());
  for (unsigned int stage=0; stage<n_stage; stage++){
    previous_F[stage].reinit (2);
    previous_F[stage].block(0).reinit(n_phi);
    previous_F[stage].block(1).reinit(n_temp);
    previous_F[stage].collect_sizes();
  }
  
  constraints1.clear ();
  DoFTools::make_hanging_node_constraints (dof_handler1,
                                           constraints1);
  constraints1.close ();
  constraints2.clear ();
  DoFTools::make_hanging_node_constraints (dof_handler2,
                                           constraints2);
  constraints2.close ();

  {
    BlockCompressedSimpleSparsityPattern c_sparsity(2,2);
    c_sparsity.block(0,0).reinit(n_phi , n_phi );
    c_sparsity.block(0,1).reinit(n_phi , n_temp);
    c_sparsity.block(1,0).reinit(n_temp, n_phi );
    c_sparsity.block(1,1).reinit(n_temp, n_temp);
    c_sparsity.collect_sizes();

    DoFTools::make_sparsity_pattern (dof_handler1, c_sparsity.block(0,0) );
    DoFTools::make_sparsity_pattern (dof_handler2, c_sparsity.block(1,1) );
    DoFTools::make_sparsity_pattern (dof_handler1, dof_handler2, c_sparsity.block(0,1) );
    DoFTools::make_sparsity_pattern (dof_handler2, dof_handler1, c_sparsity.block(1,0) );
    constraints1.condense (c_sparsity);
    constraints2.condense (c_sparsity);
    sparsity_pattern.copy_from(c_sparsity);
  }

  system_matrix.reinit (sparsity_pattern);
  mass_matrix.reinit (sparsity_pattern);
  stif_matrix.reinit (sparsity_pattern);
  
  if(parameters.output_sparsity_pattern){
    std::ofstream out("sparsity_pattern.gpl");
    sparsity_pattern.print_gnuplot (out);
  }

}

template <int dim, int ord>
void twoPhysics<dim, ord>::assemble_mass_matrix ()
{
  mass_matrix = 0.0;
  stif_matrix = 0.0;

  QGauss<dim>  quadrature_formula(ord+1);
  UpdateFlags update_flags = update_values | update_gradients | update_quadrature_points | update_JxW_values;

  FEValues<dim> fe_values1 (fe1, quadrature_formula, update_flags);
  const unsigned int   n_q_points    = quadrature_formula.size();

  typename DoFHandler<dim>::active_cell_iterator
    cell_1 = dof_handler1.begin_active(),
    endc_1 = dof_handler1.end();
  for (; cell_1!=endc_1; ++cell_1)
    {
      fe_values1.reinit (cell_1);
      const unsigned int   dofs_per_cell = fe1.dofs_per_cell;
      std::vector<unsigned int> local_dof_indices (dofs_per_cell);

      FullMatrix<double>   local_mass (dofs_per_cell, dofs_per_cell);
      FullMatrix<double>   local_stif (dofs_per_cell, dofs_per_cell);

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point)
        for (unsigned int i=0; i<dofs_per_cell; ++i)
          for (unsigned int j=0; j<dofs_per_cell; ++j){
            local_mass(i,j) += fe_values1.shape_value(i, q_point) *
                               fe_values1.shape_value(j, q_point) *
                               fe_values1.JxW (q_point);

            local_stif(i,j) += fe_values1.shape_grad(i, q_point) *
                               fe_values1.shape_grad(j, q_point) *
                               fe_values1.JxW (q_point);
        }
      cell_1->get_dof_indices (local_dof_indices);

      for (unsigned int i=0; i<dofs_per_cell; ++i)
        for (unsigned int j=0; j<dofs_per_cell; ++j){
          mass_matrix.block(0,0).add (local_dof_indices[i],
                                      local_dof_indices[j],
                                      local_mass(i,j));
          stif_matrix.block(0,0).add (local_dof_indices[i],
                                      local_dof_indices[j],
                                      local_stif(i,j));

        }
    }

  FEValues<dim> fe_values2 (fe2, quadrature_formula, update_flags);

  typename DoFHandler<dim>::active_cell_iterator
    cell_2 = dof_handler2.begin_active(),
    endc_2 = dof_handler2.end();
  for (; cell_2!=endc_2; ++cell_2)
    {
      fe_values2.reinit (cell_2);
      const unsigned int   dofs_per_cell = fe2.dofs_per_cell;
      std::vector<unsigned int> local_dof_indices (dofs_per_cell);

      FullMatrix<double>   local_mass (dofs_per_cell, dofs_per_cell);
      FullMatrix<double>   local_stif (dofs_per_cell, dofs_per_cell);

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point)
        for (unsigned int i=0; i<dofs_per_cell; ++i)
          for (unsigned int j=0; j<dofs_per_cell; ++j){
            local_mass(i,j) += fe_values2.shape_value(i, q_point) *
                               fe_values2.shape_value(j, q_point) *
                               fe_values2.JxW (q_point);

            local_stif(i,j) += fe_values2.shape_grad(i, q_point) *
                               fe_values2.shape_grad(j, q_point) *
                               fe_values2.JxW (q_point);
        }
      cell_2->get_dof_indices (local_dof_indices);

      for (unsigned int i=0; i<dofs_per_cell; ++i)
        for (unsigned int j=0; j<dofs_per_cell; ++j){
          mass_matrix.block(1,1).add (local_dof_indices[i],
                                      local_dof_indices[j],
                                      local_mass(i,j));
          stif_matrix.block(1,1).add (local_dof_indices[i],
                                      local_dof_indices[j],
                                      local_stif(i,j));

        }
    }

  // Applying boundary conditions
  // Physic 1
  std::map<unsigned int,double> boundary_values1;
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            0,
                                            ZeroFunction<dim>(),
                                            boundary_values1);
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            1,
                                            ZeroFunction<dim>(),
                                            boundary_values1);
  MatrixTools::apply_boundary_values (boundary_values1,
                                      mass_matrix.block(0,0),
                                      newton_update.block(0),
                                      system_rhs.block(0));
  // Physic 2
  std::map<unsigned int,double> boundary_values2;
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            0,
                                            ZeroFunction<dim>(),
                                            boundary_values2);
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            1,
                                            ZeroFunction<dim>(),
                                            boundary_values2);
  MatrixTools::apply_boundary_values (boundary_values2,
                                      mass_matrix.block(1,1),
                                      newton_update.block(1),
                                      system_rhs.block(1));

}

template <int dim, int ord>
void twoPhysics<dim, ord>::init_system()
{
  // Initialize Solution as BV
  BoundaryValuesPhi<dim> BVP;
  BoundaryValuesT  <dim> BVT;
  Point<dim> p;

  solution.block(0) = BVP.value(p);
  solution.block(1) = BVT.value(p);

}

template <int dim, int ord>
void twoPhysics<dim, ord>::assemble_system (const unsigned int stage_i, const double time)
{

  //Assemble TR Residual
  previous_F[stage_i] = 0.0;
  previous_F[stage_i].add(1.0, solution, -1.0, old_solution);

  mass_matrix.vmult(tmp_vector, previous_F[stage_i]);

  assemble_ss_system (time);
  // Set Dirichlet BCs
  set_DC_Boundary ();

  system_rhs.block(0) *= 1.0;//parameters.velocity;
  system_rhs.block(1) *= 1.0;//parameters.rhoCp;
  system_matrix.block(0,0) *= 1.0;//parameters.velocity;
  system_matrix.block(0,1) *= 1.0;//parameters.velocity;
  system_matrix.block(1,0) *= 1.0;//parameters.rhoCp;
  system_matrix.block(1,1) *= 1.0;//parameters.rhoCp;

  previous_F[stage_i] = system_rhs;
  system_rhs.sadd (time_step * A[stage_i][stage_i], tmp_vector);

  for(unsigned int stage_j=0; stage_j < stage_i; stage_j++)
    system_rhs.add(time_step * A[stage_i][stage_j], previous_F[stage_j]);

  // Set F to -F
  system_rhs *= -1.0;

  system_matrix *= time_step * A[stage_i][stage_i];
  for(unsigned int i=0; i<parameters.n_physics; i++)
    system_matrix.block(i,i).add(1.0, mass_matrix.block(i,i));

  // Set Dirichlet BCs
  set_DC_Boundary ();
}

template <int dim, int ord>
void twoPhysics<dim, ord>::set_DC_Boundary ()
{
  // Applying boundary conditions
  // Physic 1
  std::map<unsigned int,double> boundary_values1;
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            0,
                                            ZeroFunction<dim>(),
                                            boundary_values1);
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            1,
                                            ZeroFunction<dim>(),
                                            boundary_values1);
  MatrixTools::apply_boundary_values (boundary_values1,
                                      system_matrix.block(0,0),
                                      newton_update.block(0),
                                      system_rhs.block(0));
  // Physic 2
  std::map<unsigned int,double> boundary_values2;
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            0,
                                            ZeroFunction<dim>(),
                                            boundary_values2);
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            1,
                                            ZeroFunction<dim>(),
                                            boundary_values2);
  MatrixTools::apply_boundary_values (boundary_values2,
                                      system_matrix.block(1,1),
                                      newton_update.block(1),
                                      system_rhs.block(1));

  // Physic Coupling 1->2
  FullMatrix<double> Identity (solution.block(1).size(), solution.block(1).size());
  for(unsigned int i = 0; i < Identity.m(); i++)
    Identity(i, i) = 1.0;

  typedef std::map<unsigned int, double>::iterator it_type;
  for(it_type iterator = boundary_values2.begin(); iterator != boundary_values2.end(); iterator++){
    Identity( iterator->first, iterator->first) = 0.0;
  }

  FullMatrix<double> tmpBlock10, tmpMatrix;
  tmpBlock10.copy_from(system_matrix.block(1,0));
  tmpMatrix.copy_from(system_matrix.block(1,0));
  Identity.mmult(tmpMatrix, tmpBlock10);
  
  system_matrix.block(1,0).copy_from(tmpMatrix);

  set_boundary_values ();
}

template <int dim, int ord>
void twoPhysics<dim, ord>::assemble_ss_system (const double time)
{
  system_matrix = 0;
  system_rhs = 0;

  // Assemble Flux Physic 1
  assemble_phi (time);

  // Assemble Temp Physic 2
  assemble_temp (time);

  // Assemble Contribution of Coupling between Physic 1 & 2 in block (1,0)
  const std::list<std::pair<typename DoFHandler<dim>::cell_iterator,
    typename DoFHandler<dim>::cell_iterator> > cell_list =
    GridTools::get_finest_common_cells (dof_handler1, dof_handler2);

  typename std::list<std::pair<typename DoFHandler<dim>::cell_iterator,
    typename DoFHandler<dim>::cell_iterator> >::const_iterator
    cell_iter = cell_list.begin();
  
  for (; cell_iter!=cell_list.end(); ++cell_iter){
    FullMatrix<double> unit_matrix (fe1.dofs_per_cell, fe2.dofs_per_cell);
    for(unsigned int i=0; i<unit_matrix.m(); ++i)
      unit_matrix(i, i) = 1;

    assemble_off_diagonal (cell_iter->first, cell_iter->second, unit_matrix);
  }
  system_matrix.block(1,0).vmult_add (system_rhs.block(1), solution.block(0));

}

template <int dim, int ord>
void twoPhysics<dim, ord>::assemble_phi (const double time)
{

  QGauss<dim>  quadrature_formula(ord+1);
  FEValues<dim> fe_values (fe1, quadrature_formula,
                           update_values | update_gradients |
                           update_quadrature_points | update_JxW_values);
  const unsigned int   dofs_per_cell = fe1.dofs_per_cell;
  const unsigned int   n_q_points    = quadrature_formula.size();

  FullMatrix<double>   cell_matrix (dofs_per_cell, dofs_per_cell);
  Vector<double>       cell_rhs (dofs_per_cell);
  std::vector<unsigned int> local_dof_indices (dofs_per_cell);

  // Verification Params
  RightHandSidePhi<dim> RHS(parameters);
  std::vector<double> rhs_values (n_q_points);
  RHS.set_time(time);

  // Assemble Contribution of Physic 1 in block (0,0)
  typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler1.begin_active(),
    endc = dof_handler1.end();
  for (; cell!=endc; ++cell)
    {
      fe_values.reinit (cell);

      RHS.value_list (fe_values.get_quadrature_points(),
                         rhs_values);

      cell_matrix = 0;
      cell_rhs = 0;

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
        for (unsigned int i=0; i<dofs_per_cell; ++i){
          for (unsigned int j=0; j<dofs_per_cell; ++j)
            cell_matrix(i,j) += ((parameters.Diffusion_Const *
                                  fe_values.shape_grad (i, q_point) *
                                  fe_values.shape_grad (j, q_point) +
                                  parameters.Absorption_Const *
                                  fe_values.shape_value(i, q_point) *
                                  fe_values.shape_value(j, q_point)) *
                                  fe_values.JxW (q_point));

          cell_rhs(i) -= ((fe_values.shape_value (i, q_point) *
                          (parameters.Flux_Source +
                           rhs_values[q_point])) *
                          fe_values.JxW (q_point));
        }
      }
      cell->get_dof_indices (local_dof_indices);

      for (unsigned int i=0; i<dofs_per_cell; ++i)
        for (unsigned int j=0; j<dofs_per_cell; ++j)
          system_matrix.block(0,0).add (local_dof_indices[i],
                             local_dof_indices[j],
                             cell_matrix(i,j));

      for (unsigned int i=0; i<dofs_per_cell; ++i)
        system_rhs.block(0)(local_dof_indices[i]) += cell_rhs(i);
    }
  system_matrix.block(0,0).vmult_add(system_rhs.block(0), solution.block(0));
}

template <int dim, int ord>
void twoPhysics<dim, ord>::assemble_temp(const double time)
{
  QGauss<dim>  quadrature_formula(ord+10);
  FEValues<dim> fe_values (fe2, quadrature_formula,
                           update_values | update_gradients | 
                           update_quadrature_points | update_JxW_values);
  const unsigned int   dofs_per_cell = fe2.dofs_per_cell;
  const unsigned int   n_q_points    = quadrature_formula.size();
  FullMatrix<double>   cell_matrix (dofs_per_cell, dofs_per_cell);
  Vector<double>       cell_rhs (dofs_per_cell);
  std::vector<unsigned int> local_dof_indices (dofs_per_cell);
  std::vector<double > old_solution_values(n_q_points);
  std::vector<Tensor<1,dim> > old_solution_gradients(n_q_points);

  // Verification Params
  RightHandSideT<dim> RHS(parameters);
  std::vector<double> rhs_values (n_q_points);
  RHS.set_time(time);

  // Assemble Linear Contribution of Physic 2 in block (1,1)
  typename DoFHandler<dim>::active_cell_iterator
    cell2 = dof_handler2.begin_active(),
    endc2 = dof_handler2.end();
  for (; cell2!=endc2; ++cell2)
    {
      fe_values.reinit (cell2);

      // Storing FE data from previous Newton iteration.
      fe_values.get_function_values(solution.block(1), old_solution_values);

      RHS.value_list (fe_values.get_quadrature_points(),
                       rhs_values);

      cell_matrix = 0;
      cell_rhs = 0;

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
        // Non-linear thermal conductivity.
        double ThermalCond_Constant = parameters.k0 + parameters.k1/ (parameters.k2 + old_solution_values[q_point]);
        for (unsigned int i=0; i<dofs_per_cell; ++i){
          for (unsigned int j=0; j<dofs_per_cell; ++j)
            cell_matrix(i,j) += ((ThermalCond_Constant *
                                  fe_values.shape_grad (i, q_point) *
                                  fe_values.shape_grad (j, q_point)) *
                                  fe_values.JxW (q_point));

          cell_rhs(i) -= ((fe_values.shape_value (i, q_point) *
                          (parameters.Heat_Source +
                          rhs_values[q_point])) *
                          fe_values.JxW (q_point));
        }
      }
      cell2->get_dof_indices (local_dof_indices);

      for (unsigned int i=0; i<dofs_per_cell; ++i)
        for (unsigned int j=0; j<dofs_per_cell; ++j)
          system_matrix.block(1,1).add (local_dof_indices[i],
                             local_dof_indices[j],
                             cell_matrix(i,j));

      for (unsigned int i=0; i<dofs_per_cell; ++i)
        system_rhs.block(1)(local_dof_indices[i]) += cell_rhs(i);
    }

  system_matrix.block(1,1).vmult_add(system_rhs.block(1), solution.block(1));

  // Assemble Non-Linear Contribution of Physic 2 in block (1,1)
  for (; cell2!=endc2; ++cell2)
    {
      fe_values.reinit (cell2);

      // Storing FE data from previous Newton iteration.
      fe_values.get_function_values(solution.block(1), old_solution_values);
      fe_values.get_function_gradients(solution.block(1), old_solution_gradients);

      cell_matrix = 0;

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
        // Non-linear thermal conductivity.
        double d_ThermalCond_Constant_dT = 0 - parameters.k1/((parameters.k2 + old_solution_values[q_point])*
                                                              (parameters.k2 + old_solution_values[q_point]));
        for (unsigned int i=0; i<dofs_per_cell; ++i){
          for (unsigned int j=0; j<dofs_per_cell; ++j)
            cell_matrix(i,j) += ((d_ThermalCond_Constant_dT *
                                  old_solution_gradients[q_point]*
                                  fe_values.shape_grad (i, q_point) *
                                  fe_values.shape_value(j, q_point)) *
                                  fe_values.JxW (q_point));
        }
      }
      cell2->get_dof_indices (local_dof_indices);

      for (unsigned int i=0; i<dofs_per_cell; ++i)
        for (unsigned int j=0; j<dofs_per_cell; ++j)
          system_matrix.block(1,1).add (local_dof_indices[i],
                             local_dof_indices[j],
                             cell_matrix(i,j));
    }
}

template <int dim, int ord>
void twoPhysics<dim, ord>::assemble_off_diagonal(const typename DoFHandler<dim>::cell_iterator &physic1,
                                                 const typename DoFHandler<dim>::cell_iterator &physic2,
                                                 const FullMatrix<double> prolongation_matrix)
{
  if(!physic1->has_children() && !physic2->has_children() ){

      QGauss<dim>   quadrature_formula(ord+1);
      FEValues<dim> fe_values1 (fe1, quadrature_formula,
                             update_values | update_gradients | update_JxW_values);
      FEValues<dim> fe_values2 (fe2, quadrature_formula,
                             update_values | update_gradients | update_JxW_values);

      const unsigned int   dofs_per_cell_1 = fe1.dofs_per_cell;
      const unsigned int   dofs_per_cell_2 = fe2.dofs_per_cell;
      const unsigned int   n_q_points    = quadrature_formula.size();

      FullMatrix<double>   cell_matrix3 (dofs_per_cell_2, dofs_per_cell_1);
      std::vector<unsigned int> local_dof_indices_1 (dofs_per_cell_1);
      std::vector<unsigned int> local_dof_indices_2 (dofs_per_cell_2);

      fe_values1.reinit (physic1);
      fe_values2.reinit (physic2);

      cell_matrix3 = 0;

      if(physic1->level() > physic2->level() ){
        for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
          for (unsigned int i=0; i<dofs_per_cell_2; ++i){
            for (unsigned int j=0; j<dofs_per_cell_1; ++j)
              cell_matrix3(i,j) += (-fe_values1.shape_value(j, q_point) *
                                    fe_values2.shape_value(i, q_point) *
                                    parameters.kappa *
                                    fe_values1.JxW (q_point));
          }
        }
      } else {
        for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
          for (unsigned int i=0; i<dofs_per_cell_2; ++i){
            for (unsigned int j=0; j<dofs_per_cell_1; ++j)
              cell_matrix3(i,j) += (-fe_values1.shape_value(j, q_point) *
                                    fe_values2.shape_value(i, q_point) *
                                    parameters.kappa *
                                    fe_values2.JxW (q_point));
          }
        }
      }

      FullMatrix<double> tmpM (dofs_per_cell_1, dofs_per_cell_2);
      // Apply prolongation matrix to cell matrix
      if(physic1->level() > physic2->level() ){
        prolongation_matrix.Tmmult (tmpM, cell_matrix3);
      } else {
        cell_matrix3.mmult (tmpM, prolongation_matrix);
      }
      cell_matrix3 = tmpM;
      
      physic2->get_dof_indices (local_dof_indices_2);

      for (unsigned int i=0; i<dofs_per_cell_2; ++i){
        physic1->get_dof_indices (local_dof_indices_1);
        for (unsigned int j=0; j<dofs_per_cell_1; ++j){
          system_matrix.block(1,0).add (local_dof_indices_2[i],
                             local_dof_indices_1[j],
                             cell_matrix3(i,j));
        }
      }

    } else {

      for(unsigned int child=0; child<GeometryInfo<dim>::max_children_per_cell; child++){
        FullMatrix<double> new_matrix (fe1.dofs_per_cell, fe2.dofs_per_cell);
        prolongation_matrix.mmult (new_matrix, fe1.get_prolongation_matrix(child));
        if(physic1->has_children()){
          assemble_off_diagonal (physic1->child(child), physic2, new_matrix);
        } else {
          assemble_off_diagonal (physic1, physic2->child(child), new_matrix);
        }
      }
    }
}

template <int dim, int ord>
void twoPhysics<dim, ord>::set_boundary_values ()
{
  std::map<unsigned int, double> boundary_values1;
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            0,
                                            BoundaryValuesPhi<dim>(),
                                            boundary_values1);
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            1,
                                            BoundaryValuesPhi<dim>(),
                                            boundary_values1);
  for(std::map<unsigned int, double>::const_iterator
        p = boundary_values1.begin();
        p != boundary_values1.end(); ++p)
    solution.block(0)(p->first) = p->second;

  std::map<unsigned int, double> boundary_values2;
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            0,
                                            BoundaryValuesT<dim>(),
                                            boundary_values2);
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            1,
                                            BoundaryValuesT<dim>(),
                                            boundary_values2);
  for(std::map<unsigned int, double>::const_iterator
        p = boundary_values2.begin();
        p != boundary_values2.end(); ++p)
    solution.block(1)(p->first) = p->second;
}

// Calculate Norms of Residuals
template <int dim, int ord>
void twoPhysics<dim, ord>::update_newton_norms ()
{
  residual_norm = system_rhs.l2_norm ();
  update_norm = newton_update.l2_norm();
}

// Linear Solver setup.
template <int dim, int ord>
void twoPhysics<dim, ord>::linear_solve ()
{
  // Declaration of Direct Variables
  Vector<double> b(system_rhs.size());
  Vector<double> x(newton_update.size());
  SparseDirectUMFPACK A_direct;

  // Declaration of GMRes Variables
  SolverControl                       solver_control (parameters.max_linear_iterations,
                                                      parameters.linear_tol);
  SolverGMRES<BlockVector<double> >   solver (solver_control);
  PreconditionJacobi<BlockSparseMatrix<double> > preconditioner;

  switch (parameters.solver){
    case Parameters::Solver::direct:
      b = system_rhs;
      x = newton_update;

      A_direct.initialize(system_matrix);
      A_direct.vmult(x, b);

      system_rhs = b;
      newton_update = x;
      break;
    case Parameters::Solver::gmres:
      preconditioner.initialize(system_matrix);
      solver.solve (system_matrix,
                    newton_update,
                    system_rhs,
                    preconditioner);
      if(Verbose_Output){
        std::cout << "    "
                  << solver_control.last_step()
                  << " GMRES iterations needed to obtain convergence."
                  << std::endl;
      }
      break;
    default:
      AssertThrow(false, ExcMessage("Solver Not Supported"));
  }

  constraints1.distribute (newton_update.block(0));
  constraints2.distribute (newton_update.block(1));

  double alpha = determine_step_length();
  solution.add (alpha, newton_update);

}

// Newton Solver
template <int dim, int ord>
void twoPhysics<dim, ord>::newton_solve(const unsigned int stage_i, const double time)
{
  Timer timer;
  timer.start();
  if(timeInfo_Output) printf("\n");

  int iter = 1;
  bool done = false;
  double Res_tol;//, Upd_tol;
  do{
        if(Verbose_Output) std::cout << "  Newton Iteration " << iter << std::endl;
        if(timeInfo_Output) printf("  Assemble System   "); timer.restart();
      assemble_system (stage_i, time);
        if(timeInfo_Output) printf("  Time: %7.3f [s]\n", timer());
        if(timeInfo_Output) printf("  Linear Solve      "); timer.restart();
      linear_solve ();
        if(timeInfo_Output) printf("  Time: %7.3f [s]\n", timer());
      update_newton_norms ();
      if(iter == 1){
        Res_tol = parameters.nonLinear_atol + parameters.nonLinear_rtol * residual_norm;
//        Upd_tol = parameters.nonLinear_atol + parameters.nonLinear_rtol * update_norm;

      }
        if(Verbose_Output){
          std::cout << "    Residual Norm: " << residual_norm << std::endl;
          std::cout << "    Update Norm: " << update_norm << std::endl;
        }
      iter++ ;

//      if(residual_norm < parameters.nonLinear_tol || update_norm < parameters.update_tol){
//      if(residual_norm < Res_tol || update_norm < Upd_tol){
      if(residual_norm < Res_tol){
        done = true;
        if(timeInfo_Output) printf("\n");
      }
      if(iter >= parameters.max_nonLinear_iterations)
        done = true;
  } while (!done);
}

// Damping coefficient for Newton's Method
template <int dim, int ord>
double twoPhysics<dim, ord>::determine_step_length()
{
  return 1;
}

// h-AMR
template <int dim, int ord>
void twoPhysics<dim, ord>::refine_grid ()
{

  SolutionTransfer<dim> transfer1 (dof_handler1);
  SolutionTransfer<dim> transfer2 (dof_handler2);
  transfer1.prepare_for_coarsening_and_refinement(solution.block(0));
  transfer2.prepare_for_coarsening_and_refinement(solution.block(1));

  if(parameters.refineScheme == Parameters::AllParameters::hAMR){
    // Physic 1
    Vector<float> estimated_error_per_cell_1 (triangulation1.n_active_cells());
    KellyErrorEstimator<dim>::estimate (dof_handler1,
                                        QGauss<dim-1>(ord+1),
                                        typename FunctionMap<dim>::type(),
                                        solution.block(0),
                                        estimated_error_per_cell_1);
    GridRefinement::refine_and_coarsen_fixed_number (triangulation1,
                                                     estimated_error_per_cell_1,
                                                     0.5, 0.0);

    triangulation1.execute_coarsening_and_refinement ();


    // Physic 2
    Vector<float> estimated_error_per_cell_2 (triangulation2.n_active_cells());
    KellyErrorEstimator<dim>::estimate (dof_handler2,
                                        QGauss<dim-1>(ord+1),
                                        typename FunctionMap<dim>::type(),
                                        solution.block(1),
                                        estimated_error_per_cell_2);
    GridRefinement::refine_and_coarsen_fixed_number (triangulation2,
                                                     estimated_error_per_cell_2,
                                                     0.5, 0.0);
    triangulation2.execute_coarsening_and_refinement ();
  } else if (parameters.refineScheme == Parameters::AllParameters::global){
    triangulation1.refine_global(1);
    triangulation2.refine_global(1);
  }

  Vector<double> tmp1(dof_handler1.n_dofs());
  Vector<double> tmp2(dof_handler2.n_dofs());
  transfer1.interpolate(solution.block(0), tmp1);
  transfer2.interpolate(solution.block(1), tmp2);
  solution.block(0) = tmp1;
  solution.block(1) = tmp2;

}

// Display solution in gnuplot data file
template <int dim, int ord>
void twoPhysics<dim, ord>::output_results () const
{
  std::vector<std::string> solution_names;
  solution_names.push_back("Flux");
  solution_names.push_back("Temp");
  {
    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler1);
    data_out.add_data_vector (solution.block(0), solution_names[0]);
    data_out.build_patches ();

    std::ofstream output ("solution-Flux.gpl");
    data_out.write_gnuplot (output);  
  }{
    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler2);
    data_out.add_data_vector (solution.block(1), solution_names[1]);
    data_out.build_patches ();

    std::ofstream output ("solution-Temp.gpl");
    data_out.write_gnuplot (output); 
  }

}

// Output adapted grid if in 2D
template <int dim, int ord>
void twoPhysics<dim, ord>::output_grid (const unsigned int cycle) const
{
  Assert (cycle < 100, ExcNotImplemented());
  if(dim == 2){
    std::string filename = "Flux-grid-";
    if(cycle < 10)
      filename += "0";
    std::stringstream ss;
    ss << cycle;
    filename += ss.str();
    filename += ".eps";

    std::ofstream output (filename.c_str());

    GridOut grid_out;
    grid_out.write_eps (triangulation1, output);
  }
  if(dim == 2){
    std::string filename = "Temp-grid-";
    if(cycle < 10)
      filename += "0";
    std::stringstream ss;
    ss << cycle;
    filename += ss.str();
    filename += ".eps";

    std::ofstream output (filename.c_str());

    GridOut grid_out;
    grid_out.write_eps (triangulation2, output);
  }
}

template <int dim, int ord>
void twoPhysics<dim, ord>::process_solution(const double time, const bool final)
{
double total_error = 0.0;
  {
  // Error estimate Phi
  Vector<float> difference_per_cell_1 (triangulation1.n_active_cells());
  SolutionPhi<dim> ExactSolutionPhi(parameters);
  ExactSolutionPhi.set_time(time);
  VectorTools::integrate_difference (dof_handler1,
                                     solution.block(0),
                                     ExactSolutionPhi,
                                     difference_per_cell_1,
                                     QGauss<dim>(ord+2),
                                     VectorTools::L2_norm);
  const double L2_error = difference_per_cell_1.l2_norm();

  VectorTools::integrate_difference (dof_handler1,
                                     solution.block(0),
                                     SolutionPhi<dim>(parameters),
                                     difference_per_cell_1,
                                     QGauss<dim>(ord+2),
                                     VectorTools::H1_seminorm);
//  const double H1_error = difference_per_cell_1.l2_norm();
    if(final){
/*
  Point<dim> pt;
  switch(dim){
  case 1:
    pt =  Point<dim> (parameters.length/2.);
    break;
  case 2:
    pt =  Point<dim> (parameters.length/2.,parameters.length/2.);
    break;
  case 3:
    pt = Point<dim> (parameters.length/2.,parameters.length/2.,parameters.length/2.);
    break;
  }
 Vector<double> diff (1);
 VectorTools::point_difference (dof_handler1,
                                solution.block(0),
                                ExactSolutionPhi,
                                diff,
				pt);
 std::cout << "pt diff = : " << diff(0) << std::endl;

 VectorTools::point_value (dof_handler1,
                           solution.block(0),
                           pt,                           
                           diff);
 std::cout << "pt value FEM solutuion = : " << diff(0) << std::endl;

 std::cout << "pt value of EXACT sol  = : " << ExactSolutionPhi.value(pt) << std::endl;
*/
      //printf("\tFlux Solution Error: %7.3e", L2_error);
      convergence_tablePHI.add_value("Flux Error", L2_error);
      total_error += L2_error;
    }
  }{
  // Error Estimate T 
  Vector<float> difference_per_cell_2 (triangulation2.n_active_cells());
  SolutionT<dim> ExactSolutionT(parameters);
  ExactSolutionT.set_time(time);
  VectorTools::integrate_difference (dof_handler2,
                                     solution.block(1),
                                     ExactSolutionT,
                                     difference_per_cell_2,
                                     QGauss<dim>(ord+2),
                                     VectorTools::L2_norm);
  const double L2_error = difference_per_cell_2.l2_norm();

  VectorTools::integrate_difference (dof_handler2,
                                     solution.block(1),
                                     SolutionT<dim>(parameters),
                                     difference_per_cell_2,
                                     QGauss<dim>(ord+2),
                                     VectorTools::H1_seminorm);
//  const double H1_error = difference_per_cell_2.l2_norm();
    if(final){
/*
  Point<dim> pt;
  switch(dim){
  case 1:
    pt =  Point<dim> (parameters.length/2.);
    break;
  case 2:
    pt =  Point<dim> (parameters.length/2.,parameters.length/2.);
    break;
  case 3:
    pt = Point<dim> (parameters.length/2.,parameters.length/2.,parameters.length/2.);
    break;
  }
 Vector<double> diff (1);
 VectorTools::point_difference (dof_handler2,
                                solution.block(1),
                                ExactSolutionT,
                                diff,
                                pt);
 std::cout << "pt diff = : " << diff(0) << std::endl;

 VectorTools::point_value (dof_handler2,
                           solution.block(1),
                           pt,                           
                           diff);
 std::cout << "pt value FEM solutuion = : " << diff(0) << std::endl;

 std::cout << "pt value of EXACT sol  = : " << ExactSolutionT.value(pt) << std::endl;
*/
      //printf("\tTemp Solution Error: %7.3e\n", L2_error);
      convergence_tablePHI.add_value("Temp Error", L2_error);
      total_error += L2_error;
      convergence_tableT.add_value("Total Error", total_error);
    }
  }
}

// Problem driver
template <int dim, int ord>
void twoPhysics<dim, ord>::run ()
{
  Timer timer, runtime, component;
  timer.start();
  runtime.start();
  component.start();

  double init_time = parameters.initial_time;
  double end_time  = parameters.end_time;
  unsigned int n_steps = parameters.number_time_steps;

  make_grid ();
  setup_system ();
  init_system();

  SolutionPhi<dim> ExactSolutionPhi(parameters);
  SolutionT<dim>   ExactSolutionT  (parameters);

  std::vector<unsigned int> N_cycle (1, 0.0);
  for(unsigned int i=0; i<N_cycle.size(); i++)
    N_cycle[i] = std::pow(2.0,i)*n_steps;

  for(unsigned int cycle=0; cycle < N_cycle.size(); cycle++){

  std::cout << "Number of Time Steps: " << N_cycle[cycle] << std::endl;

  time_step = (end_time - init_time)/N_cycle[cycle];
  current_time = init_time;

  ExactSolutionPhi.set_time(current_time);
  ExactSolutionT  .set_time(current_time);

  VectorTools::interpolate(dof_handler1,
                           ExactSolutionPhi, 
                           solution.block(0));
  VectorTools::interpolate(dof_handler2,
                           ExactSolutionT, 
                           solution.block(1));
  process_solution (current_time, false);
  old_solution = solution;

  assemble_mass_matrix ();

  for(unsigned int step = 1; step <= N_cycle[cycle]; step++){
//    printf("Problem Time: %f\n",current_time);
    for(unsigned int refine = 0; refine < parameters.cycles; refine++){
    std::cout << "  Time: " << step*time_step << "  Cycle: " << refine << std::endl;

      for(unsigned int stage = 0; stage < n_stage; stage++){
  //      printf("Stage: %i\n",stage+1);
        newton_solve (stage, current_time + C[stage] * time_step);

  //      printf("  Time: %7.3f [s]\n", timer());
        timer.restart();
      }

      // Update New solution
      system_rhs = 0.0;
      system_matrix = 0.0;
      mass_matrix.vmult(system_rhs, old_solution);
      for(unsigned int stage_i =0; stage_i<n_stage; stage_i++)
        system_rhs.add(-time_step * B[stage_i], previous_F[stage_i]);

      system_matrix.copy_from(mass_matrix);
      linear_solve ();
      solution = newton_update;

      bool final_step = false;
      if(step == N_cycle[cycle]){
        convergence_tablePHI.add_value("Steps", N_cycle[cycle]);
        convergence_tableT.add_value("Steps", N_cycle[cycle]);
        final_step = true;
      }
      current_time += time_step;
      old_solution = solution;

      process_solution (current_time, final_step);
    }
  }

  }
  ExactSolutionPhi.set_time(end_time);
  ExactSolutionT  .set_time(end_time);

  VectorTools::interpolate(dof_handler1,
                           ExactSolutionPhi, 
                           newton_update.block(0));
  VectorTools::interpolate(dof_handler2,
                           ExactSolutionT, 
                           newton_update.block(1));
  std::vector<std::string> solution_names;
  solution_names.push_back("Flux");
  solution_names.push_back("Temp");
  {
    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler1);
    data_out.add_data_vector (newton_update.block(0), solution_names[0]);
    data_out.build_patches ();

    std::ofstream output ("exact-solution-Flux.gpl");
    data_out.write_gnuplot (output);  
  }{
    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler2);
    data_out.add_data_vector (newton_update.block(1), solution_names[1]);
    data_out.build_patches ();

    std::ofstream output ("exact-solution-Temp.gpl");
    data_out.write_gnuplot (output); 
  }


  output_results ();
  printf("Runtime: %7.3f [s]\n",runtime());

  convergence_tablePHI.set_precision("Flux Error", 4);
  convergence_tablePHI.set_precision("Temp Error", 4);
  convergence_tableT  .set_precision("Total Error", 4);
  convergence_tablePHI.set_scientific("Flux Error", true);
  convergence_tablePHI.set_scientific("Temp Error", true);
  convergence_tableT  .set_scientific("Total Error", true);
//  convergence_tablePHI.evaluate_convergence_rates("Flux Error", ConvergenceTable::reduction_rate_log2);
//  convergence_tablePHI.evaluate_convergence_rates("Temp Error", ConvergenceTable::reduction_rate_log2);
  std::cout << std::endl;
  convergence_tablePHI.write_text(std::cout);
  std::cout << std::endl;
  std::cout << std::endl;
  convergence_tableT  .write_text(std::cout);
  std::cout << std::endl;

}
} // End namespace


int main ()
{
  try{
    using namespace BlockPhysics;
    const int dimension = 1;
    const int polyOrder = 2;

    deallog.depth_console (0);

    std::string input_filename = "input_params.prm";
    ParameterHandler prm;
    Parameters::AllParameters::declare_parameters (prm);
    prm.read_input (input_filename.c_str());
    Parameters::AllParameters parameters;
    parameters.parse_parameters (prm);

    twoPhysics<dimension,polyOrder> laplace_problem(parameters);
    laplace_problem.run ();

  } catch (std::exception &exc){
    std::cerr << std::endl << std::endl
              << "----------------------------------------------------"
              << std::endl;
    std::cerr << "Exception on processing: " << std::endl
              << exc.what() << std::endl
              << "Aborting!" << std::endl
              << "----------------------------------------------------"
              << std::endl;

    return 1;
  } catch (...){
    std::cerr << std::endl << std::endl
              << "----------------------------------------------------"
              << std::endl;
    std::cerr << "Unknown exception!" << std::endl
              << "Aborting!" << std::endl
              << "----------------------------------------------------"
              << std::endl;
    return 1;
  }
  

  return 0;
}

