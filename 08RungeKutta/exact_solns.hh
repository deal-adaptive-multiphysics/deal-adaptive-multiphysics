#ifndef EXACT_SOLNS_HH
#define EXACT_SOLNS_HH

using namespace dealii;

//===============================================================================
//  MMS Solutions
//===============================================================================
// MMS for PHI
template <int dim>
class SolutionPhi : public Function<dim>
{
  public:
    SolutionPhi (const Parameters::AllParameters params) : Function<dim>(),
                                                           params(params) {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;
    virtual Tensor<1,dim> gradient (const Point<dim>  &p,
                                    const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double SolutionPhi<dim>::value (const Point<dim> &p,
                             const unsigned int) const
{
  double value = params.C_phi;
  double time = this->get_time();

  switch(params.exact_solution_type){
    // phi = C_phi * sin(PI * x / L)
    case Parameters::AllParameters::sin:

      for(unsigned int i=0; i<dim; i++){
        value *= sin(p[i] * M_PI / params.length);
      }
      break;
    // phi = C_phi * ( 1 - exp(-beta * x) - exp(beta * (x-L) )
    case Parameters::AllParameters::exp:
      for(unsigned int i=0; i<dim; i++){
        value *= 1.0 - exp(-params.beta * p[i]) - exp(params.beta *(p[i] - params.length));
      }
      break;
    // phi = - C_phi * x * (x-L) * exp(tau * t)
    case Parameters::AllParameters::quadt:
      value *= std::pow(-1.0,dim);
      for(unsigned int i=0; i<dim; i++){
        value *= p[i] * (p[i] - params.length);
      }
      value *= exp(params.t_phi * time);
      break;
    default:
      AssertThrow(false, ExcMessage("Exact Solution Not Supported"));
  }

  return value;
}

template <int dim>
Tensor<1,dim> SolutionPhi<dim>::gradient (const Point<dim>  &p,
                                          const unsigned int) const
{
  Tensor<1,dim> value;
  double time = this->get_time();

  switch(params.exact_solution_type){
    // phi = C_phi * sin(PI * x / L)
    case Parameters::AllParameters::sin:

      for(unsigned int i=0; i<dim; i++){
        value[i] = params.C_phi * (M_PI / params.length);
        for(unsigned int j=0; j<dim; j++){
          if(j == i){
            value[i] *= cos(M_PI * p[j] / params.length);
          } else {
            value[i] *= sin(M_PI * p[j] / params.length);
          }
        }
      }
      break;
    // phi = C_phi * ( 1 - exp(-beta * x) - exp(beta * (x-L) )
    case Parameters::AllParameters::exp:

      for(unsigned int i=0; i<dim; i++){
        value[i] = params.C_phi * params.beta;
        for(unsigned int j=0; j<dim; j++){
          if(j == i){
            value[i] *= exp(-params.beta*p[j]) - exp(params.beta*(p[j] - params.length));
          } else {
            value[i] *= 1.0 - exp(-params.beta*p[j]) - exp(params.beta*(p[j] - params.length));
          }
        }
      }
      break;
    // phi = - C_phi * x * (x-L) * exp(tau * t)
    case Parameters::AllParameters::quadt:
      
      for(unsigned int i=0; i<dim; i++){
        value[i] = std::pow(-1.0,dim) * params.C_phi * exp(params.t_phi * time);
        for(unsigned int j=0; j<dim; j++){
          if(j == i){
            value[i] *= 2.0 * p[j] - params.length;
          } else {
            value[i] *= p[j] * ( p[j] - params.length );
          }
        }
      }
      break;
    default:
      AssertThrow(false, ExcMessage("Exact Solution Not Supported"));
  }
  return value;
}

template <int dim>
class RightHandSidePhi : public Function<dim>
{
  public:
    RightHandSidePhi (const Parameters::AllParameters params) : Function<dim>(),
                                                                params(params) {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double RightHandSidePhi<dim>::value (const Point<dim>  &p,
                                     const unsigned int) const
{
  double value1, value2, value3;
  double time = this->get_time();
  double tmp;

  switch(params.exact_solution_type){
    // phi = C_phi * sin(PI * x / L)
    case Parameters::AllParameters::sin:

      value1 = params.C_phi;
      value2 = 0.0;

      for(unsigned int i=0; i<dim; i++){
        value1 *= sin(p[i] * M_PI / params.length);
      }

      value2 = params.Diffusion_Const * (M_PI / params.length) * (M_PI / params.length) * dim +
               params.Absorption_Const;

      value3 = value1 * value2 - params.Flux_Source;
      break;
    // phi = C_phi * ( 1 - exp(-beta * x) - exp(beta * (x-L) )
    case Parameters::AllParameters::exp:

      for(unsigned int i=0; i<dim; i++){
        tmp = 1.0;
        for(unsigned int j=0; j<dim; j++){
          if(j==i)
            tmp *= exp(-params.beta * p[j]) + exp(params.beta*(p[j] - params.length));
          else
            tmp *= 1.0 - exp(-params.beta * p[j]) - exp(params.beta*(p[j] - params.length));
        }
        value1 += tmp;
      }
      value1 = params.C_phi * params.beta * params.beta * params.Diffusion_Const * tmp;

      value2 = params.C_phi * params.Absorption_Const;
      for(unsigned int i=0; i<dim; i++)
        value2 *= 1.0 - exp(-params.beta*p[i]) - exp(params.beta*(p[i] - params.length));

      value3 = value1 + value2 - params.Flux_Source;
      break;
    // phi = - C_phi * x * (x-L) * exp(tau * t)
    case Parameters::AllParameters::quadt:

      value1 = 0.0;
      for(unsigned int i=0; i<dim; i++){
        tmp = 1.0;
        for(unsigned int j=0; j<dim; j++){
          if(j==i)
            tmp *= 2.0;
          else
            tmp *= p[j] * (p[j] - params.length);
        }
        value1 += tmp;
      }
      value1 *= std::pow(-1.0,dim+1) * params.Diffusion_Const * params.C_phi * exp(params.t_phi * time);

      value2 = std::pow(-1.0,dim) * params.C_phi;
      for(unsigned int i=0; i<dim; i++){
        value2 *= p[i] * (p[i] - params.length);
      }
      value2 *= exp(params.t_phi * time) * (params.Absorption_Const + params.t_phi);

      value3 = value1 + value2 - params.Flux_Source;
      break;
    default:
      AssertThrow(false, ExcMessage("Exact Solution Not Supported"));
  }
  return value3;
}

// MMS for T
template <int dim>
class SolutionT : public Function<dim>
{
  public:
    SolutionT (const Parameters::AllParameters params) : Function<dim>(),
                                                         params(params) {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;
    virtual Tensor<1,dim> gradient (const Point<dim>  &p,
                                    const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double SolutionT<dim>::value (const Point<dim> &p,
                              const unsigned int) const
{
  double value = params.C_T;
  double time = this->get_time();

  switch(params.exact_solution_type){
    // T = C_T * sin(PI * x / L)
    case Parameters::AllParameters::sin:

      for(unsigned int i=0; i<dim; i++){
        value *= sin(p[i] * M_PI / params.length);
      }
      break;
    // T = C_T * ( 1 - exp(-beta * x) - exp(beta * (x-L) )
    case Parameters::AllParameters::exp:
      for(unsigned int i=0; i<dim; i++){
        value *= 1.0 - exp(-params.beta * p[i]) - exp(params.beta *(p[i] - params.length));
      }
      break;
    // T = - C_T * x * (x-L) * exp(tau * time)
    case Parameters::AllParameters::quadt:
      value *= std::pow(-1.0,dim);
      for(unsigned int i=0; i<dim; i++){
        value *= p[i] * (p[i] - params.length);
      }
      value *= exp(params.t_T * time);
      break;
    default:
      AssertThrow(false, ExcMessage("Exact Solution Not Supported"));
  }

  return value;
}

template <int dim>
Tensor<1,dim> SolutionT<dim>::gradient (const Point<dim>  &p,
                                        const unsigned int) const
{
  Tensor<1,dim> value;
  double time = this->get_time();

  switch(params.exact_solution_type){
    // T = C_T * sin(PI * x / L)
    case Parameters::AllParameters::sin:

      for(unsigned int i=0; i<dim; i++){
        value[i] = params.C_T * (M_PI / params.length);
        for(unsigned int j=0; j<dim; j++){
          if(j == i){
            value[i] *= cos(M_PI * p[j] / params.length);
          } else {
            value[i] *= sin(M_PI * p[j] / params.length);
          }
        }
      }
      break;
    // T = C_T * ( 1 - exp(-beta * x) - exp(beta * (x-L) )
    case Parameters::AllParameters::exp:

      for(unsigned int i=0; i<dim; i++){
        value[i] = params.C_T * params.beta;
        for(unsigned int j=0; j<dim; j++){
          if(j == i){
            value[i] *= exp(-params.beta*p[j]) - exp(params.beta*(p[j] - params.length));
          } else {
            value[i] *= 1.0 - exp(-params.beta*p[j]) - exp(params.beta*(p[j] - params.length));
          }
        }
      }
      break;
    // T = - C_T * x * (x-L) * exp(tau * time)
    case Parameters::AllParameters::quadt:
      
      for(unsigned int i=0; i<dim; i++){
        value[i] = std::pow(-1.0,dim) * params.C_T * exp(params.t_T * time);
        for(unsigned int j=0; j<dim; j++){
          if(j == i){
            value[i] *= 2.0 * p[j] - params.length;
          } else {
            value[i] *= p[j] * ( p[j] - params.length );
          }
        }
      }
      break;
    default:
      AssertThrow(false, ExcMessage("Exact Solution Not Supported"));
  }
  return value;
}

template <int dim>
class RightHandSideT : public Function<dim>
{
  public:
    RightHandSideT (const Parameters::AllParameters params) : Function<dim>(),
                                                              params(params) {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double RightHandSideT<dim>::value (const Point<dim>  &p,
                                     const unsigned int) const
{
  double value;
  double time = this->get_time();
  double L = params.length;
  double C_phi = params.C_phi;
  double t_phi = params.t_phi;
  double C_T = params.C_T;
  double t_T = params.t_T;
  double Heat_Source = params.Heat_Source;
  double kappa = params.kappa;
  double k0 = params.k0;
  double k1 = params.k1;
  double k2 = params.k2;

  switch(params.exact_solution_type){
    // T = C_T * sin(PI * x / L)
    case Parameters::AllParameters::sin:
      if(dim == 1){
        value = -1.0* Heat_Source - C_phi * kappa * sin((M_PI*p[0])/L) + C_T * 1/(L*L) * (M_PI*M_PI) * sin((M_PI*p[0])/L) *
                (k0 +    k1 / (k2 + C_T * sin((M_PI*p[0])/L))) + (C_T*C_T) * 1/(L*L) * (M_PI*M_PI) * k1 *
                 std::pow(cos((M_PI*p[0])/L),2.0) * 1/std::pow(k2 + C_T * sin((M_PI*p[0])/L),2.0);
      } else if(dim == 2){
        value = -1.0* Heat_Source - C_phi * kappa * sin((M_PI*p[0])/L)*sin((M_PI*p[1])/L) + C_T*1/(L*L)*(M_PI*M_PI) * 
                sin((M_PI*p[0])/L)*sin((M_PI*p[1])/L) * (k0 + k1/(k2 + C_T * sin((M_PI*p[0])/L) * sin((M_PI*p[1])/L))) * 
                2.0 + (C_T*C_T)*1/(L*L)*(M_PI*M_PI) * k1 * std::pow(cos((M_PI*p[0])/L),2.0) * std::pow(sin((M_PI*p[1])/L),2.0) * 
                1/std::pow(k2 + C_T * sin((M_PI*p[0])/L) * sin((M_PI*p[1])/L),2.0) + (C_T*C_T)*1/(L*L)*(M_PI*M_PI) * 
                k1 * std::pow(cos((M_PI*p[1])/L),2.0) * std::pow(sin((M_PI*p[0])/L),2.0) * 1/std::pow(k2 + C_T * sin((M_PI*p[0])/L) * 
                sin((M_PI*p[1])/L),2.0);
      } else if(dim == 3){
        value = -1.0* Heat_Source - C_phi * kappa * sin((M_PI*p[0])/L) * sin((M_PI*p[1])/L) * sin((M_PI*p[2])/L) + C_T * 1/(L*L) *
                (M_PI*M_PI) * sin((M_PI*p[0])/L) * sin((M_PI*p[1])/L) * sin((M_PI*p[2])/L) * (k0 + k1/(k2 + C_T *
                sin((M_PI*p[0])/L) * sin((M_PI*p[1])/L) * sin((M_PI*p[2])/L))) * 3.0 + (C_T*C_T)*1/(L*L) * (M_PI*M_PI) * k1 * 
                std::pow(cos((M_PI*p[0])/L),2.0) * std::pow(sin((M_PI*p[1])/L),2.0) * std::pow(sin((M_PI*p[2])/L),2.0) * 1/std::pow(k2 + C_T * 
                sin((M_PI*p[0])/L) * sin((M_PI*p[1])/L) * sin((M_PI*p[2])/L),2.0) + (C_T*C_T) * 1/(L*L) * (M_PI*M_PI) * k1 * 
                std::pow(cos((M_PI*p[1])/L),2.0) * std::pow(sin((M_PI*p[0])/L),2.0) * std::pow(sin((M_PI*p[2])/L),2.0) * 1/std::pow(k2 + C_T * 
                sin((M_PI*p[0])/L) * sin((M_PI*p[1])/L) * sin((M_PI*p[2])/L),2.0) + (C_T*C_T) * 1/(L*L) * (M_PI*M_PI) * k1 * 
                std::pow(cos((M_PI*p[2])/L),2.0) * std::pow(sin((M_PI*p[0])/L),2.0) * std::pow(sin((M_PI*p[1])/L),2.0) * 1/std::pow(k2 + C_T * 
                sin((M_PI*p[0])/L) * sin((M_PI*p[1])/L) * sin((M_PI*p[2])/L),2.0);
      }
      break;
    // T = C_T * ( 1 - exp(-beta * x) - exp(beta * (x-L) )
    case Parameters::AllParameters::exp:
      AssertThrow(false, ExcMessage("Exact Solution Not Supported"));
      if(dim == 1){
        value = 0.0;
      } else if(dim == 2){
        value = 0.0;
      } else if(dim == 3){
        value = 0.0;
      }
      break;
    // T = - C_T * x * (x-L) * exp(tau * time)
    case Parameters::AllParameters::quadt:
      if(dim == 1){
        value = -Heat_Source+C_T*exp(time*t_T)*(k0+k1/(k2+C_T*p[0]*exp(time*t_T)*(L-p[0])))*2.0+k1*1/std::pow(k2+C_T*p[0]*exp(time*t_T) * 
                (L-p[0]),2.0)*std::pow(C_T*p[0]*exp(time*t_T)-C_T*exp(time*t_T)*(L-p[0]),2.0)-C_phi*kappa*p[0]*exp(time*t_phi)*(L-p[0]) + 
                C_T*t_T*p[0]*exp(time*t_T)*(L-p[0]);
      } else if(dim == 2){
        value = -Heat_Source+k1*1/std::pow(k2+C_T*p[0]*p[1]*exp(time*t_T)*(L-p[0])*(L-p[1]),2.0)*std::pow(C_T*p[0]*p[1]*exp(time*t_T)*(L-p[0])-C_T*p[0]*exp(time*t_T)*(L-p[0])*(L-p[1]),2.0)+k1*1/std::pow(k2+C_T*p[0]*p[1]*exp(time*t_T)*(L-p[0])*(L-p[1]),2.0)*std::pow(C_T*p[0]*p[1]*exp(time*t_T)*(L-p[1])-C_T*p[1]*exp(time*t_T)*(L-p[0])*(L-p[1]),2.0)+C_T*p[0]*exp(time*t_T)*(L-p[0])*(k0+k1/(k2+C_T*p[0]*p[1]*exp(time*t_T)*(L-p[0])*(L-p[1])))*2.0+C_T*p[1]*exp(time*t_T)*(L-p[1])*(k0+k1/(k2+C_T*p[0]*p[1]*exp(time*t_T)*(L-p[0])*(L-p[1])))*2.0-C_phi*kappa*p[0]*p[1]*exp(time*t_phi)*(L-p[0])*(L-p[1])+C_T*t_T*p[0]*p[1]*exp(time*t_T)*(L-p[0])*(L-p[1]);
      } else if(dim == 3){
        value = 0.0;
      }
      break;
    default:
      AssertThrow(false, ExcMessage("Exact Solution Not Supported"));
  }
  return value;
}


#endif // EXACT_SOLNS_HH
