# Listing of Parameters
# ---------------------
subsection Exact Solution Propterties
  # State the type of exact solution.
  # Choices are <sin|exp|sint|expt|quadt>.
  set Exact Solution Type = sin
end


subsection Reporting
  # Whether to Print a full convergence chart or not
  set Full Conv Chart  = false

  # Output the sparsity pattern or not.
  set Sparsity Pattern = false

  # Display Time info.
  # Choices are <quiet|verbose>.
  set Time Info        = quiet
end


subsection Time Parameters
  # Final Time
  set End Time     = 1.0

  # Initial Time
  set Initial Time = 0.0

  # Runge Kutta Method used.
  # Choices are <SDIRK22|SDIRK33|BE|CN|RK4>
  set RK Method    = SDIRK33

  # Number of steps to be taken
  set Time Steps   = 1
end


subsection domain
  # Number of Physics in Problem
  set Number of Physics = 2

  # Parameter to determine shape of diffusive MMS
  set beta              = 10.0

  # Length of Domain
  set length            = 20.0
end


subsection linear solver
  # Linear Solver Tolerance
  set Linear tolerance      = 1E-12

  # Maximum number of linear iterations
  set Max linear iterations = 1000000

  # Which Linear Solver Method is used?
  # Choices are <direct|gmres>.
  set Solver                = direct
end


subsection neutronics properties
  # Absorption Constant for Neutrons
  set Absorption Constant = 3

  # Diffusion Constant for Neutrons
  set Diffusion Constant  = 2

  # Maximum Flux for MMS
  set Flux Amplitude      = 2.5

  # Frequency of Oscillation
  set Flux Frequency      = 5.0

  # Source for Flux
  set Flux Source         = 30

  # Time Constant for Exponential Growth
  set Flux Time Constant  = 0.5
end


subsection nonlinear solver
  # Maximum number of nonlinear iterations
  set max nonlinear iterations     = 50

  # Nonlinear Absolute Tolerance
  set nonlinear absolute tolerance = 1E-10

  # Nonlinear Relative Tolerance
  set nonlinear relative tolerance = 1E-10

  # State whether nonlinear output should be printed.
  # Choices are <quiet|verbose>.
  set output                       = quiet
end


subsection refinement
  # Number of initial global refinements.
  # Minimum is 2.
  set Initial Refinements = 3

  # State the type of refinement scheme.
  # Choices are <global|hAMR>.
  set Refinement Type     = global

  # Number of Refinement Cycles
  set cycles              = 4
end


subsection temperature properties
  # First parameter for conductivity
  set Conductivity k0    = 5.0

  # Second parameter for conductivity
  # Set to Zero for Linear Problem
  set Conductivity k1    = 2049.59

  # Third parameter for conductivity
  set Conductivity k2    = 200.0

  # Maximum Temp for MMS
  set Temp Amplitude     = 6.0

  # Frequency of Oscillation
  set Temp Frequency     = 10.0

  # Time Constant for Exponential Growth
  set Temp Time Constant = -1

  # Source to Drive Temperature
  set Thermal Source     = 3.0

  # Thermal Diffusivity
  set kappa              = 0.1
end


