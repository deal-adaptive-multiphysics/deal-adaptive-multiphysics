/* Author: Wolfgang Bangerth, University of Heidelberg, 2000 */

/*    $Id: step-6.cc 25840 2012-08-09 20:22:00Z bangerth $       */
/*                                                                */
/*    Copyright (C) 2000, 2001, 2002, 2003, 2004, 2006, 2007, 2008, 2010, 2011, 2012 by the deal.II authors */
/*                                                                */
/*    This file is subject to QPL and may not be  distributed     */
/*    without copyright and license information. Please refer     */
/*    to the file deal.II/doc/license.html for the  text  and     */
/*    further information on this license.                        */

                                 // @sect3{Include files}

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/logstream.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/compressed_sparsity_pattern.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/grid/tria.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/tria_boundary_lib.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/base/convergence_table.h>

#include <fstream>
#include <iostream>
#include <deal.II/fe/fe_q.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/lac/constraint_matrix.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/numerics/error_estimator.h>

using namespace dealii;

template <int dim>
class Step6
{
  public:
    Step6 ();
    ~Step6 ();

    void run ();

  private:
    void setup_system ();
    void assemble_system ();
    void solve ();
    void refine_grid (const bool h_ref_choice);
    void output_results (const unsigned int cycle) const;
    void process_solution ();

    Triangulation<dim>   triangulation;

    DoFHandler<dim>      dof_handler;
    FE_Q<dim>            fe;

    ConstraintMatrix     hanging_node_constraints;

    SparsityPattern      sparsity_pattern;
    SparseMatrix<double> system_matrix;

    Vector<double>       solution;
    Vector<double>       system_rhs;

    ConvergenceTable          convergence_table;
};

template <int dim>
class SolutionPhi : public Function<dim>
{
  public:
    SolutionPhi () : Function<dim>() {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;
};

template <int dim>
double SolutionPhi<dim>::value (const Point<dim> &p,
                             const unsigned int) const
{
//  return 5.0 * cos( M_PI * p[0] / 2.0) * cos( M_PI * p[1] / 2.0);
  return (p[0]*p[0] - 1.0) * (p[1]*p[1] - 1.0) * exp(-10*((p[0] - 0.3)*(p[0] - 0.3) + (p[1] + 0.3)*(p[1] + 0.3) ));
}

template <int dim>
class RightHandSidePhi : public Function<dim>
{
  public:
    RightHandSidePhi () : Function<dim>() {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;
    virtual void value_list (const std::vector<Point<dim> > &points,
                             std::vector<double>            &values,
                             const unsigned int              component = 0) const;
};

template <int dim>
double RightHandSidePhi<dim>::value (const Point<dim>  &p,
                                     const unsigned int) const
{
//  return 2.0 * 1.0 * 5.0 * (M_PI/2.0) * (M_PI/2.0) * cos( M_PI * p[0] / 2.0) * cos( M_PI * p[1] / 2.0) ;
  return exp(std::pow(p[0]-3.0/1.0E1,2.0)*(-1.0E1)-std::pow(p[1]+3.0/1.0E1,2.0)*1.0E1)*(p[0]*p[0]-1.0)*(-2.0)-exp(std::pow(p[0]-3.0/1.0E1,2.0)*(-1.0E1)-std::pow(p[1]+3.0/1.0E1,2.0)*1.0E1)*(p[1]*p[1]-1.0)*2.0+exp(std::pow(p[0]-3.0/1.0E1,2.0)*(-1.0E1)-std::pow(p[1]+3.0/1.0E1,2.0)*1.0E1)*(p[0]*p[0]-1.0)*(p[1]*p[1]-1.0)*4.0E1+p[0]*exp(std::pow(p[0]-3.0/1.0E1,2.0)*(-1.0E1)-std::pow(p[1]+3.0/1.0E1,2.0)*1.0E1)*(p[0]*2.0E1-6.0)*(p[1]*p[1]-1.0)*4.0+p[1]*exp(std::pow(p[0]-3.0/1.0E1,2.0)*(-1.0E1)-std::pow(p[1]+3.0/1.0E1,2.0)*1.0E1)*(p[0]*p[0]-1.0)*(p[1]*2.0E1+6.0)*4.0-exp(std::pow(p[0]-3.0/1.0E1,2.0)*(-1.0E1)-std::pow(p[1]+3.0/1.0E1,2.0)*1.0E1)*std::pow(p[0]*2.0E1-6.0,2.0)*(p[0]*p[0]-1.0)*(p[1]*p[1]-1.0)-exp(std::pow(p[0]-3.0/1.0E1,2.0)*(-1.0E1)-std::pow(p[1]+3.0/1.0E1,2.0)*1.0E1)*(p[0]*p[0]-1.0)*std::pow(p[1]*2.0E1+6.0,2.0)*(p[1]*p[1]-1.0);
}

template <int dim>
void RightHandSidePhi<dim>::value_list (const std::vector<Point<dim> > &points,
                                        std::vector<double>            &values,
                                        const unsigned int              component) const
{
  const unsigned int n_points = points.size();

  Assert (values.size() == n_points,
          ExcDimensionMismatch (values.size(), n_points));

  Assert (component == 0,
          ExcIndexRange (component, 0, 1));

  for (unsigned int i=0; i<n_points; ++i)
    values[i] = value(points[i],0);
}

template <int dim>
class Coefficient : public Function<dim>
{
  public:
    Coefficient () : Function<dim>() {}

    virtual double value (const Point<dim>   &p,
                          const unsigned int  component = 0) const;

    virtual void value_list (const std::vector<Point<dim> > &points,
                             std::vector<double>            &values,
                             const unsigned int              component = 0) const;
};



template <int dim>
double Coefficient<dim>::value (const Point<dim> &p,
                                const unsigned int) const
{
  if (p.square() < 0.5*0.5)
    return 1;
  else
    return 1;
}



template <int dim>
void Coefficient<dim>::value_list (const std::vector<Point<dim> > &points,
                                   std::vector<double>            &values,
                                   const unsigned int              component) const
{
  const unsigned int n_points = points.size();

  Assert (values.size() == n_points,
          ExcDimensionMismatch (values.size(), n_points));

  Assert (component == 0,
          ExcIndexRange (component, 0, 1));

  for (unsigned int i=0; i<n_points; ++i)
    {
      if (points[i].square() < 0.5*0.5)
        values[i] = 1;
      else
        values[i] = 1;
    }
}


template <int dim>
Step6<dim>::Step6 ()
                :
                dof_handler (triangulation),
                fe (1)
{}


template <int dim>
Step6<dim>::~Step6 ()
{
  dof_handler.clear ();
}

template <int dim>
void Step6<dim>::setup_system ()
{
  dof_handler.distribute_dofs (fe);

  solution.reinit (dof_handler.n_dofs());
  system_rhs.reinit (dof_handler.n_dofs());

  hanging_node_constraints.clear ();
  DoFTools::make_hanging_node_constraints (dof_handler,
                                           hanging_node_constraints);

   hanging_node_constraints.close ();

  CompressedSparsityPattern c_sparsity(dof_handler.n_dofs());
  DoFTools::make_sparsity_pattern (dof_handler, c_sparsity);

  hanging_node_constraints.condense (c_sparsity);

  sparsity_pattern.copy_from(c_sparsity);

  system_matrix.reinit (sparsity_pattern);
}

template <int dim>
void Step6<dim>::assemble_system ()
{
  const QGauss<dim>  quadrature_formula(15);

  FEValues<dim> fe_values (fe, quadrature_formula,
                           update_values    |  update_gradients |
                           update_quadrature_points  |  update_JxW_values);

  const unsigned int   dofs_per_cell = fe.dofs_per_cell;
  const unsigned int   n_q_points    = quadrature_formula.size();

  FullMatrix<double>   cell_matrix (dofs_per_cell, dofs_per_cell);
  Vector<double>       cell_rhs (dofs_per_cell);

  std::vector<unsigned int> local_dof_indices (dofs_per_cell);

  const Coefficient<dim> coefficient;
  std::vector<double>    coefficient_values (n_q_points);

  const RightHandSidePhi<dim> RHS;
  std::vector<double>    RHS_values (n_q_points);

  typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler.begin_active(),
    endc = dof_handler.end();
  for (; cell!=endc; ++cell)
    {
      cell_matrix = 0;
      cell_rhs = 0;

      fe_values.reinit (cell);

      coefficient.value_list (fe_values.get_quadrature_points(),
                              coefficient_values);
      RHS.value_list (fe_values.get_quadrature_points(),
                      RHS_values);

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point)
        for (unsigned int i=0; i<dofs_per_cell; ++i)
          {
            for (unsigned int j=0; j<dofs_per_cell; ++j)
              cell_matrix(i,j) += (coefficient_values[q_point] *
                                   fe_values.shape_grad(i,q_point) *
                                   fe_values.shape_grad(j,q_point) *
                                   fe_values.JxW(q_point));

            cell_rhs(i) += (fe_values.shape_value(i,q_point) *
                            RHS_values[q_point] *
                            fe_values.JxW(q_point));
          }

      cell->get_dof_indices (local_dof_indices);
      for (unsigned int i=0; i<dofs_per_cell; ++i)
        {
          for (unsigned int j=0; j<dofs_per_cell; ++j)
            system_matrix.add (local_dof_indices[i],
                               local_dof_indices[j],
                               cell_matrix(i,j));

          system_rhs(local_dof_indices[i]) += cell_rhs(i);
        }
    }

  hanging_node_constraints.condense (system_matrix);
  hanging_node_constraints.condense (system_rhs);

  std::map<unsigned int,double> boundary_values;
  VectorTools::interpolate_boundary_values (dof_handler,
                                            0,
                                            ZeroFunction<dim>(),
                                            boundary_values);
  MatrixTools::apply_boundary_values (boundary_values,
                                      system_matrix,
                                      solution,
                                      system_rhs);
}

template <int dim>
void Step6<dim>::solve ()
{
  SolverControl           solver_control (10000, 1e-12);
  SolverCG<>              solver (solver_control);

  PreconditionSSOR<> preconditioner;
  preconditioner.initialize(system_matrix, 1.2);

  solver.solve (system_matrix, solution, system_rhs,
                preconditioner);

  hanging_node_constraints.distribute (solution);
}

template <int dim>
void Step6<dim>::refine_grid (const bool h_ref_choice)
{

  if(h_ref_choice){
    Vector<float> estimated_error_per_cell (triangulation.n_active_cells());

    KellyErrorEstimator<dim>::estimate (dof_handler,
                                        QGauss<dim-1>(3),
                                        typename FunctionMap<dim>::type(),
                                        solution,
                                        estimated_error_per_cell);

    GridRefinement::refine_and_coarsen_fixed_number (triangulation,
                                                     estimated_error_per_cell,
                                                     0.3, 0.0);
    triangulation.execute_coarsening_and_refinement ();
  }
  else
    {
     triangulation.refine_global(1);
    }
}

template <int dim>
void Step6<dim>::output_results (const unsigned int cycle) const
{
  Assert (cycle < 10, ExcNotImplemented());

  std::string filename = "grid-";
  filename += ('0' + cycle);
  filename += ".eps";

  std::ofstream output (filename.c_str());

  GridOut grid_out;
  grid_out.write_eps (triangulation, output);
}

template <int dim>
void Step6<dim>::process_solution()
{

  Vector<float> difference_per_cell (triangulation.n_active_cells());
  VectorTools::integrate_difference (dof_handler,
                                     solution,
                                     SolutionPhi<dim>(),
                                     difference_per_cell,
                                     QGauss<dim>(5),
                                     VectorTools::L2_norm);
  const double L2_error = difference_per_cell.l2_norm();

  convergence_table.add_value("dofs", dof_handler.n_dofs());
  convergence_table.add_value("Error", L2_error);


}

template <int dim>
void Step6<dim>::run ()
{
  const bool do_h_ref = true;
  unsigned int nmax_cycles=15;
  if(!do_h_ref)
    nmax_cycles=9;

  for (unsigned int cycle=0; cycle<nmax_cycles; ++cycle)
    {
      std::cout << "Cycle " << cycle << ':' << std::endl;

      if (cycle == 0)
        {
          GridGenerator::hyper_cube (triangulation,-1.0, 1.0);

//          static const HyperBallBoundary<dim> boundary;
//          triangulation.set_boundary (0, boundary);

          triangulation.refine_global (1);
        }
      else
        refine_grid (do_h_ref);


      std::cout << "   Number of active cells:       "
                << triangulation.n_active_cells()
                << std::endl;

      setup_system ();

      std::cout << "   Number of degrees of freedom: "
                << dof_handler.n_dofs()
                << std::endl;

      assemble_system ();
      solve ();
//      output_results (cycle);
      process_solution ();
    }
  DataOut<dim> data_out;

  data_out.attach_dof_handler (dof_handler);
  data_out.add_data_vector (solution, "solution");
  data_out.build_patches ();

  std::ofstream output ("final-solution.gpl");
  data_out.write_gnuplot (output);

  convergence_table.set_precision("Error", 3);
  convergence_table.set_scientific("Error", true);
//  convergence_table.evaluate_convergence_rates("Error", ConvergenceTable::reduction_rate_log2);

  std::cout << std::endl;
  convergence_table.write_text(std::cout);
  std::cout << std::endl;
}

int main ()
{

  try
    {
      deallog.depth_console (0);

      Step6<2> laplace_problem_2d;
      laplace_problem_2d.run ();
    }

  catch (std::exception &exc)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Exception on processing: " << std::endl
                << exc.what() << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;

      return 1;
    }

  catch (...)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Unknown exception!" << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
      return 1;
    }

  return 0;
}
