/* Author: Wolfgang Bangerth, University of Heidelberg, 2000 */

/*    $Id: step-6.cc 25840 2012-08-09 20:22:00Z bangerth $       */
/*                                                                */
/*    Copyright (C) 2000, 2001, 2002, 2003, 2004, 2006, 2007, 2008, 2010, 2011, 2012 by the deal.II authors */
/*                                                                */
/*    This file is subject to QPL and may not be  distributed     */
/*    without copyright and license information. Please refer     */
/*    to the file deal.II/doc/license.html for the  text  and     */
/*    further information on this license.                        */

                                 // @sect3{Include files}

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/convergence_table.h>

#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/compressed_sparsity_pattern.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/constraint_matrix.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/tria_boundary_lib.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_refinement.h>

#include <deal.II/hp/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/hp/fe_values.h>
#include <deal.II/fe/fe_q.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/error_estimator.h>

#include <fstream>
#include <iostream>
#include <complex>

using namespace dealii;

template <int dim>
class Step6
{
  public:
    Step6 ();
    ~Step6 ();

    void run ();

  private:
    void setup_system ();
    void assemble_system ();
    void solve ();
    void refine_grid (const bool h_ref_choice, const bool p_ref_choice);
    void estimate_smoothness (Vector<float> &smoothness_indicators) const;
    void output_results (const unsigned int cycle) const;
    void process_solution ();

    Triangulation<dim>   triangulation;

    hp::DoFHandler<dim>      dof_handler;
    hp::FECollection<dim>    fe_collection;
    hp::QCollection<dim>     quadrature_collection;
    hp::QCollection<dim-1>   face_quadrature_collection;

    ConstraintMatrix     hanging_node_constraints;

    SparsityPattern      sparsity_pattern;
    SparseMatrix<double> system_matrix;

    Vector<double>       solution;
    Vector<double>       system_rhs;

    ConvergenceTable     convergence_table;
    const unsigned int   max_degree;
};

template <int dim>
class SolutionPhi : public Function<dim>
{
  public:
    SolutionPhi () : Function<dim>() {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;
};

template <int dim>
double SolutionPhi<dim>::value (const Point<dim> &p,
                             const unsigned int) const
{
  double C_phi = 5.0;
  double L = 2.0;
  double beta = 10.0;
  double value;
  if(dim == 1){
    //value = C_phi * cos( M_PI * p[0] / L);
    value = -C_phi * (p[0] * p[0] - L/2.0) * exp(-beta *(p[0] - 0.3)*(p[0] - 0.3));
  } else if (dim == 2){
    //value = C_phi * cos( M_PI * p[0] / L) * cos( M_PI * p[1] / L);
    value = C_phi * (p[0]*p[0] - L/2.0) * (p[1]*p[1] - L/2.0) * exp(-beta*((p[0] - 0.3)*(p[0] - 0.3) + (p[1] + 0.3)*(p[1] + 0.3) ));
  }

  return value;
}

template <int dim>
class RightHandSidePhi : public Function<dim>
{
  public:
    RightHandSidePhi () : Function<dim>() {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;
    virtual void value_list (const std::vector<Point<dim> > &points,
                             std::vector<double>            &values,
                             const unsigned int              component = 0) const;
};

template <int dim>
double RightHandSidePhi<dim>::value (const Point<dim>  &p,
                                     const unsigned int) const
{
  double C_phi = 5.0;
  double lambda = 1.0;
  double L = 2.0;
  double beta = 10.0;
  double value;

  if(dim == 1){
    //value = dim * lambda * C_phi * (M_PI/L) * (M_PI/L) * cos( M_PI * p[0] / L);
    value = lambda*(C_phi*exp(-beta*std::pow(p[0]-3.0/1.0E1,2.0))*2.0+C_phi*beta*exp(-beta*std::pow(p[0]-3.0/1.0E1,2.0))*(L*(1.0/2.0)-p[0]*p[0])*2.0-C_phi*beta*p[0]*exp(-beta*std::pow(p[0]-3.0/1.0E1,2.0))*(p[0]*2.0-3.0/5.0)*4.0-C_phi*(beta*beta)*exp(-beta*std::pow(p[0]-3.0/1.0E1,2.0))*std::pow(p[0]*2.0-3.0/5.0,2.0)*(L*(1.0/2.0)-p[0]*p[0]));
  } else if (dim == 2){
    //value = dim * lambda * C_phi * (M_PI/L) * (M_PI/L) * cos( M_PI * p[0] / L) * cos( M_PI * p[1] / L) ;
    value = lambda*(C_phi*exp(-beta*(std::pow(p[0]-3.0/1.0E1,2.0)+std::pow(p[1]+3.0/1.0E1,2.0)))*(L*(1.0/2.0)-p[1]*p[1])*2.0+C_phi*beta*exp(-beta *(std::pow(p[0]-3.0/1.0E1,2.0)+std::pow(p[1]+3.0/1.0E1,2.0)))*(L*(1.0/2.0)-p[0]*p[0])*(L*(1.0/2.0)-p[1]*p[1])*2.0-C_phi*(beta*beta)*exp(-beta *(std::pow(p[0]-3.0/1.0E1,2.0)+std::pow(p[1]+3.0/1.0E1,2.0)))*std::pow(p[0]*2.0-3.0/5.0,2.0)*(L*(1.0/2.0)-p[0]*p[0])*(L*(1.0/2.0)-p[1]*p[1])-C_phi *beta*p[0]*exp(-beta*(std::pow(p[0]-3.0/1.0E1,2.0)+std::pow(p[1]+3.0/1.0E1,2.0)))*(p[0]*2.0-3.0/5.0)*(L*(1.0/2.0)-p[1]*p[1])*4.0)+lambda*(C_phi*exp(-beta *(std::pow(p[0]-3.0/1.0E1,2.0)+std::pow(p[1]+3.0/1.0E1,2.0)))*(L*(1.0/2.0)-p[0]*p[0])*2.0+C_phi*beta*exp(-beta*(std::pow(p[0]-3.0/1.0E1,2.0) +std::pow(p[1]+3.0/1.0E1,2.0)))*(L*(1.0/2.0)-p[0]*p[0])*(L*(1.0/2.0)-p[1]*p[1])*2.0-C_phi*(beta*beta)*exp(-beta*(std::pow(p[0]-3.0/1.0E1,2.0) +std::pow(p[1]+3.0/1.0E1,2.0)))*std::pow(p[1]*2.0+3.0/5.0,2.0)*(L*(1.0/2.0)-p[0]*p[0])*(L*(1.0/2.0)-p[1]*p[1])-C_phi*beta*p[1]*exp(-beta *(std::pow(p[0]-3.0/1.0E1,2.0) + std::pow(p[1]+3.0/1.0E1,2.0)))*(p[1]*2.0+3.0/5.0)*(L*(1.0/2.0)-p[0]*p[0])*4.0);
  }
  
  return value;
}

template <int dim>
void RightHandSidePhi<dim>::value_list (const std::vector<Point<dim> > &points,
                                        std::vector<double>            &values,
                                        const unsigned int              component) const
{
  const unsigned int n_points = points.size();

  Assert (values.size() == n_points,
          ExcDimensionMismatch (values.size(), n_points));

  Assert (component == 0,
          ExcIndexRange (component, 0, 1));

  for (unsigned int i=0; i<n_points; ++i)
    values[i] = value(points[i],0);
}

template <int dim>
class Coefficient : public Function<dim>
{
  public:
    Coefficient () : Function<dim>() {}

    virtual double value (const Point<dim>   &p,
                          const unsigned int  component = 0) const;

    virtual void value_list (const std::vector<Point<dim> > &points,
                             std::vector<double>            &values,
                             const unsigned int              component = 0) const;
};



template <int dim>
double Coefficient<dim>::value (const Point<dim> &p,
                                const unsigned int) const
{
  if (p.square() < 0.5*0.5)
    return 1;
  else
    return 1;
}



template <int dim>
void Coefficient<dim>::value_list (const std::vector<Point<dim> > &points,
                                   std::vector<double>            &values,
                                   const unsigned int              component) const
{
  const unsigned int n_points = points.size();

  Assert (values.size() == n_points,
          ExcDimensionMismatch (values.size(), n_points));

  Assert (component == 0,
          ExcIndexRange (component, 0, 1));

  for (unsigned int i=0; i<n_points; ++i)
    {
      if (points[i].square() < 0.5*0.5)
        values[i] = 1;
      else
        values[i] = 1;
    }
}


template <int dim>
Step6<dim>::Step6 ()
                :
                dof_handler (triangulation),
                max_degree (dim <= 2 ? 9 : 7)
{
  for(unsigned int degree=1; degree<=max_degree; ++degree){
    fe_collection.push_back (FE_Q<dim>(degree));
    quadrature_collection.push_back(QGauss<dim>(degree+10));
    face_quadrature_collection.push_back(QGauss<dim-1>(degree+10));
  }
}


template <int dim>
Step6<dim>::~Step6 ()
{
  dof_handler.clear ();
}

template <int dim>
void Step6<dim>::setup_system ()
{
  dof_handler.distribute_dofs (fe_collection);

  solution.reinit (dof_handler.n_dofs());
  system_rhs.reinit (dof_handler.n_dofs());

  hanging_node_constraints.clear ();
  DoFTools::make_hanging_node_constraints (dof_handler,
                                           hanging_node_constraints);

   hanging_node_constraints.close ();

  CompressedSparsityPattern c_sparsity(dof_handler.n_dofs());
  DoFTools::make_sparsity_pattern (dof_handler, c_sparsity);

  hanging_node_constraints.condense (c_sparsity);

  sparsity_pattern.copy_from(c_sparsity);

  system_matrix.reinit (sparsity_pattern);
}

template <int dim>
void Step6<dim>::assemble_system ()
{

  hp::FEValues<dim> hp_fe_values (fe_collection, quadrature_collection,
                                  update_values    |  update_gradients |
                                  update_quadrature_points  |  update_JxW_values);

  FullMatrix<double>   cell_matrix;
  Vector<double>       cell_rhs;
  std::vector<unsigned int> local_dof_indices;

  const RightHandSidePhi<dim> RHS;
  const Coefficient<dim> coefficient;

  typename hp::DoFHandler<dim>::active_cell_iterator
    cell = dof_handler.begin_active(),
    endc = dof_handler.end();
  for (; cell!=endc; ++cell)
    {
      hp_fe_values.reinit (cell);
      const FEValues<dim> &fe_values = hp_fe_values.get_present_fe_values ();

      const unsigned int   dofs_per_cell = cell->get_fe().dofs_per_cell;
      const unsigned int   n_q_points    = fe_values.n_quadrature_points;

      std::vector<double>    coefficient_values (n_q_points);
      std::vector<double>    RHS_values (n_q_points);

      cell_matrix.reinit(dofs_per_cell,dofs_per_cell);
      cell_matrix = 0;
      cell_rhs.reinit(dofs_per_cell);
      cell_rhs = 0;


      
      coefficient.value_list (fe_values.get_quadrature_points(),
                              coefficient_values);
      RHS.value_list (fe_values.get_quadrature_points(),
                      RHS_values);

      for (unsigned int q_point=0; q_point<n_q_points; ++q_point)
        for (unsigned int i=0; i<dofs_per_cell; ++i)
          {
            for (unsigned int j=0; j<dofs_per_cell; ++j)
              cell_matrix(i,j) += (coefficient_values[q_point] *
                                   fe_values.shape_grad(i,q_point) *
                                   fe_values.shape_grad(j,q_point) *
                                   fe_values.JxW(q_point));

            cell_rhs(i) += (fe_values.shape_value(i,q_point) *
                            RHS_values[q_point] *
                            fe_values.JxW(q_point));
          }

      local_dof_indices.resize (dofs_per_cell);
      cell->get_dof_indices (local_dof_indices);
      for (unsigned int i=0; i<dofs_per_cell; ++i)
        {
          for (unsigned int j=0; j<dofs_per_cell; ++j)
            system_matrix.add (local_dof_indices[i],
                               local_dof_indices[j],
                               cell_matrix(i,j));

          system_rhs(local_dof_indices[i]) += cell_rhs(i);
        }
    }

  hanging_node_constraints.condense (system_matrix);
  hanging_node_constraints.condense (system_rhs);

  std::map<unsigned int,double> boundary_values;
  VectorTools::interpolate_boundary_values (dof_handler,
                                            0,
                                            ZeroFunction<dim>(),
                                            boundary_values);
  VectorTools::interpolate_boundary_values (dof_handler,
                                            1,
                                            ZeroFunction<dim>(),
                                            boundary_values);
  MatrixTools::apply_boundary_values (boundary_values,
                                      system_matrix,
                                      solution,
                                      system_rhs);
}

template <int dim>
void Step6<dim>::solve ()
{
  SolverControl           solver_control (10000, 1e-12);
  SolverCG<>              solver (solver_control);

  PreconditionSSOR<> preconditioner;
  preconditioner.initialize(system_matrix, 1.2);

  solver.solve (system_matrix, solution, system_rhs,
                preconditioner);

  hanging_node_constraints.distribute (solution);
}

template <int dim>
void Step6<dim>::refine_grid (const bool h_ref_choice, const bool p_ref_choice)
{

  if(h_ref_choice){
    Vector<float> estimated_error_per_cell (triangulation.n_active_cells());
    KellyErrorEstimator<dim>::estimate (dof_handler,
                                        face_quadrature_collection,
                                        typename FunctionMap<dim>::type(),
                                        solution,
                                        estimated_error_per_cell);

    GridRefinement::refine_and_coarsen_fixed_number (triangulation,
                                                     estimated_error_per_cell,
                                                     0.3, 0.0);
    if(p_ref_choice){
      Vector<float> smoothness_indicators (triangulation.n_active_cells());
      estimate_smoothness (smoothness_indicators);

      float max_smoothness = *std::min_element (smoothness_indicators.begin(),
                                                smoothness_indicators.end()) ,
            min_smoothness = *std::max_element (smoothness_indicators.begin(),
                                                smoothness_indicators.end()) ;

      typename hp::DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
        endc = dof_handler.end();
      for (unsigned int index = 0; cell!=endc; ++cell, ++index)
        if(cell->refine_flag_set()){
          max_smoothness = std::max (max_smoothness,
                                     smoothness_indicators(index));
          min_smoothness = std::min (min_smoothness,
                                     smoothness_indicators(index));
        }

      const float threshold_smoothness = (max_smoothness + min_smoothness) / 2.0;
      cell = dof_handler.begin_active();
      for (unsigned int index = 0; cell!=endc; ++cell, ++index)
        if (cell->refine_flag_set() && (smoothness_indicators(index) > threshold_smoothness) && (cell->active_fe_index()+1 < fe_collection.size())){
          cell->clear_refine_flag();
          cell->set_active_fe_index (cell->active_fe_index() +1);
        }
    }
    triangulation.execute_coarsening_and_refinement ();
  }
  else
    {
     triangulation.refine_global(1);
/*      typename hp::DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
        endc = dof_handler.end();
      for (unsigned int index = 0; cell!=endc; ++cell, ++index)
        if (cell->active_fe_index()+1 < fe_collection.size()){
          cell->set_active_fe_index (cell->active_fe_index() +1);
        }
      triangulation.execute_coarsening_and_refinement ();*/
    }
}

template <int dim>
void Step6<dim>::output_results (const unsigned int cycle) const
{
  Assert (cycle < 10, ExcNotImplemented());

  std::string filename = "grid-";
  filename += ('0' + cycle);
  filename += ".eps";

  std::ofstream output (filename.c_str());

  GridOut grid_out;
  grid_out.write_eps (triangulation, output);
}

template <int dim>
void Step6<dim>::process_solution()
{

  Vector<float> difference_per_cell (triangulation.n_active_cells());
  VectorTools::integrate_difference (dof_handler,
                                     solution,
                                     SolutionPhi<dim>(),
                                     difference_per_cell,
                                     quadrature_collection,
                                     VectorTools::L2_norm);
  const double L2_error = difference_per_cell.l2_norm();

  convergence_table.add_value("dofs", dof_handler.n_dofs());
  convergence_table.add_value("Error", L2_error);
}

template <int dim>
void Step6<dim>::run ()
{
  const bool do_h_ref = true;
  const bool do_p_ref = true;
  unsigned int nmax_cycles=14;
  if(!do_h_ref)
    nmax_cycles=8;
  if(do_h_ref && do_p_ref)
    nmax_cycles=21;

  for (unsigned int cycle=0; cycle<nmax_cycles; ++cycle)
    {
      std::cout << "Cycle " << cycle << ':' << std::endl;

      if (cycle == 0)
        {
          GridGenerator::hyper_cube (triangulation,-1.0, 1.0);

          triangulation.refine_global (1);
        }
      else
        refine_grid (do_h_ref,do_p_ref);


      std::cout << "   Number of active cells:       "
                << triangulation.n_active_cells()
                << std::endl;

      setup_system ();

      std::cout << "   Number of degrees of freedom: "
                << dof_handler.n_dofs()
                << std::endl;

      assemble_system ();
      solve ();
//      output_results (cycle);
      process_solution ();
    }

  DataOut<dim,hp::DoFHandler<dim> > data_out;

  data_out.attach_dof_handler (dof_handler);
  data_out.add_data_vector (solution, "solution");
  data_out.build_patches ();

  std::ofstream output ("final-solution.gpl");
  data_out.write_gnuplot (output);

  convergence_table.set_precision("Error", 3);
  convergence_table.set_scientific("Error", true);
//  convergence_table.evaluate_convergence_rates("Error", ConvergenceTable::reduction_rate_log2);

  std::cout << std::endl;
  convergence_table.write_text(std::cout);
  std::cout << std::endl;

}

template <int dim>
void
Step6<dim>::
estimate_smoothness (Vector<float> &smoothness_indicators) const
{
  const unsigned int N = max_degree;

  std::vector<Tensor<1,dim> > k_vectors;
  std::vector<unsigned int>   k_vectors_magnitude;
  switch (dim)
    {
      case 2:
      {
        for (unsigned int i=0; i<N; ++i)
          for (unsigned int j=0; j<N; ++j)
            if (!((i==0) && (j==0))
                &&
                (i*i + j*j < N*N))
              {
                k_vectors.push_back (Point<dim>(numbers::PI * i,
                                                numbers::PI * j));
                k_vectors_magnitude.push_back (i*i+j*j);
              }

        break;
      }

      case 3:
      {
        for (unsigned int i=0; i<N; ++i)
          for (unsigned int j=0; j<N; ++j)
            for (unsigned int k=0; k<N; ++k)
              if (!((i==0) && (j==0) && (k==0))
                  &&
                  (i*i + j*j + k*k < N*N))
                {
                  k_vectors.push_back (Point<dim>(numbers::PI * i,
                                                  numbers::PI * j,
                                                  numbers::PI * k));
                  k_vectors_magnitude.push_back (i*i+j*j+k*k);
                }

        break;
      }

      default:
            Assert (false, ExcNotImplemented());
    }

  const unsigned n_fourier_modes = k_vectors.size();
  std::vector<double> ln_k (n_fourier_modes);
  for (unsigned int i=0; i<n_fourier_modes; ++i)
    ln_k[i] = std::log (k_vectors[i].norm());


  std::vector<Table<2,std::complex<double> > >
    fourier_transform_matrices (fe_collection.size());

  QGauss<1>      base_quadrature (2);
  QIterated<dim> quadrature (base_quadrature, N);


  for (unsigned int fe=0; fe<fe_collection.size(); ++fe)
    {
      fourier_transform_matrices[fe].reinit (n_fourier_modes,
                                             fe_collection[fe].dofs_per_cell);

      for (unsigned int k=0; k<n_fourier_modes; ++k)
        for (unsigned int j=0; j<fe_collection[fe].dofs_per_cell; ++j)
          {
            std::complex<double> sum = 0;
            for (unsigned int q=0; q<quadrature.size(); ++q)
              {
                const Point<dim> x_q = quadrature.point(q);
                sum += std::exp(std::complex<double>(0,1) *
                                (k_vectors[k] * x_q)) *
                       fe_collection[fe].shape_value(j,x_q) *
                       quadrature.weight(q);
              }
            fourier_transform_matrices[fe](k,j)
              = sum / std::pow(2*numbers::PI, 1.*dim/2);
          }
    }

  std::vector<std::complex<double> > fourier_coefficients (n_fourier_modes);
  Vector<double>                     local_dof_values;

  typename hp::DoFHandler<dim>::active_cell_iterator
    cell = dof_handler.begin_active(),
    endc = dof_handler.end();
  for (unsigned int index=0; cell!=endc; ++cell, ++index)
    {
      local_dof_values.reinit (cell->get_fe().dofs_per_cell);
      cell->get_dof_values (solution, local_dof_values);

      for (unsigned int f=0; f<n_fourier_modes; ++f)
        {
          fourier_coefficients[f] = 0;

          for (unsigned int i=0; i<cell->get_fe().dofs_per_cell; ++i)
            fourier_coefficients[f] +=
              fourier_transform_matrices[cell->active_fe_index()](f,i)
              *
              local_dof_values(i);
        }

      std::map<unsigned int, double> k_to_max_U_map;
      for (unsigned int f=0; f<n_fourier_modes; ++f)
        if ((k_to_max_U_map.find (k_vectors_magnitude[f]) ==
             k_to_max_U_map.end())
            ||
            (k_to_max_U_map[k_vectors_magnitude[f]] <
             std::abs (fourier_coefficients[f])))
          k_to_max_U_map[k_vectors_magnitude[f]]
            = std::abs (fourier_coefficients[f]);

      double  sum_1           = 0,
              sum_ln_k        = 0,
              sum_ln_k_square = 0,
              sum_ln_U        = 0,
              sum_ln_U_ln_k   = 0;
      for (unsigned int f=0; f<n_fourier_modes; ++f)
        if (k_to_max_U_map[k_vectors_magnitude[f]] ==
            std::abs (fourier_coefficients[f]))
          {
            sum_1 += 1;
            sum_ln_k += ln_k[f];
            sum_ln_k_square += ln_k[f]*ln_k[f];
            sum_ln_U += std::log (std::abs (fourier_coefficients[f]));
            sum_ln_U_ln_k += std::log (std::abs (fourier_coefficients[f])) *
                             ln_k[f];
          }

      const double mu
        = (1./(sum_1*sum_ln_k_square - sum_ln_k*sum_ln_k)
           *
           (sum_ln_k*sum_ln_U - sum_1*sum_ln_U_ln_k));

      smoothness_indicators(index) = mu - 1.*dim/2;
    }
}


int main ()
{

  try
    {
      deallog.depth_console (0);

      Step6<2> laplace_problem_2d;
      laplace_problem_2d.run ();
    }

  catch (std::exception &exc)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Exception on processing: " << std::endl
                << exc.what() << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;

      return 1;
    }

  catch (...)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Unknown exception!" << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
      return 1;
    }

  return 0;
}
