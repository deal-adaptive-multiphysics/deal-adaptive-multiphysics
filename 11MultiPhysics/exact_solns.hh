#ifndef EXACT_SOLNS_HH
#define EXACT_SOLNS_HH

using namespace dealii;

template <int dim>
class Solution1 : public Function<dim>
{
  public:
    Solution1 (const Parameters::AllParameters params) : Function<dim>(),
                                                           params(params) {}
    virtual ~Solution1 () {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double Solution1<dim>::value (const Point<dim> &p,
                              const unsigned int) const
{
  double time = this->get_time();

  double value;
  if(dim == 1){
    double x0 = params.xPosition1*cos(params.solutionSpeed1 * time);
    value = params.C_phi1 * (-(p[0]/params.length)*(p[0]/params.length) + 1)
                          * exp (-(p[0] - x0)*(p[0] - x0));
  }else if (dim == 2){
    value = params.C_phi1 * (-(p[0]/params.length)*(p[0]/params.length) + 1)
                          * (-(p[1]/params.length)*(p[1]/params.length) + 1)
                          * exp (-(p[0] - 0)*(p[0] - 0) - (p[1] - 0)*(p[1] - 0));
  }

  return value;
}

template <int dim>
class Solution2 : public Function<dim>
{
  public:
    Solution2 (const Parameters::AllParameters params) : Function<dim>(),
                                                           params(params) {}
    virtual ~Solution2 () {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double Solution2<dim>::value (const Point<dim> &p,
                              const unsigned int) const
{
  double time = this->get_time();

  double value;

  if(dim == 1){
    double x0 = params.xPosition2 * cos(params.solutionSpeed2 * time);
    value = params.C_phi2 * (-(p[0]/params.length)*(p[0]/params.length) + 1)
                          * exp (-(p[0] - x0)*(p[0] - x0));
  }else if(dim == 2){
    value = params.C_phi2 * (-(p[0]/params.length)*(p[0]/params.length) + 1)
                          * (-(p[1]/params.length)*(p[1]/params.length) + 1)
                          * exp (-(p[0] - 5)*(p[0] - 5) - (p[1] - 5)*(p[1] - 5));
  }

  return value;
}


#endif // EXACT_SOLNS_HH
