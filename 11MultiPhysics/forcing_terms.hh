#ifndef FORCING_TERMS_HH
#define FORCING_TERMS_HH

using namespace dealii;
using namespace std;

template <int dim>
class RightHandSide1 : public Function<dim>
{
  public:
    RightHandSide1 (const Parameters::AllParameters params) : Function<dim>(),
                                                              params(params) {}
    virtual ~RightHandSide1 () {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double RightHandSide1<dim>::value (const Point<dim>  &p,
                                   const unsigned int) const
{

  double D1 = params.Diffusion_Const1;
  double C_phi1 = params.C_phi1;
  double L = params.length;
  double Sa1 = params.Absorption_Const1;
  double Ss1 = params.Scattering_Const1;
  double Sf1 = params.Fission_Const1;
  double omega1 = params.solutionSpeed1;
  double x01 = params.xPosition1;
  double C_phi2 = params.C_phi2;
  double Sf2 = params.Fission_Const2;
  double omega2 = params.solutionSpeed2;
  double x02 = params.xPosition2;
  double nu = params.nu;
  double beta = params.beta;
  double gamma = params.Doppler_Const;
  double T_ref = params.T_ref;
  double x1;
  double x2;

  double current_time = this->get_time();

  double value;
  if(dim == 1){
    x1 = p[0];

    value = D1*(C_phi1*exp(-pow(x1-x01*cos(current_time*omega1),2.0))*(1/(L*L)*(x1*x1)-1.0)*2.0-C_phi1*1/(L*L)*exp(-pow(x1-x01*cos(current_time*omega1),2.0))*2.0-C_phi1*exp(-pow(x1-x01*cos(current_time*omega1),2.0))*(1/(L*L)*(x1*x1)-1.0)*pow(x1*2.0-x01*cos(current_time*omega1)*2.0,2.0)+C_phi1*1/(L*L)*x1*exp(-pow(x1-x01*cos(current_time*omega1),2.0))*(x1*2.0-x01*cos(current_time*omega1)*2.0)*4.0)+C_phi1*exp(-pow(x1-x01*cos(current_time*omega1),2.0))*(1/(L*L)*(x1*x1)-1.0)*(Ss1-Sa1*(gamma*(T_ref*T_ref-(C_phi2*C_phi2)*exp(pow(x1-x02*cos(current_time*omega2),2.0)*(-2.0))*pow(1/(L*L)*(x1*x1)-1.0,2.0))-1.0)+Sf1*nu*(beta-1.0))+C_phi2*Sf2*nu*exp(-pow(x1-x02*cos(current_time*omega2),2.0))*(1/(L*L)*(x1*x1)-1.0)*(beta-1.0)-C_phi1*omega1*x01*exp(-pow(x1-x01*cos(current_time*omega1),2.0))*sin(current_time*omega1)*(1/(L*L)*(x1*x1)-1.0)*(x1-x01*cos(current_time*omega1))*2.0;

  }else if(dim == 2){
    x1 = p[0];
    x2 = p[1];

    value = -D1*(C_phi1*exp(-x1*x1)*exp(-x2*x2)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*2.0-C_phi1*1/(L*L)*exp(-x1*x1)*exp(-x2*x2)*(1/(L*L)*(x1*x1)-1.0)*2.0-C_phi1*(x2*x2)*exp(-x1*x1)*exp(-x2*x2)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*4.0+C_phi1*1/(L*L)*(x2*x2)*exp(-x1*x1)*exp(-x2*x2)*(1/(L*L)*(x1*x1)-1.0)*8.0)-D1*(C_phi1*exp(-x1*x1)*exp(-x2*x2)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*2.0-C_phi1*1/(L*L)*exp(-x1*x1)*exp(-x2*x2)*(1/(L*L)*(x2*x2)-1.0)*2.0-C_phi1*(x1*x1)*exp(-x1*x1)*exp(-x2*x2)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*4.0+C_phi1*1/(L*L)*(x1*x1)*exp(-x1*x1)*exp(-x2*x2)*(1/(L*L)*(x2*x2)-1.0)*8.0)-C_phi1*exp(-x1*x1)*exp(-x2*x2)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(Ss1-Sa1*(gamma*(T_ref*T_ref-(C_phi2*C_phi2)*exp(pow(x1-5.0,2.0)*(-2.0))*exp(pow(x2-5.0,2.0)*(-2.0))*pow(1/(L*L)*(x1*x1)-1.0,2.0)*pow(1/(L*L)*(x2*x2)-1.0,2.0))-1.0)+Sf1*nu*(beta-1.0))-C_phi2*Sf2*nu*exp(-pow(x1-5.0,2.0))*exp(-pow(x2-5.0,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*(beta-1.0);
  }

  return value;
}

template <int dim>
class RightHandSide2 : public Function<dim>
{
  public:
    RightHandSide2 (const Parameters::AllParameters params) : Function<dim>(),
                                                              params(params) {}
    virtual ~RightHandSide2 () {}
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    const Parameters::AllParameters params;
};

template <int dim>
double RightHandSide2<dim>::value (const Point<dim>  &p,
                                   const unsigned int) const
{

  double D2 = params.Diffusion_Const2;
  double C_phi2 = params.C_phi2;
  double omega2 = params.solutionSpeed2;
  double x01 = params.xPosition1;
  double L = params.length;
  double Sa2 = params.Absorption_Const2;
  double C_phi1 = params.C_phi1;
  double Ss1 = params.Scattering_Const1;
  double omega1 = params.solutionSpeed1;
  double x02 = params.xPosition2;
  double x1;
  double x2;
  double current_time = this->get_time();

  double value;
  if(dim == 1){
    x1 = p[0];

    value = D2*(C_phi2*exp(-pow(x1-x02*cos(current_time*omega2),2.0))*(1.0/(L*L)*(x1*x1)-1.0)*2.0-C_phi2*1.0/(L*L)*exp(-pow(x1-x02*cos(current_time*omega2),2.0))*2.0-C_phi2*exp(-pow(x1-x02*cos(current_time*omega2),2.0))*(1.0/(L*L)*(x1*x1)-1.0)*pow(x1*2.0-x02*cos(current_time*omega2)*2.0,2.0)+C_phi2*1.0/(L*L)*x1*exp(-pow(x1-x02*cos(current_time*omega2),2.0))*(x1*2.0-x02*cos(current_time*omega2)*2.0)*4.0)+C_phi2*Sa2*exp(-pow(x1-x02*cos(current_time*omega2),2.0))*(1.0/(L*L)*(x1*x1)-1.0)-C_phi1*Ss1*exp(-pow(x1-x01*cos(current_time*omega1),2.0))*(1.0/(L*L)*(x1*x1)-1.0)-C_phi2*omega2*x02*exp(-pow(x1-x02*cos(current_time*omega2),2.0))*sin(current_time*omega2)*(1.0/(L*L)*(x1*x1)-1.0)*(x1-x02*cos(current_time*omega2))*2.0;

  }else if(dim == 2){
    x1 = p[0];
    x2 = p[1];

    value = -D2*(C_phi2*exp(-pow(x1-5.0,2.0))*exp(-pow(x2-5.0,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*2.0-C_phi2*1/(L*L)*exp(-pow(x1-5.0,2.0))*exp(-pow(x2-5.0,2.0))*(1/(L*L)*(x2*x2)-1.0)*2.0-C_phi2*exp(-pow(x1-5.0,2.0))*exp(-pow(x2-5.0,2.0))*pow(x1*2.0-1.0E1,2.0)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)+C_phi2*1/(L*L)*x1*exp(-pow(x1-5.0,2.0))*exp(-pow(x2-5.0,2.0))*(x1*2.0-1.0E1)*(1/(L*L)*(x2*x2)-1.0)*4.0)-D2*(C_phi2*exp(-pow(x1-5.0,2.0))*exp(-pow(x2-5.0,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)*2.0-C_phi2*1/(L*L)*exp(-pow(x1-5.0,2.0))*exp(-pow(x2-5.0,2.0))*(1/(L*L)*(x1*x1)-1.0)*2.0-C_phi2*exp(-pow(x1-5.0,2.0))*exp(-pow(x2-5.0,2.0))*pow(x2*2.0-1.0E1,2.0)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)+C_phi2*1/(L*L)*x2*exp(-pow(x1-5.0,2.0))*exp(-pow(x2-5.0,2.0))*(x2*2.0-1.0E1)*(1/(L*L)*(x1*x1)-1.0)*4.0)-C_phi2*Sa2*exp(-pow(x1-5.0,2.0))*exp(-pow(x2-5.0,2.0))*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0)+C_phi1*Ss1*exp(-x1*x1)*exp(-x2*x2)*(1/(L*L)*(x1*x1)-1.0)*(1/(L*L)*(x2*x2)-1.0);

  }

  return value;
}


#endif // FORCING_TERMS_HH
