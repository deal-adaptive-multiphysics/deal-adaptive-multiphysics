#ifndef PARAMETERS_HH
#define PARAMETERS_HH

#include <deal.II/base/parameter_handler.h>

using namespace dealii;
namespace Parameters
{

//==========================================
// Solver Parameters
//==========================================

/**
 * @brief Parameters for Solvers
 * @details Parameters include such things as: Type of linear and non-linear solvers, Solver tolerances, etc...
 */
struct Solver
{
  /**
   * @brief Enum for the type of Non-linear solver to be used
   * @details This provides a way to choose which type of non-linear solver can be used
   *          and declaring a variable of this type. Currently the only non-linear solver
   *          implemented is Newton's Method.
   */
  enum NonLinearSolverType {newton};
  NonLinearSolverType NLsolver;

  /**
   * @brief Enum for the type of Linear solver to be used
   * @details This provides a way to choose which type of linear solver can be used
   *          and declaring a variable of this type. The two methods to choose from
   *          are a direct solver provided by the UMFPACK library, or GMRes provided
   *          by the dealii library.
   */
  enum LinearSolverType {direct, gmres};
  LinearSolverType solver;

  /**
   * @brief Enum for the behavior of console output
   * @details The console output can either be printed or silenced. Choosing "quiet"
   *          suppresses output while choosing "verbose" will print output to the
   *          console.
   */
  enum OutputType { quiet, verbose };
  OutputType NLoutput;
  OutputType output;

  double linear_tol;
  int max_linear_iterations;

  double nonLinear_atol;
  double nonLinear_rtol;
  int max_nonLinear_iterations;

  /**
   * @brief Declares solver parameters and allocates memory
   * @param params Stores all parameters in this object.
   */
  static void declare_parameters (ParameterHandler &params);
  /**
   * @brief Parses solver parameters and sets variables to parsed values
   * @param params Stores all parameters in this object.
   */
  void parse_parameters   (ParameterHandler &params);
};

/**
 * @brief Parameters for Problem
 * @details These parameters include constants used for this specific problem, domain size,
 *          time integrator, time domain, etc...
 */
struct AllParameters : public Solver
{
  /**
   * @brief Constructor
   */
  AllParameters ();

  /**
   * @brief Type of Spatial Refinement
   * @details Choice of spatial refinement technique. Choosing "global"
   *          causes mesh to be refined globally. Choosing "hAMR" causes
   *          mesh to be adaptively refined using the Kelley Error Estimator.
   *
   */
  enum RefinementType {global, hAMR};
  RefinementType refineScheme;

  /**
   * @brief Output timers to console
   * @details Whether to output information about runtime on console. If "verbose",
   *          the console will have elapsed times for various processes.
   */
  enum TimeInfoType {quiet, verbose};
  TimeInfoType timeInfo;

  /**
   * @brief Type of solution used by MMS
   * @details Currently only one solution type is implemented. Depricated.
   */
  enum SolutionType {sin, exp, sint, expt, quadt};
  SolutionType exact_solution_type;

  /**
   * @brief Time integrator method
   * @details Choice of time integrator methods. All use the same implementation except that
   *          each have a different butcher tableau.
   */
  enum TimeStepMethod {SDIRK22, SDIRK33, BE, CN, RK4};
  TimeStepMethod time_step_method;

  unsigned int n_init_refinements;
  unsigned int n_global_refinements;
  unsigned int cycles;
  bool fullChart;
  bool output_sparsity_pattern;

  unsigned int polynomial_degree;
  unsigned int n_quad_points;

  double length;
  unsigned int n_physics;

  // Nuclear Constants
  double beta;
  double nu;

  // Phi 1 Constants
  double C_phi1;
  double Diffusion_Const1;
  double Absorption_Const1;
  double Fission_Const1;
  double Scattering_Const1;
  double solutionSpeed1;
  double xPosition1;
  double yPosition1;

  // Phi 2 Constants
  double C_phi2;
  double Diffusion_Const2;
  double Absorption_Const2;
  double Fission_Const2;
  double solutionSpeed2;
  double xPosition2;
  double yPosition2;

  // Temperature Constants
  double C_T;
  double Doppler_Const;
  double T_ref;

  double initial_time;
  double end_time;
  unsigned int number_time_steps;

  /**
   * @brief Declares solver parameters and allocates memory
   * @param params Stores all parameters in this object.
   */
  static void declare_parameters (ParameterHandler &prm);
  /**
   * @brief Parses solver parameters and sets variables to parsed values
   * @param params Stores all parameters in this object.
   */
  void parse_parameters (ParameterHandler &prm);
};


//==========================================================================================
//==========================================================================================
//                                       Implementations
//==========================================================================================
//==========================================================================================

void Solver::parse_parameters (ParameterHandler &params){

  params.enter_subsection("nonlinear solver");
  {
    const std::string op = params.get("output");
    if (op == "verbose") output = verbose;
    if (op == "quiet")   output = quiet;

    nonLinear_atol = params.get_double("nonlinear absolute tolerance");
    nonLinear_rtol = params.get_double("nonlinear relative tolerance");
    max_nonLinear_iterations = params.get_integer("max nonlinear iterations");
  }
  params.leave_subsection();

  params.enter_subsection("linear solver");
  {
    const std::string option = params.get("Solver");
    if (option == "direct") solver = direct;
    if (option == "gmres" ) solver = gmres;

    linear_tol = params.get_double("Linear tolerance");
    max_linear_iterations = params.get_integer("Max linear iterations");
  }
  params.leave_subsection();

}

void Solver::declare_parameters (ParameterHandler &params){

  params.enter_subsection("nonlinear solver");
  {
    params.declare_entry("output", "quiet",
                         Patterns::Selection("quiet|verbose"),
                         "State whether nonlinear output should be printed.\n"
                         "Choices are <quiet|verbose>.");
    params.declare_entry("nonlinear absolute tolerance", "1E-10",
                         Patterns::Double(),
                         "Nonlinear Absolute Tolerance");
    params.declare_entry("nonlinear relative tolerance", "1E-10",
                         Patterns::Double(),
                         "Nonlinear Relative Tolerance");
    params.declare_entry("max nonlinear iterations", "50",
                         Patterns::Integer(),
                         "Maximum number of nonlinear iterations");
  }
  params.leave_subsection();

  params.enter_subsection("linear solver");
  {
    params.declare_entry("Solver", "direct",
                          Patterns::Selection("direct|gmres"),
                         "Which Linear Solver Method is used?\n"
                         "Choices are <direct|gmres>.");
    params.declare_entry("Linear tolerance", "1E-12",
                          Patterns::Double(),
                         "Linear Solver Tolerance");
    params.declare_entry("Max linear iterations", "1000000",
                          Patterns::Integer(),
                         "Maximum number of linear iterations");
  }
  params.leave_subsection();

}


AllParameters::AllParameters ()
{}

void AllParameters::declare_parameters (ParameterHandler &prm)
{
  prm.enter_subsection("refinement");
  {
    prm.declare_entry("Refinement Type","global",
                       Patterns::Selection("global|hAMR"),
                      "State the type of refinement scheme.\n"
                      "Choices are <global|hAMR>.");
    prm.declare_entry("cycles","4",
                       Patterns::Integer(0),
                      "Number of Refinement Cycles");
    prm.declare_entry("Initial Global Refinements","3",
                      Patterns::Integer(2),
                      "Number of initial global refinements.\n"
                      "Minimum is 2.");
    prm.declare_entry("Initial Adaptive Refinements","3",
                      Patterns::Integer(2),
                      "Number of initial adaptive refinements to initial condition.\n"
                      "Minimum is 2.");
  }
  prm.leave_subsection();

  prm.enter_subsection("Reporting");
  {
    prm.declare_entry("Full Conv Chart","false",
                       Patterns::Bool(),
                      "Whether to Print a full convergence chart or not");
    prm.declare_entry("Sparsity Pattern","false",
                       Patterns::Bool(),
                      "Output the sparsity pattern or not.");
    prm.declare_entry("Time Info","quiet",
                       Patterns::Selection("quiet|verbose"),
                      "Display Time info.\n"
                      "Choices are <quiet|verbose>.");
  }
  prm.leave_subsection();

  prm.enter_subsection("domain");
  {
    prm.declare_entry("length","10.0",
                       Patterns::Double(0),
                       "Length of Domain");
    prm.declare_entry("Number of Physics","2",
                       Patterns::Integer(1),
                       "Number of Physics in Problem");
  }
  prm.leave_subsection();

  prm.enter_subsection("Nuclear Properties");
  {
    prm.declare_entry("Delayed Neutron Fraction","0.1",
                       Patterns::Double(0,1),
                      "Delayed Neutron Fraction");
    prm.declare_entry("Neutron Multiplicity","1.6",
                       Patterns::Double(0),
                      "Nu");
  }
  prm.leave_subsection ();

  prm.enter_subsection("Fast Flux properties");
  {
    prm.declare_entry("Flux Amplitude","2.5",
                       Patterns::Double(0),
                      "Maximum Flux for MMS");
    prm.declare_entry("Diffusion Constant","2",
                       Patterns::Double(0),
                      "Diffusion Constant for Fast Neutrons");
    prm.declare_entry("Absorption Constant","3",
                       Patterns::Double(0),
                      "Absorption Constant for Fast Neutrons");
    prm.declare_entry("Fission Constant","5",
                       Patterns::Double(0),
                      "Fission Constant for Fast Neutrons");
    prm.declare_entry("Scattering Constant","0.01",
                       Patterns::Double(0),
                      "Scattering Constant for Fast Neutrons");
    prm.declare_entry("Solution Speed","0.1",
                       Patterns::Double(0),
                      "Speed of Rotation for MMS");
    prm.declare_entry("X0 Position","5.0",
                       Patterns::Double(),
                      "Starting X Position for MMS");
    prm.declare_entry("Y0 Position","5.0",
                       Patterns::Double(),
                      "Starting Y Position for MMS");
  }
  prm.leave_subsection();

  prm.enter_subsection("Thermal Flux properties");
  {
    prm.declare_entry("Flux Amplitude","1.5",
                       Patterns::Double(0),
                      "Maximum Flux for MMS");
    prm.declare_entry("Diffusion Constant","1",
                       Patterns::Double(0),
                      "Diffusion Constant for Thermal Neutrons");
    prm.declare_entry("Absorption Constant","2",
                       Patterns::Double(0),
                      "Absorption Constant for Thermal Neutrons");
    prm.declare_entry("Fission Constant","4",
                       Patterns::Double(0),
                      "Fission Constant for Thermal Neutrons");
    prm.declare_entry("Solution Speed","0.2",
                       Patterns::Double(0),
                      "Speed of Rotation for MMS");
    prm.declare_entry("X0 Position","-5.0",
                       Patterns::Double(),
                      "Starting X Position for MMS");
    prm.declare_entry("Y0 Position","-5.0",
                       Patterns::Double(),
                      "Starting Y Position for MMS");
  }
  prm.leave_subsection();

  prm.enter_subsection("Temperature properties");
  {
    prm.declare_entry("Temp Amplitude","0.5",
                       Patterns::Double(0),
                      "Maximum Temp for MMS");
    prm.declare_entry("Doppler Constant","0.001",
                       Patterns::Double(0),
                      "Doppler Coefficient for Fast Absorption");
    prm.declare_entry("Reference Temp","3",
                       Patterns::Double(0),
                      "Reference Temp for Fast Absorption");
  }
  prm.leave_subsection();

  prm.enter_subsection("Exact Solution Propterties");
  {
    prm.declare_entry("Exact Solution Type","sin",
                      Patterns::Selection("sin|exp|sint|expt|quadt"),
                      "State the type of exact solution.\n"
                      "Choices are <sin|exp|sint|expt|quadt>.");
  }
  prm.leave_subsection();

  prm.enter_subsection("Time Parameters");
  {
    prm.declare_entry("Initial Time","0.0",
                      Patterns::Double(0),
                      "Initial Time");
    prm.declare_entry("End Time","1.0",
                      Patterns::Double(0),
                      "Final Time");
    prm.declare_entry("Time Steps","2",
                      Patterns::Integer(0),
                      "Number of steps to be taken");
    prm.declare_entry("RK Method","SDIRK22",
                      Patterns::Selection("SDIRK22|SDIRK33|BE|CN|RK4"),
                      "Runge Kutta Method used.\n"
                      "Choices are <SDIRK22|SDIRK33|BE|CN|RK4>");
  }
  prm.leave_subsection();

  Parameters::Solver::declare_parameters (prm);
}

void AllParameters::parse_parameters (ParameterHandler &prm)
{
  prm.enter_subsection("refinement");
  {
    const std::string option = prm.get("Refinement Type");
    if (option == "global") refineScheme = global;
    if (option == "hAMR")   refineScheme = hAMR;

    cycles = prm.get_integer("cycles");
    n_global_refinements = prm.get_integer("Initial Global Refinements");
    n_init_refinements = prm.get_integer("Initial Adaptive Refinements");
  }
  prm.leave_subsection();

  prm.enter_subsection("Reporting");
  {
    fullChart = prm.get_bool("Full Conv Chart");
    output_sparsity_pattern = prm.get_bool("Sparsity Pattern");
    
    const std::string option = prm.get("Time Info");
    if (option == "quiet")   timeInfo = quiet;
    if (option == "verbose") timeInfo = verbose;
  }
  prm.leave_subsection();

  prm.enter_subsection("domain");
  {
    length = prm.get_double("length");
    n_physics = prm.get_integer("Number of Physics");
  }
  prm.leave_subsection();

  prm.enter_subsection("Fast Flux properties");
  {
    C_phi1 = prm.get_double("Flux Amplitude");
    Diffusion_Const1  = prm.get_double("Diffusion Constant");
    Absorption_Const1 = prm.get_double("Absorption Constant");
    Fission_Const1 = prm.get_double("Fission Constant");
    Scattering_Const1 = prm.get_double("Scattering Constant");
    solutionSpeed1 = prm.get_double("Solution Speed");
    xPosition1 = prm.get_double("X0 Position");
    yPosition1 = prm.get_double("Y0 Position");
  }
  prm.leave_subsection();

  prm.enter_subsection("Thermal Flux properties");
  {
    C_phi2 = prm.get_double("Flux Amplitude");
    Diffusion_Const2  = prm.get_double("Diffusion Constant");
    Absorption_Const2 = prm.get_double("Absorption Constant");
    Fission_Const2 = prm.get_double("Fission Constant");
    solutionSpeed2 = prm.get_double("Solution Speed");
    xPosition2 = prm.get_double("X0 Position");
    yPosition2 = prm.get_double("Y0 Position");
  }
  prm.leave_subsection();

  prm.enter_subsection("Temperature properties");
  {
    C_T = prm.get_double("Temp Amplitude");
    Doppler_Const  = prm.get_double("Doppler Constant");
    T_ref = prm.get_double("Reference Temp");
  }
  prm.leave_subsection();

  prm.enter_subsection("Exact Solution Propterties");
  {
    const std::string option = prm.get("Exact Solution Type");
    if (option == "sin")  exact_solution_type = sin;
    if (option == "exp")  exact_solution_type = exp;
    if (option == "sint") exact_solution_type = sint;
    if (option == "expt") exact_solution_type = expt;
    if (option == "quadt") exact_solution_type = quadt;
  }
  prm.leave_subsection();

  prm.enter_subsection("Time Parameters");
  {
    initial_time = prm.get_double("Initial Time");
    end_time = prm.get_double("End Time");
    number_time_steps = prm.get_integer("Time Steps");

    const std::string option = prm.get("RK Method");
    if (option == "SDIRK22")  time_step_method = SDIRK22;
    if (option == "SDIRK33")  time_step_method = SDIRK33;
    if (option == "BE"     )  time_step_method = BE;
    if (option == "CN"     )  time_step_method = CN;
    if (option == "RK4"    )  time_step_method = RK4;
  }
  prm.leave_subsection();

  Parameters::Solver::parse_parameters(prm);
}

}// end namespace

#endif // PARAMETERS_HH
