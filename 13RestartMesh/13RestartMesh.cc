#include <deal.II/base/function.h>
#include <deal.II/base/timer.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/grid_refinement.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/dofs/dof_renumbering.h>

#include <deal.II/lac/block_sparse_matrix.h>
#include <deal.II/lac/block_vector.h>
#include <deal.II/lac/constraint_matrix.h>
#include <deal.II/lac/sparse_direct.h>

#include <deal.II/numerics/matrices.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/numerics/solution_transfer.h>

#include "parameters.hh"
#include "exact_solns.hh"
#include "forcing_terms.hh"
#include "material_properties.hh"
#include <iostream>
#include <fstream>

namespace MultiPhysics {
using namespace dealii;

/**
 * @brief Time-dependent coupled physics problem
 * @details Solves time-dependent coupled physics problem with MMS
 *          and spatially adapts the domain in time when needed. This
 *          means that the same mesh is used for multiple time steps until
 *          the solution has moved significantly from its starting position.
 * @author Kevin Dugan, 2013
 */
template <int dim>
class blockPhysics
{
  public:
    /**
     * @brief Constructor for coupled physics
     * @param params Stores all parameters for this problem
     */
    blockPhysics (const Parameters::AllParameters params);
    /**
     * @brief Controller Function
     * @details Controls the execution of this problem.
     */
    void run ();

  private:
    /**
     * @brief Initializes Solution
     * @details Projects the initial condition to the coarse mesh then
     *          adapts the mesh based on the projected solution. This member
     *          is only called at the beginning of the solution sequence.
     */
    void initialize_solution ();
    /**
     * @brief Sets up Spatial grid
     * @details Initializes a single coarse mesh and globally refines a
     *          specified number of times. The refined coarse mesh is then
     *          copied to the triangulations for all other physics.
     */
    void make_grid ();
    /**
     * @brief Resets Triangulations
     * @details Clears the triangulations for all physics and copies the
     *          original coarse mesh back to all triangulations.
     */
    void reset_grid ();
    /**
     * @brief Estimates error of solution
     * @details Uses the KelleyErrorEstimator and stores the current solution
     *          error for each physic and stores the value in the vector
     *          current_error
     */
    void estimate_solution_error ();
    /**
     * @brief Resets Error Vectors
     * @details Allocates memory for the vectors: initial_error, current_error,
     *          and average_error. These vectors are resized based on the number
     *          of active cells in the corresponding triangulation.
     */
    void initialize_error_vectors ();
    /**
     * @brief Resets single Error Vector
     * @details Allocates memory for the vectors: initial_error, current_error,
     *          and average_error but only for a single physic. The number of
     *          the physic is passed as an argument. Only this physic's error
     *          vectors are resized and reallocated.
     * @param physic The number of the physic affected
     */
    void initialize_error_vectors (unsigned int physic);
    /**
     * @brief Refine and Coarsen all triangulations
     * @details This function uses the current_error vector for all physic to
     *          refine and coarsen the mesh based on the error vector.
     */
    void refine_grid ();
    /**
     * @brief Refine and Coarsen specified triangulation
     * @details Refines and coarsens the triangulation provided in the arguments.
     *          This function is used to for a particular physic, thus all arguments
     *          passed pertain to a single physic.
     *
     * @param triangulation Triangulation to be refined and coarsened
     * @param dof_handler DoFHandler object associated with the physic
     * @param fe FiniteElement object associated with the physic
     * @param solution Block of the solution vector for the physic
     * @param constraints ConstraintMatrix associated with the physic
     * @param error_estimate Error vector to be used to determine which
     *                       cells are refined and coarsened
     * @param max_levels This integer tells the maximum number of refinement
     *                   levels that are allowed. Without this term, there is a
     *                   possibility for the refinement procedure to "run away" and
     *                   cause the mesh size to become extremely small and thus driving
     *                   the time step size smaller and smaller.
     */
    void refine_grid (Triangulation<dim> &triangulation, DoFHandler<dim> &dof_handler,
                      FE_Q<dim> &fe, Vector<double> &solution, ConstraintMatrix &constraints,
                      Vector<float> error_estimate, const unsigned int max_levels);
    /**
     * @brief Allocates memory for the entire problem
     * @details Sets up the block vector sizes, matrix sizes, constraint matrices, etc...
     *
     * @param init Boolean denoting whether this is the initial setup of the system or if this
     *             is a resizing of the system. In the case of resizing, the constraint matrix
     *             is generally already taken care of when the mesh is transformed and thus does
     *             not need to be repeated.
     *
     */
    void setup_system (bool init);
    /**
     * @brief Refines initial mesh with initial condition
     * @details This function is designed to take the initial condition and adaptively refine
     *          the corresponding mesh. We need to know the current time to pass to the initial
     *          condition and we only want to refine if this is the first "cycle". If it is not
     *          the first cycle, we simply want to project the initial condition to the mesh.
     * @param time Current time used by initial condition object
     */
    void initialize_solution (const double time);
    /**
     * @brief Assemble Block mass matrix
     * @details Assemble the mass matrix for each physic and put in a block diagonal matrix
     */
    void assemble_mass_matrix ();
    /**
     * @brief Assemble Transient Residual and Jacobian
     */
    void assemble_tr_system (const unsigned int stage_i, const double time);
    /**
     * @brief Assemble Steady State Residual and Jacobian
     */
    void assemble_ss_system (const double time);
    /**
     * @brief Assemble Block 1 of Residual and Jacobian
     */
    void assemble_physic1 ();
    /**
     * @brief Assemble Block 2 of Residual and Jacobian
     */
    void assemble_physic2 ();
    /**
     * @brief Assemble forcing terms from MMS
     */
    void assemble_load_vector (const double time);
    /**
     * @brief Assemble coupling terms
     * @details This function assembles the off diagonal terms in the Jacobian
     *          matrix. This function is called recursively for meshes that have
     *          different levels of refinement.
     */
    void assemble_off_diagonal (const typename DoFHandler<dim>::cell_iterator &physic1,
                                const typename DoFHandler<dim>::cell_iterator &physic2,
                                const FullMatrix<double> prolongation_matrix,
                                const double Coeff, const int blockM, const int blockN);
    /**
     * @brief Assemble non-linear coupling
     * @details Assembles contributions of non-linear couplings. These couplings are hard coded
     *          in this function.
     */
    void assemble_off_diagonal_nonlinear (const typename DoFHandler<dim>::cell_iterator &physic1,
                                          const typename DoFHandler<dim>::cell_iterator &physic2,
                                          const FullMatrix<double> prolongation_matrix,
                                          const int blockM, const int blockN);
    /**
     * @brief Dirichelt Boundary Conditions
     * @details This function sets the homogeneous Dirichlet condition for the update vector
     *          according to boundary indicators of 1 and 0. Then the specified boundary
     *          values are set for the solution vector using the same boundary indicators.
     */
    void set_Boundary_Conditions ();
    /**
     * @brief Newton's Method
     * @details Solves the non-linear residual by Newton's method. We implement a relative tolerance
     *          for convergence based on the size of the initial residual.
     */
    void newton_solve (const unsigned int stage_i, const double time);
    /**
     * @brief Matrix Inversion
     * @details Solves the system Ax=b for x. Currently only the direct solver from UMFPACK is
     *          implemented.
     */
    void linear_solve ();
    /**
     * @brief Increment Solution from Newton Iteration
     * @details Adds the update vector to the solution vector. A tuning parameter is available for
     *          use in a line search method, but is set to unity currently.
     */
    void update_solution ();
    /**
     * @brief Determines if spatial refinement is needed
     */
    std::vector<bool> refinement_needed(bool just_refined[]);
    /**
     * @brief Spatially refine in a time loop
     * @details If the solution has changed, the mesh will be adapted and prepared to continue
     *          with the time marching scheme.
     */
    void refine_spatially (bool just_refined[],
                           const std::vector<bool> needs_refinement,
                           const unsigned int step);
    /**
     * @brief Calculates Error for Steady State Problem
     * @details Uses the manufactured solution to calculate the error of
     *          the current solution.
     */
    void process_solution ();
    /**
     * @brief Calculates Error for Transient Problem
     * @details Uses the manufactured solution to calculate the error of
     *          the current solution at the specified time.
     */
    void process_solution (const double time);
    /**
     * @brief Calculates Error for Transient Problem
     * @details Uses the manufactured solution to calculate the error of
     *          the current solution at the specified time. Includes the
     *          number of time steps in the output.
     */
    void process_solution (const double time, const unsigned int steps);
    /**
     * @brief Outputs solution to VTK file
     * @details Outputs the current solution at the current time step into
     *          a VTK file
     */
    void output_results (const unsigned int time_step);
    /**
     * @brief Outputs Exact Solution to VTK file
     * @details Outputs the exact solution at the specified time to a
     *          VTK file.
     */
    void output_exact (const double time);

    Triangulation<dim>        coarse_mesh;
    Triangulation<dim>        triangulation1;
    Triangulation<dim>        triangulation2;
    FE_Q<dim>                 fe1;
    FE_Q<dim>                 fe2;
    DoFHandler<dim>           dof_handler1;
    DoFHandler<dim>           dof_handler2;
    ConstraintMatrix          constraints1;
    ConstraintMatrix          constraints2;

    BlockSparsityPattern      sparsity_pattern;
    BlockSparseMatrix<double> system_matrix;
    BlockSparseMatrix<double> mass_matrix;

    BlockVector<double>       solution;
    BlockVector<double>       old_solution;
    BlockVector<double>       initial_solution;
    BlockVector<double>       update;
    BlockVector<double>       system_rhs;
    std::vector<BlockVector<double> > previous_F;
    BlockVector<double>       tmp_vector;

    const Parameters::AllParameters parameters;

    unsigned int n_stage;
    std::vector<std::vector<double> > A;
    std::vector<double> B, C;

    std::vector<Vector<float> > current_error;
    std::vector<Vector<float> > initial_error;
    std::vector<Vector<float> > average_error;

    double current_time;
    double time_step;

};

template <int dim>
blockPhysics<dim>::blockPhysics (const Parameters::AllParameters params)
                 : fe1 (2),
                   fe2 (2),
                   dof_handler1 (triangulation1),
                   dof_handler2 (triangulation2),
                   parameters (params)
{
  init_butcher_tableau(method_time(parameters.time_step_method), n_stage, A, B, C);
  current_time = parameters.initial_time;
  time_step = 1;

  // Print Butcher Tableau to Screen
  printf("\n\tButcher Tableau:\n");
  for(unsigned int i=0; i<n_stage; i++){
    printf("\t%7.3f | ",C[i]);
    for(unsigned int j=0; j<n_stage; j++){
      printf("%7.3f ",A[i][j]);
    } printf("\n");
  } printf("\t ");
  for(unsigned int i=0; i<n_stage+1; i++){
    if(i==0) printf("_______|");
    else printf("________");
  } printf("\n");
  printf("\t        | ");
  for(unsigned int i=0; i<n_stage; i++){
    printf("%7.3f ",B[i]);
  } printf("\n\n");
}

template <int dim>
void blockPhysics<dim>::run ()
{
  Timer runtime;
  runtime.start();

  double init_time = parameters.initial_time;
  double end_time = parameters.end_time;
  unsigned int n_steps = parameters.number_time_steps;

  make_grid ();
  setup_system (true);
  initialize_error_vectors();

  time_step = (end_time - init_time)/n_steps;
  current_time = init_time;
  initialize_solution (current_time);

  initialize_error_vectors();
  estimate_solution_error();
  initial_error = current_error;
  old_solution = solution;
  initial_solution = solution;
  output_results (0);

  //Initialize Cycle Parameters
  unsigned int cycle = 1;
  const unsigned int cycle_max = parameters.cycles;
  unsigned int init_step = 1;
  unsigned int packet = 1;
  double init_cycle_time = current_time;

  assemble_mass_matrix();

  bool just_refined[parameters.n_physics];
  for(unsigned int i=0; i<parameters.n_physics; i++)
    just_refined[i] = false;

  unsigned int output_step = 0;
  for (unsigned int step = 1; step <= n_steps; step++){
//    std::cout << "Cycle: " << cycle << "\tTime Step: " << step << std::endl;
    for(unsigned int stage = 0; stage < n_stage; stage++){
      newton_solve (stage, current_time + C[stage] * time_step);
    }

    system_rhs = 0.0;
    system_matrix = 0.0;
    mass_matrix.vmult(system_rhs, old_solution);

    for(unsigned int stage_i = 0; stage_i<n_stage; stage_i++)
      system_rhs.add(time_step * B[stage_i], previous_F[stage_i]);
    system_matrix.copy_from(mass_matrix);

    constraints1.condense(system_matrix.block(0,0));
    constraints1.condense(system_rhs.block(0));
    constraints2.condense(system_matrix.block(1,1));
    constraints2.condense(system_rhs.block(1));
    set_Boundary_Conditions();

    linear_solve ();
    solution = update;

    current_time += time_step;
    old_solution = solution;

    std::vector<bool> needs_refinement = refinement_needed(just_refined);

    bool refine = false;
    for(unsigned int phys = 0; phys < parameters.n_physics; phys++)
      if(needs_refinement[phys])
        refine = true;

    // If last time step, don't refine spatially
    if(step == parameters.number_time_steps)
      refine = false;


    if(refine && cycle == cycle_max){
      initial_solution = old_solution;
      refine_spatially(just_refined, needs_refinement, step);
      old_solution = initial_solution;
      cycle = 1;
      if(cycle_max > 1)
        packet ++;
      init_step = step+1;
      init_cycle_time = current_time;
    }else if(refine && cycle < cycle_max){
      refine_spatially(just_refined, needs_refinement, step);
      cycle ++;
      step = init_step -1;
      current_time = init_cycle_time;
      old_solution = initial_solution;
    }

    // Evaluate Initial Error for Just refined solutions
    solution = old_solution;
    estimate_solution_error();
    for(unsigned int i=0; i<parameters.n_physics; i++)
      if(just_refined[i])
          initial_error[i] = current_error[i];

    output_step ++;
    output_results (output_step);
//    process_solution(current_time);
  }

  process_solution (current_time, n_steps);
  output_exact (current_time);
  printf("Runtime: %7.3f [s]\n",runtime());
}

template <int dim>
void blockPhysics<dim>::make_grid ()
{
  GridGenerator::hyper_cube (coarse_mesh, -parameters.length, parameters.length);
  coarse_mesh.refine_global (parameters.n_global_refinements);

  triangulation1.copy_triangulation (coarse_mesh);
  triangulation2.copy_triangulation (coarse_mesh);
}

template <int dim>
std::vector<bool> blockPhysics<dim>::refinement_needed(bool just_refined[])
{
  std::vector<bool> needs_refinement(parameters.n_physics,false);

  estimate_solution_error();
  float errorAngle[parameters.n_physics];
  for(unsigned int i=0; i<parameters.n_physics; i++){
    if(just_refined[i]){
        //initial_error[i] = current_error[i];
        just_refined[i] = false;
    }
    errorAngle[i] = (initial_error[i] * current_error[i])/
                    (initial_error[i].l2_norm() * current_error[i].l2_norm());

    float angleTol = std::cos(parameters.errorAngle * M_PI / 180.0);
    float errorEps = 0.000001;
    if(errorAngle[i] <= 1.0 + errorEps && errorAngle[i] > angleTol){
        needs_refinement[i] = false;
    }else if(errorAngle[i] <= angleTol && errorAngle[i] >= -(1.0+errorEps) ){
        needs_refinement[i] = true;
    }else
      AssertThrow(false, ExcMessage("Physic Not Supported"));
  }

  return needs_refinement;
}

template <int dim>
void blockPhysics<dim>::refine_spatially(bool just_refined[],
                                         const std::vector<bool> needs_refinement,
                                         const unsigned int step)
{

  unsigned int max_level = parameters.n_init_refinements + parameters.n_global_refinements +0;

  if(step != parameters.number_time_steps){
      for(unsigned int i=0; i < parameters.n_physics; i++){
        if(!needs_refinement[i]){
            average_error[i] += current_error[i];
        }else if(needs_refinement[i]){
            average_error[i] += current_error[i];
            if(i == 0){
              std::cout << "Refining Fast Flux. Step " << step << std::endl;
              refine_grid(triangulation1, dof_handler1, fe1, initial_solution.block(i), constraints1,
                          average_error[i], max_level);
            }else if(i == 1){
              std::cout << "Refining Thermal Flux. Step " << step << std::endl;
              refine_grid(triangulation2, dof_handler2, fe2, initial_solution.block(i), constraints2,
                          average_error[i], max_level);
            }else
              AssertThrow(false, ExcMessage("Physic Not Supported"));

            initialize_error_vectors(i);
            just_refined[i] = true;
        }else
          AssertThrow(false,ExcMessage("Angle outside of range [-1, 1]") );
      }
  }

}

template <int dim>
void blockPhysics<dim>::reset_grid()
{
  triangulation1.clear ();
  triangulation2.clear ();
  triangulation1.copy_triangulation (coarse_mesh);
  triangulation2.copy_triangulation (coarse_mesh);
  setup_system (false);
}

template <int dim>
void blockPhysics<dim>::estimate_solution_error()
{
  KellyErrorEstimator<dim>::estimate (dof_handler1,
                                      QGauss<dim-1>(3),
                                      typename FunctionMap<dim>::type(),
                                      solution.block(0),
                                      current_error[0]);
  KellyErrorEstimator<dim>::estimate (dof_handler2,
                                      QGauss<dim-1>(3),
                                      typename FunctionMap<dim>::type(),
                                      solution.block(1),
                                      current_error[1]);
}

template <int dim>
void blockPhysics<dim>::refine_grid ()
{
  if(parameters.refineScheme == Parameters::AllParameters::hAMR){
    // Physic 1
    GridRefinement::refine_and_coarsen_fixed_number (triangulation1,
                                                     current_error[0],
                                                     0.3, 0.0);
    triangulation1.execute_coarsening_and_refinement ();

    // Physic 2
    GridRefinement::refine_and_coarsen_fixed_number (triangulation2,
                                                     current_error[1],
                                                     0.3, 0.0);
    triangulation2.execute_coarsening_and_refinement ();

  } else if(parameters.refineScheme == Parameters::AllParameters::global){
    triangulation1.refine_global(1);
    triangulation2.refine_global(1);
  } else
    AssertThrow(false,ExcMessage("Refinement Scheme Not Implemented"));

}

template <int dim>
void blockPhysics<dim>::refine_grid(Triangulation<dim> &triangulation, DoFHandler<dim> &dof_handler,
                                    FE_Q<dim> &fe, Vector<double> &solution, ConstraintMatrix &constraints,
                                    Vector<float> error_estimate, const unsigned int max_levels)
{
  GridRefinement::refine_and_coarsen_fixed_number(triangulation,
                                                  error_estimate,
                                                  0.3, 0.2);

  if(triangulation.n_levels() > max_levels)
    for (typename Triangulation<dim>::active_cell_iterator
            cell = triangulation.begin_active(max_levels);
            cell != triangulation.end(); ++cell)
         cell->clear_refine_flag ();

  triangulation.prepare_coarsening_and_refinement ();
  SolutionTransfer<dim> solution_transfer(dof_handler);
  solution_transfer.prepare_for_coarsening_and_refinement(solution);

  triangulation.execute_coarsening_and_refinement ();
  dof_handler.distribute_dofs(fe);
  DoFRenumbering::Cuthill_McKee(dof_handler);

  Vector<double> tmp(dof_handler.n_dofs());
  solution_transfer.interpolate(solution, tmp);
  solution = tmp;

  constraints.clear();
  DoFTools::make_hanging_node_constraints(dof_handler, constraints);
  constraints.close();
  constraints.distribute(solution);

  setup_system (false);
  assemble_mass_matrix ();
  set_Boundary_Conditions();
}

template <int dim>
void blockPhysics<dim>::setup_system (bool init)
{
  system_matrix.clear ();
  mass_matrix.clear ();

  if(init){
    dof_handler1.distribute_dofs (fe1);
    dof_handler2.distribute_dofs (fe2);
    DoFRenumbering::Cuthill_McKee(dof_handler1);
    DoFRenumbering::Cuthill_McKee(dof_handler2);
  }

  const unsigned int n_phys1 = dof_handler1.n_dofs(),
                     n_phys2 = dof_handler2.n_dofs();

  solution.reinit (2);
  solution.block(0).reinit(n_phys1);
  solution.block(1).reinit(n_phys2);
  solution.collect_sizes ();
  update.reinit (2);
  update.block(0).reinit(n_phys1);
  update.block(1).reinit(n_phys2);
  update.collect_sizes ();
  system_rhs.reinit (2);
  system_rhs.block(0).reinit(n_phys1);
  system_rhs.block(1).reinit(n_phys2);
  system_rhs.collect_sizes ();
  tmp_vector.reinit (2);
  tmp_vector.block(0).reinit(n_phys1);
  tmp_vector.block(1).reinit(n_phys2);
  tmp_vector.collect_sizes ();
  old_solution.reinit (2);
  old_solution.block(0).reinit(n_phys1);
  old_solution.block(1).reinit(n_phys2);
  old_solution.collect_sizes ();
  if(init){
    initial_solution.reinit (2);
    initial_solution.block(0).reinit(n_phys1);
    initial_solution.block(1).reinit(n_phys2);
    initial_solution.collect_sizes ();
  }

  previous_F = std::vector<BlockVector<double> > (n_stage, BlockVector<double> ());
  for(unsigned int stage=0; stage<n_stage; stage++){
      previous_F[stage].reinit(2);
      previous_F[stage].block(0).reinit(n_phys1);
      previous_F[stage].block(1).reinit(n_phys2);
      previous_F[stage].collect_sizes();
  }

  if(init){
    constraints1.clear ();
    constraints2.clear ();
    DoFTools::make_hanging_node_constraints (dof_handler1,
                                             constraints1);
    DoFTools::make_hanging_node_constraints (dof_handler2,
                                             constraints2);
    constraints1.close ();
    constraints2.close ();
  }

  {
    BlockCompressedSimpleSparsityPattern c_sparsity (2,2);
    c_sparsity.block(0,0).reinit (n_phys1, n_phys1);
    c_sparsity.block(0,1).reinit (n_phys1, n_phys2);
    c_sparsity.block(1,0).reinit (n_phys2, n_phys1);
    c_sparsity.block(1,1).reinit (n_phys2, n_phys2);
    c_sparsity.collect_sizes ();

    DoFTools::make_sparsity_pattern (dof_handler1, c_sparsity.block(0,0) );
    DoFTools::make_sparsity_pattern (dof_handler1, dof_handler2, c_sparsity.block(0,1) );
    DoFTools::make_sparsity_pattern (dof_handler2, c_sparsity.block(1,1) );
    DoFTools::make_sparsity_pattern (dof_handler2, dof_handler1, c_sparsity.block(1,0) );
    constraints1.condense (c_sparsity.block(0,0));
    constraints2.condense (c_sparsity.block(1,1));
    sparsity_pattern.copy_from (c_sparsity);
  }

  system_matrix.reinit (sparsity_pattern);
  mass_matrix.reinit (sparsity_pattern);

}

template <int dim>
void blockPhysics<dim>::initialize_error_vectors()
{
  current_error = std::vector<Vector<float> > (parameters.n_physics, Vector<float>() );
  initial_error = std::vector<Vector<float> > (parameters.n_physics, Vector<float>() );
  average_error = std::vector<Vector<float> > (parameters.n_physics, Vector<float>() );

  current_error[0].reinit(triangulation1.n_active_cells() );
  current_error[1].reinit(triangulation2.n_active_cells() );
  initial_error[0].reinit(triangulation1.n_active_cells() );
  initial_error[1].reinit(triangulation2.n_active_cells() );
  average_error[0].reinit(triangulation1.n_active_cells() );
  average_error[1].reinit(triangulation2.n_active_cells() );
}

template <int dim>
void blockPhysics<dim>::initialize_error_vectors(unsigned int physic)
{
  unsigned int cells;
  if(physic == 0)
    cells = triangulation1.n_active_cells();
  else if(physic == 1)
    cells = triangulation2.n_active_cells();
  else
    AssertThrow(false,ExcMessage("Physic Not Supported"));

  current_error[physic].reinit(cells );
  initial_error[physic].reinit(cells );
  average_error[physic].reinit(cells );
}

template <int dim>
void blockPhysics<dim>::initialize_solution(const double time)
{
  Solution1<dim> ExactSolution1(parameters);
  Solution2<dim> ExactSolution2(parameters);

  ExactSolution1.set_time(time);
  ExactSolution2.set_time(time);

  VectorTools::interpolate(dof_handler1,
                           ExactSolution1,
                           solution.block(0));
  VectorTools::interpolate(dof_handler2,
                           ExactSolution2,
                           solution.block(1));

  for(unsigned int i = 0; i < parameters.n_init_refinements; i++){
      estimate_solution_error ();
      refine_grid ();
      setup_system (true);

      VectorTools::interpolate(dof_handler1,
                               ExactSolution1,
                               solution.block(0));
      VectorTools::interpolate(dof_handler2,
                               ExactSolution2,
                               solution.block(1));

      constraints1.distribute(solution.block(0));
      constraints2.distribute(solution.block(1));
  }
}

template <int dim>
void blockPhysics<dim>::assemble_mass_matrix ()
{
  mass_matrix = 0.0;

  QGauss<dim> quadrature_formula(3);
  UpdateFlags update_flags = update_values | update_gradients |
                             update_quadrature_points | update_JxW_values;

  FEValues<dim> fe_values1 (fe1, quadrature_formula, update_flags);
  FEValues<dim> fe_values2 (fe2, quadrature_formula, update_flags);

  const unsigned int n_q_points = quadrature_formula.size();

  typename DoFHandler<dim>::active_cell_iterator
    cell1 = dof_handler1.begin_active(),
    endc1 = dof_handler1.end();
  for(; cell1!=endc1; ++cell1)
    {
      fe_values1.reinit (cell1);
      const unsigned int dofs_per_cell = fe1.dofs_per_cell;
      std::vector<unsigned int> local_dof_indices (dofs_per_cell);

      FullMatrix<double>  local_mass (dofs_per_cell, dofs_per_cell);

      for(unsigned int q_point=0; q_point<n_q_points; ++q_point)
        for(unsigned int i=0; i<dofs_per_cell; ++i)
          for(unsigned int j=0; j<dofs_per_cell; ++j)
            local_mass(i,j) += fe_values1.shape_value(i, q_point) *
                               fe_values1.shape_value(j, q_point) *
                               fe_values1.JxW (q_point);
      cell1->get_dof_indices (local_dof_indices);

      for(unsigned int i=0; i<dofs_per_cell; ++i)
        for(unsigned int j=0; j<dofs_per_cell; ++j)
          mass_matrix.block(0,0).add(local_dof_indices[i],
                                             local_dof_indices[j],
                                             local_mass(i,j));

    }

  typename DoFHandler<dim>::active_cell_iterator
    cell2 = dof_handler2.begin_active(),
    endc2 = dof_handler2.end();
  for(; cell2!=endc2; ++cell2)
    {
      fe_values2.reinit (cell2);
      const unsigned int dofs_per_cell = fe2.dofs_per_cell;
      std::vector<unsigned int> local_dof_indices (dofs_per_cell);

      FullMatrix<double>  local_mass (dofs_per_cell, dofs_per_cell);

      for(unsigned int q_point=0; q_point<n_q_points; ++q_point)
        for(unsigned int i=0; i<dofs_per_cell; ++i)
          for(unsigned int j=0; j<dofs_per_cell; ++j)
            local_mass(i,j) += fe_values2.shape_value(i, q_point) *
                               fe_values2.shape_value(j, q_point) *
                               fe_values2.JxW (q_point);
      cell2->get_dof_indices (local_dof_indices);

      for(unsigned int i=0; i<dofs_per_cell; ++i)
        for(unsigned int j=0; j<dofs_per_cell; ++j)
          mass_matrix.block(1,1).add(local_dof_indices[i],
                                             local_dof_indices[j],
                                             local_mass(i,j));

    }


  std::vector<std::map<unsigned int, double> > zero_boundary_values(solution.n_blocks());

  // Physic 1
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            0,
                                            ZeroFunction<dim>(),
                                            zero_boundary_values[0]);
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            1,
                                            ZeroFunction<dim>(),
                                            zero_boundary_values[0]);
  // Physic 2
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            0,
                                            ZeroFunction<dim>(),
                                            zero_boundary_values[1]);
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            1,
                                            ZeroFunction<dim>(),
                                            zero_boundary_values[1]);
  for(unsigned int i=0; i<solution.n_blocks(); i++)
    MatrixTools::apply_boundary_values (zero_boundary_values[i],
                                        mass_matrix.block(i,i),
                                        update.block(i),
                                        system_rhs.block(i));

}

template<int dim>
void blockPhysics<dim>::assemble_tr_system(const unsigned int stage_i, const double time)
{
  previous_F[stage_i] = 0.0;
  previous_F[stage_i].add(1.0, solution, -1.0, old_solution);

  mass_matrix.vmult(tmp_vector, previous_F[stage_i]);

  assemble_ss_system(time);
  set_Boundary_Conditions ();

  system_rhs.block(0) *= 1.0;//parameters.velocity1;
  system_rhs.block(1) *= 1.0;//parameters.velocity2;
  system_matrix.block(0,0) *= 1.0;//parameters.velocity1;
  system_matrix.block(0,1) *= 1.0;//parameters.velocity1;
  system_matrix.block(1,0) *= 1.0;//parameters.velocity2;
  system_matrix.block(1,1) *= 1.0;//parameters.velocity2;

  previous_F[stage_i] = system_rhs;
  system_rhs.sadd(-1.0 * time_step * A[stage_i][stage_i], tmp_vector);

  for(unsigned int stage_j=0; stage_j < stage_i; stage_j++)
    system_rhs.add(-1.0 * time_step * A[stage_i][stage_j], previous_F[stage_j]);

  system_rhs *= -1.0;

  system_matrix *= -1.0 * time_step * A[stage_i][stage_i];
  for(unsigned int i=0; i < solution.n_blocks(); i++)
    system_matrix.block(i,i).add(1.0,mass_matrix.block(i,i));

  constraints1.condense(system_matrix.block(0,0));
  constraints1.condense(system_rhs.block(0));
  constraints1.condense(previous_F[stage_i].block(0));
  constraints2.condense(system_matrix.block(1,1));
  constraints2.condense(system_rhs.block(1));
  constraints2.condense(previous_F[stage_i].block(1));

  set_Boundary_Conditions ();
}

template <int dim>
void blockPhysics<dim>::assemble_ss_system (const double time)
{
  system_matrix = 0;
  system_rhs = 0;

  assemble_physic1 ();
  assemble_physic2 ();

  {
    // Assemble off Diagonal Blocks
    const std::list<std::pair<typename DoFHandler<dim>::cell_iterator,
      typename DoFHandler<dim>::cell_iterator> > cell_list =
      GridTools::get_finest_common_cells (dof_handler1, dof_handler2);

    typename std::list<std::pair<typename DoFHandler<dim>::cell_iterator,
      typename DoFHandler<dim>::cell_iterator> >::const_iterator
      cell_iter = cell_list.begin();
    
    for (; cell_iter!=cell_list.end(); ++cell_iter){
      FullMatrix<double> unit_matrix (fe1.dofs_per_cell, fe2.dofs_per_cell);
      for(unsigned int i=0; i<unit_matrix.m(); ++i)
        unit_matrix(i, i) = 1;

      assemble_off_diagonal (cell_iter->first, cell_iter->second, unit_matrix, parameters.nu*(1-parameters.beta)*parameters.Fission_Const2, 0, 1);
      assemble_off_diagonal (cell_iter->second, cell_iter->first, unit_matrix, parameters.Scattering_Const1, 1, 0);
    }
  }

  assemble_load_vector (time);
  system_matrix.vmult_add (system_rhs, solution);

  {
    // Assemble Non-linear off Diagonal Blocks
    const std::list<std::pair<typename DoFHandler<dim>::cell_iterator,
      typename DoFHandler<dim>::cell_iterator> > cell_list =
      GridTools::get_finest_common_cells (dof_handler1, dof_handler2);

    typename std::list<std::pair<typename DoFHandler<dim>::cell_iterator,
      typename DoFHandler<dim>::cell_iterator> >::const_iterator
      cell_iter = cell_list.begin();
    
    for (; cell_iter!=cell_list.end(); ++cell_iter){
      FullMatrix<double> unit_matrix (fe1.dofs_per_cell, fe2.dofs_per_cell);
      for(unsigned int i=0; i<unit_matrix.m(); ++i)
        unit_matrix(i, i) = 1;

      assemble_off_diagonal_nonlinear (cell_iter->first, cell_iter->second, unit_matrix, 0, 1);
    }
  }

}

template <int dim>
void blockPhysics<dim>::set_Boundary_Conditions ()
{
  std::vector<std::map<unsigned int, double> > zero_boundary_values(solution.n_blocks());

  // Physic 1
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            0,
                                            ZeroFunction<dim>(),
                                            zero_boundary_values[0]);
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            1,
                                            ZeroFunction<dim>(),
                                            zero_boundary_values[0]);
  // Physic 2
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            0,
                                            ZeroFunction<dim>(),
                                            zero_boundary_values[1]);
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            1,
                                            ZeroFunction<dim>(),
                                            zero_boundary_values[1]);
  for(unsigned int i=0; i<solution.n_blocks(); i++)
    MatrixTools::apply_boundary_values (zero_boundary_values[i],
                                        system_matrix.block(i,i),
                                        update.block(i),
                                        system_rhs.block(i));

  // Clear Rows in Off Diagonal Blocks.
  for(unsigned int i=0; i < solution.n_blocks(); i++)
    for(unsigned int j=0; j < solution.n_blocks(); j++)
      if(j != i){
        typedef std::map<unsigned int, double>::iterator it_type;
        for(it_type iterator = zero_boundary_values[i].begin(); iterator != zero_boundary_values[i].end(); iterator++){
          typename SparseMatrix<double>::iterator
            entry  = system_matrix.block(i,j).begin(iterator->first),
            endMat = system_matrix.block(i,j).end(iterator->first);
          for(; entry != endMat; entry++)
            system_matrix.block(i,j).set(entry->row(),entry->column(), 0.0);
        }
      }

  std::vector<std::map<unsigned int, double> > boundary_values(solution.n_blocks());
  // Physic 1
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            0,
                                            ZeroFunction<dim>(),
                                            boundary_values[0]);
  VectorTools::interpolate_boundary_values (dof_handler1,
                                            1,
                                            ZeroFunction<dim>(),
                                            boundary_values[0]);
  // Physic 2
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            0,
                                            ZeroFunction<dim>(),
                                            boundary_values[1]);
  VectorTools::interpolate_boundary_values (dof_handler2,
                                            1,
                                            ZeroFunction<dim>(),
                                            boundary_values[1]);
  for(unsigned int i=0; i<solution.n_blocks(); i++)
    for(std::map<unsigned int, double>::const_iterator
          p = boundary_values[i].begin();
          p != boundary_values[i].end(); ++p)
      solution.block(i)(p->first) = p->second;
}

template <int dim>
void blockPhysics<dim>::assemble_physic1 ()
{
  QGauss<dim>     quadrature_formula (3);
  UpdateFlags update_flags = update_values | update_gradients | update_quadrature_points | update_JxW_values;
  FEValues<dim>   fe_values (fe1, quadrature_formula, update_flags);

  const unsigned int dofs_per_cell = fe1.dofs_per_cell;
  const unsigned int n_q_points    = quadrature_formula.size();

  FullMatrix<double>        cell_matrix (dofs_per_cell, dofs_per_cell);
  std::vector<unsigned int> local_dof_indices (dofs_per_cell);

  std::vector<double >      old_solution_values(n_q_points);
  Vector<double>            old_solution;
  old_solution.reinit(dof_handler1.n_dofs());

  VectorTools::interpolate_to_different_mesh (dof_handler2, //////////////////// Will Change when more physics are added!!!!!
                                              solution.block(1),
                                              dof_handler1,
                                              old_solution);

  typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler1.begin_active(),
    endc = dof_handler1.end();
  for(; cell!=endc; ++cell){
    fe_values.reinit (cell);

    fe_values.get_function_values(old_solution, old_solution_values);

    cell_matrix = 0;
    for(unsigned int q_point=0; q_point<n_q_points; ++q_point){
//      double SigmaA = parameters.Absorption_Const1 * (1.0 + parameters.Doppler_Const * (sqrt(old_solution_values[q_point]) - sqrt(parameters.T_ref)) );
      double SigmaA = parameters.Absorption_Const1 * (1.0 + parameters.Doppler_Const * (old_solution_values[q_point]*old_solution_values[q_point] - parameters.T_ref*parameters.T_ref) );
      for(unsigned int i=0; i<dofs_per_cell; ++i)
        for(unsigned int j=0; j<dofs_per_cell; ++j)
          cell_matrix(i,j) += ((-parameters.Diffusion_Const1 *
                                 fe_values.shape_grad (i, q_point) *
                                 fe_values.shape_grad (j, q_point) -
                                 (SigmaA + parameters.Scattering_Const1 - parameters.nu*(1-parameters.beta)*parameters.Fission_Const1)*
                                 fe_values.shape_value (i, q_point) *
                                 fe_values.shape_value (j, q_point)) *
                                 fe_values.JxW (q_point));
    }
    cell->get_dof_indices (local_dof_indices);

    for (unsigned int i=0; i<dofs_per_cell; ++i)
      for (unsigned int j=0; j<dofs_per_cell; ++j)
        system_matrix.block(0,0).add (local_dof_indices[i],
                           local_dof_indices[j],
                           cell_matrix(i,j));
  }
}

template <int dim>
void blockPhysics<dim>::assemble_physic2 ()
{
  QGauss<dim>     quadrature_formula (3);
  UpdateFlags update_flags = update_values | update_gradients | update_quadrature_points | update_JxW_values;
  FEValues<dim>   fe_values (fe2, quadrature_formula, update_flags);

  const unsigned int dofs_per_cell = fe2.dofs_per_cell;
  const unsigned int n_q_points    = quadrature_formula.size();

  FullMatrix<double>        cell_matrix (dofs_per_cell, dofs_per_cell);
  std::vector<unsigned int> local_dof_indices (dofs_per_cell);

  typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler2.begin_active(),
    endc = dof_handler2.end();
  for(; cell!=endc; ++cell){
    fe_values.reinit (cell);

    cell_matrix = 0;
    for(unsigned int q_point=0; q_point<n_q_points; ++q_point)
      for(unsigned int i=0; i<dofs_per_cell; ++i)
        for(unsigned int j=0; j<dofs_per_cell; ++j)
          cell_matrix(i,j) += ((-parameters.Diffusion_Const2 *
                                 fe_values.shape_grad (i, q_point) *
                                 fe_values.shape_grad (j, q_point) -
                                 parameters.Absorption_Const2 *
                                 fe_values.shape_value (i, q_point) *
                                 fe_values.shape_value (j, q_point)) *
                                 fe_values.JxW (q_point));
    cell->get_dof_indices (local_dof_indices);

    for (unsigned int i=0; i<dofs_per_cell; ++i)
      for (unsigned int j=0; j<dofs_per_cell; ++j)
        system_matrix.block(1,1).add (local_dof_indices[i],
                           local_dof_indices[j],
                           cell_matrix(i,j));
  }
}

template <int dim>
void blockPhysics<dim>::assemble_off_diagonal_nonlinear (const typename DoFHandler<dim>::cell_iterator &physic1,
                                                         const typename DoFHandler<dim>::cell_iterator &physic2,
                                                         const FullMatrix<double> prolongation_matrix,
                                                         const int blockM, const int blockN)
{
  if(!physic1->has_children() && !physic2->has_children() ){

      QGauss<dim>   quadrature_formula(3);
      FEValues<dim> fe_values1 (physic1->get_fe(), quadrature_formula,
                             update_values | update_gradients | update_JxW_values);
      FEValues<dim> fe_values2 (physic2->get_fe(), quadrature_formula,
                             update_values | update_gradients | update_JxW_values);

      const unsigned int   dofs_per_cell_1 = physic1->get_fe().dofs_per_cell;
      const unsigned int   dofs_per_cell_2 = physic2->get_fe().dofs_per_cell;
      const unsigned int   n_q_points    = quadrature_formula.size();

      FullMatrix<double>   cell_matrix3 (dofs_per_cell_1, dofs_per_cell_2);
      std::vector<unsigned int> local_dof_indices_1 (dofs_per_cell_1);
      std::vector<unsigned int> local_dof_indices_2 (dofs_per_cell_2);
      std::vector<double>  old_physic1_values(n_q_points);
      std::vector<double>  old_physic2_values(n_q_points);

      fe_values1.reinit (physic1);
      fe_values2.reinit (physic2);

      fe_values1.get_function_values(solution.block(blockM), old_physic1_values);
      fe_values2.get_function_values(solution.block(blockN), old_physic2_values);//////// Will Change when More physics are added: Temp is physic 5

      cell_matrix3 = 0;

      if(physic1->level() > physic2->level() ){
        for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
          // double dSigmaA = parameters.Absorption_Const1 * parameters.Doppler_Const / ( 2.0 * sqrt(old_physic2_values[q_point]) );
          double dSigmaA = parameters.Absorption_Const1 * parameters.Doppler_Const * 2.0 * old_physic2_values[q_point];
          for (unsigned int i=0; i<dofs_per_cell_1; ++i){
            for (unsigned int j=0; j<dofs_per_cell_2; ++j)
              cell_matrix3(i,j) += (-fe_values1.shape_value(j, q_point) *
                                    fe_values2.shape_value(i, q_point) *
                                    dSigmaA *
                                    old_physic1_values[q_point] *
                                    fe_values1.JxW (q_point));
          }
        }
      } else {
        for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
          //double dSigmaA = parameters.Absorption_Const1 * parameters.Doppler_Const / ( 2.0 * sqrt(old_physic2_values[q_point]) );
          double dSigmaA = parameters.Absorption_Const1 * parameters.Doppler_Const * 2.0 * old_physic2_values[q_point];
          for (unsigned int i=0; i<dofs_per_cell_1; ++i){
            for (unsigned int j=0; j<dofs_per_cell_2; ++j)
              cell_matrix3(i,j) += (-fe_values1.shape_value(j, q_point) *
                                    fe_values2.shape_value(i, q_point) *
                                    dSigmaA *
                                    old_physic1_values[q_point] *
                                    fe_values2.JxW (q_point));
          }
        }
      }

      FullMatrix<double> tmpM (dofs_per_cell_1, dofs_per_cell_2);
      // Apply prolongation matrix to cell matrix
      if(physic1->level() < physic2->level() ){
        prolongation_matrix.Tmmult (tmpM, cell_matrix3);
      } else {
        cell_matrix3.mmult (tmpM, prolongation_matrix);
      }
      cell_matrix3 = tmpM;
      
      physic1->get_dof_indices (local_dof_indices_1);

      for (unsigned int i=0; i<dofs_per_cell_1; ++i){
        physic2->get_dof_indices (local_dof_indices_2);
        for (unsigned int j=0; j<dofs_per_cell_2; ++j){
          system_matrix.block(blockM,blockN).add (local_dof_indices_1[i],
                             local_dof_indices_2[j],
                             cell_matrix3(i,j));
        }
      }

    } else {

      for(unsigned int child=0; child<GeometryInfo<dim>::max_children_per_cell; child++){
        FullMatrix<double> new_matrix (physic1->get_fe().dofs_per_cell, physic2->get_fe().dofs_per_cell);
        prolongation_matrix.mmult (new_matrix, physic2->get_fe().get_prolongation_matrix(child));
        if(physic1->has_children()){
          assemble_off_diagonal_nonlinear (physic1->child(child), physic2, new_matrix, blockM, blockN);
        } else {
          assemble_off_diagonal_nonlinear (physic1, physic2->child(child), new_matrix, blockM, blockN);
        }
      }
    }
}

template <int dim>
void blockPhysics<dim>::assemble_off_diagonal(const typename DoFHandler<dim>::cell_iterator &physic1,
                                              const typename DoFHandler<dim>::cell_iterator &physic2,
                                              const FullMatrix<double> prolongation_matrix,
                                              const double Coeff, const int blockM, const int blockN)
{
  if(!physic1->has_children() && !physic2->has_children() ){

      QGauss<dim>   quadrature_formula(3);
      FEValues<dim> fe_values1 (physic1->get_fe(), quadrature_formula,
                             update_values | update_gradients | update_JxW_values);
      FEValues<dim> fe_values2 (physic2->get_fe(), quadrature_formula,
                             update_values | update_gradients | update_JxW_values);

      const unsigned int   dofs_per_cell_1 = physic1->get_fe().dofs_per_cell;
      const unsigned int   dofs_per_cell_2 = physic2->get_fe().dofs_per_cell;
      const unsigned int   n_q_points    = quadrature_formula.size();

      FullMatrix<double>   cell_matrix3 (dofs_per_cell_1, dofs_per_cell_2);
      std::vector<unsigned int> local_dof_indices_1 (dofs_per_cell_1);
      std::vector<unsigned int> local_dof_indices_2 (dofs_per_cell_2);

      fe_values1.reinit (physic1);
      fe_values2.reinit (physic2);

      cell_matrix3 = 0;

      if(physic1->level() > physic2->level() ){
        for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
          for (unsigned int i=0; i<dofs_per_cell_1; ++i){
            for (unsigned int j=0; j<dofs_per_cell_2; ++j)
              cell_matrix3(i,j) += (fe_values1.shape_value(j, q_point) *
                                    fe_values2.shape_value(i, q_point) *
                                    Coeff *
                                    fe_values1.JxW (q_point));
          }
        }
      } else {
        for (unsigned int q_point=0; q_point<n_q_points; ++q_point){
          for (unsigned int i=0; i<dofs_per_cell_1; ++i){
            for (unsigned int j=0; j<dofs_per_cell_2; ++j)
              cell_matrix3(i,j) += (fe_values1.shape_value(j, q_point) *
                                    fe_values2.shape_value(i, q_point) *
                                    Coeff *
                                    fe_values2.JxW (q_point));
          }
        }
      }

      FullMatrix<double> tmpM (dofs_per_cell_1, dofs_per_cell_2);
      // Apply prolongation matrix to cell matrix
      if(physic1->level() < physic2->level() ){
        prolongation_matrix.Tmmult (tmpM, cell_matrix3);
      } else {
        cell_matrix3.mmult (tmpM, prolongation_matrix);
      }
      cell_matrix3 = tmpM;
      
      physic1->get_dof_indices (local_dof_indices_1);

      for (unsigned int i=0; i<dofs_per_cell_1; ++i){
        physic2->get_dof_indices (local_dof_indices_2);
        for (unsigned int j=0; j<dofs_per_cell_2; ++j){
          system_matrix.block(blockM,blockN).add (local_dof_indices_1[i],
                             local_dof_indices_2[j],
                             cell_matrix3(i,j));
        }
      }

    } else {

      for(unsigned int child=0; child<GeometryInfo<dim>::max_children_per_cell; child++){
        FullMatrix<double> new_matrix (physic1->get_fe().dofs_per_cell, physic2->get_fe().dofs_per_cell);
        prolongation_matrix.mmult (new_matrix, physic2->get_fe().get_prolongation_matrix(child));
        if(physic1->has_children()){
          assemble_off_diagonal (physic1->child(child), physic2, new_matrix, Coeff, blockM, blockN);
        } else {
          assemble_off_diagonal (physic1, physic2->child(child), new_matrix, Coeff, blockM, blockN);
        }
      }
    }
}

template <int dim>
void blockPhysics<dim>::assemble_load_vector (const double time)
{

  { // Physic 1
    QGauss<dim>     quadrature_formula (4);
    UpdateFlags update_flags = update_values | update_gradients | update_quadrature_points | update_JxW_values;
    FEValues<dim>   fe_values (fe1, quadrature_formula, update_flags);

    const unsigned int dofs_per_cell = fe1.dofs_per_cell;
    const unsigned int n_q_points    = quadrature_formula.size();

    Vector<double>            cell_rhs (dofs_per_cell);
    std::vector<unsigned int> local_dof_indices (dofs_per_cell);

    RightHandSide1<dim> RHS(parameters);
    std::vector<double> rhs_values (n_q_points);
    RHS.set_time(time);

    typename DoFHandler<dim>::active_cell_iterator
      cell = dof_handler1.begin_active(),
      endc = dof_handler1.end();
    for(; cell!=endc; ++cell){
      fe_values.reinit (cell);
      RHS.value_list (fe_values.get_quadrature_points(),
                      rhs_values);

      cell_rhs = 0;
      for(unsigned int q_point=0; q_point<n_q_points; ++q_point)
        for(unsigned int i=0; i<dofs_per_cell; ++i)
            cell_rhs(i) += rhs_values[q_point] *
                           fe_values.shape_value (i, q_point) *
                           fe_values.JxW (q_point);
      cell->get_dof_indices (local_dof_indices);

      for (unsigned int i=0; i<dofs_per_cell; ++i)
        system_rhs.block(0)(local_dof_indices[i]) += cell_rhs(i);
    }
  }{ // Physic 2
    QGauss<dim>     quadrature_formula (4);
    UpdateFlags update_flags = update_values | update_gradients | update_quadrature_points | update_JxW_values;
    FEValues<dim>   fe_values (fe2, quadrature_formula, update_flags);

    const unsigned int dofs_per_cell = fe2.dofs_per_cell;
    const unsigned int n_q_points    = quadrature_formula.size();

    Vector<double>            cell_rhs (dofs_per_cell);
    std::vector<unsigned int> local_dof_indices (dofs_per_cell);

    RightHandSide2<dim> RHS(parameters);
    std::vector<double> rhs_values (n_q_points);
    RHS.set_time(time);

    typename DoFHandler<dim>::active_cell_iterator
      cell = dof_handler2.begin_active(),
      endc = dof_handler2.end();
    for(; cell!=endc; ++cell){
      fe_values.reinit (cell);
      RHS.value_list (fe_values.get_quadrature_points(),
                      rhs_values);

      cell_rhs = 0;
      for(unsigned int q_point=0; q_point<n_q_points; ++q_point)
        for(unsigned int i=0; i<dofs_per_cell; ++i)
            cell_rhs(i) += rhs_values[q_point] *
                           fe_values.shape_value (i, q_point) *
                           fe_values.JxW (q_point);
      cell->get_dof_indices (local_dof_indices);

      for (unsigned int i=0; i<dofs_per_cell; ++i)
        system_rhs.block(1)(local_dof_indices[i]) += cell_rhs(i);
    }
  }

  system_rhs *= -1.0;

}

template <int dim>
void blockPhysics<dim>::update_solution ()
{
  double alpha = 1.0; // Relaxation parameter
  solution.add (alpha, update);
}

template <int dim>
void blockPhysics<dim>::newton_solve (const unsigned int stage_i, const double time)
{

  int iter = 1;
  bool done = false;
  double Res_tol = 0.0, residual_norm = 0.0;
  do{

    assemble_tr_system (stage_i, time);
    linear_solve ();
    update_solution ();
    residual_norm = system_rhs.l2_norm ();

    if(iter == 1) Res_tol = parameters.nonLinear_atol + parameters.nonLinear_rtol * residual_norm;

    if(residual_norm < Res_tol)
      done = true;
    if(iter >= parameters.max_nonLinear_iterations)
      done = true;
    if(done == false) iter ++;

  } while (!done);

//  std::cout << "Converged in " << iter << " Newton Iterations" << std::endl;
}

template <int dim>
void blockPhysics<dim>::linear_solve ()
{
  Vector<double>      b(system_rhs.size());
  Vector<double>      x(update.size());
  SparseDirectUMFPACK A_direct;

  b = system_rhs;
  x = update;

  A_direct.initialize(system_matrix);
  A_direct.vmult(x,b);

  system_rhs = b;
  update = x;

  constraints1.distribute (update.block(0));
  constraints2.distribute (update.block(1));

}

template <int dim>
void blockPhysics<dim>::process_solution ()
{
  double total_error = 0.0;
  {
  Vector<float>   difference_per_cell (triangulation1.n_active_cells());
  Solution1<dim>  ExactSolution (parameters);
  VectorTools::integrate_difference (dof_handler1,
                                     solution.block(0),
                                     ExactSolution,
                                     difference_per_cell,
                                     QGauss<dim>(3),
                                     VectorTools::L2_norm);
  double L2_error = difference_per_cell.l2_norm();
  total_error += L2_error;
  std::cout << "DoFs: " << dof_handler1.n_dofs() << " Error: " << L2_error;
  }{
  Vector<float>   difference_per_cell (triangulation2.n_active_cells());
  Solution2<dim>  ExactSolution (parameters);
  VectorTools::integrate_difference (dof_handler2,
                                     solution.block(1),
                                     ExactSolution,
                                     difference_per_cell,
                                     QGauss<dim>(3),
                                     VectorTools::L2_norm);
  double L2_error = difference_per_cell.l2_norm();
  total_error += L2_error;
  std::cout << " DoFs: " << dof_handler2.n_dofs()
		    << " Error: " << L2_error;
  }
  std::cout << std::endl;
  
}

template <int dim>
void blockPhysics<dim>::process_solution (const double time)
{
  double total_error = 0.0;
  {
  Vector<float>   difference_per_cell (triangulation1.n_active_cells());
  Solution1<dim>  ExactSolution (parameters);
  ExactSolution.set_time(time);


                  Point<dim> pt;
                  switch(dim){
                  case 1:
                    pt =  Point<dim> (0.0);
                    break;
                  case 2:
                    pt =  Point<dim> (7.5,5.0);
                    break;
                  case 3:
                    pt = Point<dim> (0.0,0.0,0.0);
                    break;
                  }
                 Vector<double> diff (1);
                 VectorTools::point_difference (dof_handler1,
                                                solution.block(0),
                                                ExactSolution,
                                                diff,
                                                pt);
                 std::cout << "pt diff = : " << diff(0) << std::endl;

                 VectorTools::point_value (dof_handler1,
                                           solution.block(0),
                                           pt,
                                           diff);
                 std::cout << "pt value FEM solutuion = : " << diff(0) << std::endl;

                 std::cout << "pt value of EXACT sol  = : " << ExactSolution.value(pt);
                 std::cout << std::endl;



  VectorTools::integrate_difference (dof_handler1,
                                     solution.block(0),
                                     ExactSolution,
                                     difference_per_cell,
                                     QGauss<dim>(3),
                                     VectorTools::L2_norm);
/*
  std::cout << "Time: " << time << "\tError: " << std::endl;
  for(unsigned int i =0; i < difference_per_cell.size(); i++)
    std::cout << difference_per_cell(i) << std::endl;
*/
  double L2_error = difference_per_cell.l2_norm();
  total_error += L2_error;
  std::cout << "DoFs: " << dof_handler1.n_dofs() << " Error: " << L2_error;
  }{
  Vector<float>   difference_per_cell (triangulation2.n_active_cells());
  Solution2<dim>  ExactSolution (parameters);
  ExactSolution.set_time(time);

/*
                Point<dim> pt;
                switch(dim){
                case 1:
                  pt =  Point<dim> (0.0);
                  break;
                case 2:
                  pt =  Point<dim> (parameters.length/2.0,0.0);
                  break;
                case 3:
                  pt = Point<dim> (0.0,0.0,0.0);
                  break;
                }
               Vector<double> diff (1);
               VectorTools::point_difference (dof_handler2,
                                              solution.block(1),
                                              ExactSolution,
                                              diff,
                                              pt);
               std::cout << std::endl << "pt diff = : " << diff(0) << std::endl;

               VectorTools::point_value (dof_handler2,
                                         solution.block(1),
                                         pt,
                                         diff);
               std::cout << "pt value FEM solutuion = : " << diff(0) << std::endl;

               std::cout << "pt value of EXACT sol  = : " << ExactSolution.value(pt);
               std::cout << std::endl;
*/

  VectorTools::integrate_difference (dof_handler2,
                                     solution.block(1),
                                     ExactSolution,
                                     difference_per_cell,
                                     QGauss<dim>(3),
                                     VectorTools::L2_norm);
  double L2_error = difference_per_cell.l2_norm();
  total_error += L2_error;
  std::cout << " DoFs: " << dof_handler2.n_dofs() << " Error: " << L2_error;
  }
  std::cout << std::endl;

}

template <int dim>
void blockPhysics<dim>::process_solution (const double time, const unsigned int steps)
{

  std::string outputFileName = "13RestartMesh.out";
  std::ofstream outputFile (outputFileName.c_str());
  double total_error = 0.0;
  {
  Vector<float>   difference_per_cell (triangulation1.n_active_cells());
  Solution1<dim>  ExactSolution (parameters);
  ExactSolution.set_time(time);
  VectorTools::integrate_difference (dof_handler1,
                                     solution.block(0),
                                     ExactSolution,
                                     difference_per_cell,
                                     QGauss<dim>(3),
                                     VectorTools::L2_norm);
  double L2_error = difference_per_cell.l2_norm();
  total_error += L2_error;
  std::cout << "Steps: " << steps
            << " Refinements: " << parameters.n_global_refinements + parameters.n_init_refinements
            << " Error: " << L2_error;
  outputFile << "Steps: " << steps
            << " DoFs: " << dof_handler1.n_dofs()
            << " Error: " << L2_error;


    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler1);
    data_out.add_data_vector (difference_per_cell, "Error");
    data_out.build_patches ();

    std::string fileName = "Error-Per-Cell-Fast.vtk";
    std::ofstream output (fileName.c_str());
    data_out.write_vtk (output); 




  }{
  Vector<float>   difference_per_cell (triangulation2.n_active_cells());
  Solution2<dim>  ExactSolution (parameters);
  ExactSolution.set_time(time);
  VectorTools::integrate_difference (dof_handler2,
                                     solution.block(1),
                                     ExactSolution,
                                     difference_per_cell,
                                     QGauss<dim>(3),
                                     VectorTools::L2_norm);
  double L2_error = difference_per_cell.l2_norm();
  total_error += L2_error;
  std::cout << " Error: " << L2_error;
  outputFile << " Error: " << L2_error;

    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler2);
    data_out.add_data_vector (difference_per_cell, "Error");
    data_out.build_patches ();

    std::string fileName = "Error-Per-Cell-Ther.vtk";
    std::ofstream output (fileName.c_str());
    data_out.write_vtk (output);

  }
  std::cout << std::endl;
  outputFile << std::endl;

}

template <int dim>
void blockPhysics<dim>::output_results (const unsigned int time_step)
{
  std::vector<std::string> solution_names;
  solution_names.push_back("Fast_Flux");
  solution_names.push_back("Thermal_Flux");
  {
    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler1);
    data_out.add_data_vector (old_solution.block(0), solution_names[0]);
    data_out.add_data_vector (current_error[0], "Fast_Flux_Error");
    data_out.build_patches ();

    std::string fileName = "solution-FFlux-";
    if(time_step < 10)
      fileName += "0";
    fileName += Utilities::int_to_string(time_step);

    if(dim ==1)
      fileName += ".gpl";
    else if(dim == 2 || dim == 3)
      fileName += ".vtk";
    std::ofstream output (fileName.c_str());
    if(dim == 1)
      data_out.write_gnuplot(output);
    else if(dim == 2 || dim == 3)
      data_out.write_vtk (output);
  }{
    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler2);
    data_out.add_data_vector (old_solution.block(1), solution_names[1]);
    data_out.add_data_vector (current_error[1], "Thermal_Flux_Error");
    data_out.build_patches ();

    std::string fileName = "solution-TFlux-";
    if(time_step < 10)
      fileName += "0";
    fileName += Utilities::int_to_string(time_step);

    if(dim ==1)
      fileName += ".gpl";
    else if(dim == 2 || dim == 3)
      fileName += ".vtk";
    std::ofstream output (fileName.c_str());
    if(dim == 1)
      data_out.write_gnuplot(output);
    else if(dim == 2 || dim == 3)
      data_out.write_vtk (output);
  }
}

template <int dim>
void blockPhysics<dim>::output_exact (const double time)
{

  std::vector<std::string> solution_names;
  solution_names.push_back("Flux");
  solution_names.push_back("Temp");
  {
    Solution1<dim>    ExactSolution(parameters);
    ExactSolution.set_time(time);
    VectorTools::interpolate(dof_handler1,
                             ExactSolution, 
                             update.block(0));
    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler1);
    data_out.add_data_vector (update.block(0), solution_names[0]);
    data_out.build_patches ();

    std::string fileName;
    if(dim == 1)
      fileName = "exact-solution-FFlux.gpl";
    else if(dim == 2 || dim == 3)
      fileName = "exact-solution-FFlux.vtk";
    std::ofstream output (fileName);
    if(dim == 1)
      data_out.write_gnuplot (output);
    else if(dim == 2 || dim == 3)
      data_out.write_vtk (output);
  }{
    Solution2<dim>    ExactSolution(parameters);
    ExactSolution.set_time(time);
    VectorTools::interpolate(dof_handler2,
                             ExactSolution, 
                             update.block(1));

    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler2);
    data_out.add_data_vector (update.block(1), solution_names[1]);
    data_out.build_patches ();

    std::string fileName;
    if(dim == 1)
      fileName = "exact-solution-TFlux.gpl";
    else if(dim == 2 || dim == 3)
      fileName = "exact-solution-TFlux.vtk";
    std::ofstream output (fileName);
    if(dim == 1)
      data_out.write_gnuplot (output);
    else if(dim == 2 || dim == 3)
      data_out.write_vtk (output);
  }
}

} // End Namespace : MultiPhysics

int main (){

  const int dimension = 2;
  deallog.depth_console (0);

  std::string input_filename = "input_params.prm";
  ParameterHandler prm;
  Parameters::AllParameters::declare_parameters (prm);
  prm.read_input (input_filename.c_str());
  Parameters::AllParameters parameters;
  parameters.parse_parameters (prm);

  MultiPhysics::blockPhysics<dimension> problem(parameters);
  problem.run ();

  return 0;
}
