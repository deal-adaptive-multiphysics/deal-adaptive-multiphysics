#!/user/bin/perl -w
use strict;
# This code is used to test convergence of a code written in Deal.II.
# This code modifies parameters in a prm file used to control the
# Deal.II code. It loops over all convergence parameters (time step
# size and refinement level) and outputs the error output from the
# Deal.II code to a file.

#======================================================================
#                 User Parameters
#======================================================================
my $basefile = '13RestartMesh2';       # Deal.II file
my $dataFolder = 'ConvData';          # Folder that will store Output Data
my $N_refs = 7;                       # Number of refinement levels to explore
                                      # 1, 2, 3, ..., N_refs
my $N_steps = 10;                      # Number of time steps to explore
                                      # 1, 2, 4, ..., 2^(N_steps-1)

#======================================================================
#                 Program Parameters
#======================================================================
my @refinements;                      # Array to store Refinement data from output
my @timeSteps;                        # Array to store Time Step data from output
my @error1;                           # Array to store error 1 data from output
my @error2;                           # Array to store error 2 data from output
my @Arefinements;                     # Array of adaptive refinements (for changing
                                      # inputs) 
my @AtimeSteps;                       # Array of time steps (for changing inputs) 
for(my $w=0; $w < $N_refs; $w++){
  $Arefinements[$w] = $w+1;
}
for(my $w=0; $w < $N_steps; $w++){
  $AtimeSteps[$w] = 2**($w);
}
system("rm -r $dataFolder");
system("mkdir $dataFolder");

#======================================================================
#                 Initialize Input File
#======================================================================
my $init_file = 'input_params.prm';     # Input File Name
my $oldTS = "";
my $newTS = "";
my $oldAR = "";
my $newAR = "";
my $zero = 0;

# Search input file for Time Step string and initialize varables $old, $new
# These variables will be switched in new input file
open(OUT, '<', $init_file);
while (<OUT>){
  if(/set/ && /Time/ && /Steps/){
    my $line = $_;
    my @words = split(" ", $line);
    my $oldSteps = $words[4];
    $oldTS = "  set Time Steps   = $oldSteps";
    $newTS = "  set Time Steps   = $AtimeSteps[$zero]";
  }
  if(/set/ && /Initial/ && /Adaptive/ && /Refinements/){
    my $line = $_;
    my @words = split(" ", $line);
    my $oldRefs = $words[5];
    $oldAR = "  set Initial Adaptive Refinements = $oldRefs";
    $newAR = "  set Initial Adaptive Refinements = $Arefinements[$zero]";
  }
}
close(OUT);

# Store input file into variable @code
open(OUT, '<', $init_file);
my @code = <OUT>;
close(OUT);

# Loop over code and replace string $old with $new
open(OUT, '>', $init_file);
for(@code){
  s/$oldTS/$newTS/;
  s/$oldAR/$newAR/;
  print OUT;
}
close(OUT);


#======================================================================
#                 Loop over all Time Steps and Refinements
#======================================================================
for(my $k=0; $k < $N_refs; $k++){
  for(my $m=0; $m < $N_steps; $m++){

    # Run The Program
    system("$basefile");

    # Search the output file for data
    my $filename = $basefile.'.out';
    open(OUT, '<', $filename);

    while(<OUT>){
      if(/Steps:/){
        my $line = $_;
        my @words = split(" ",$line);
        my $steps = $words[1];
        my $refs  = $words[3];
        my $err1  = $words[5];
        my $err2  = $words[7];
        push(@refinements, $refs);
        push(@timeSteps, $steps);
        push(@error1, $err1);
        push(@error2, $err2);
      }
    }

    # Modify the Input file for Time Steps
    # If last time step, reinitialize
    my $mNext = $m+1;
    $oldTS = "  set Time Steps   = $AtimeSteps[$m]";
    if($m == $N_steps -1){
      $newTS = "  set Time Steps   = $AtimeSteps[$zero]";
    }else{
      $newTS = "  set Time Steps   = $AtimeSteps[$mNext]";
    }
    open(OUT,'<',$init_file);
    my @code = <OUT>;
    close(OUT);

    open(OUT,'>',$init_file);
    for(@code){
      s/$oldTS/$newTS/;
      print OUT;
    }
    close(OUT);
  }

  # Modify the Input file for Refinements
  # If last time step, reinitialize
  my $kNext = $k+1;
  $oldAR = "  set Initial Adaptive Refinements = $Arefinements[$k]";
  if($k == $N_refs -1){
    $newAR = "  set Initial Adaptive Refinements = $Arefinements[$zero]";
  }else{
    $newAR = "  set Initial Adaptive Refinements = $Arefinements[$kNext]";
  }
  open(OUT,'<',$init_file);
  my @code = <OUT>;
  close(OUT);

  open(OUT,'>',$init_file);
  for(@code){
    s/$oldAR/$newAR/;
    print OUT;
  }
  close(OUT);

}

#======================================================================
#            Write Data to Output
#======================================================================
# Time Convergence
for(my $i=0; $i<$N_refs; $i++){
  my $outFile = "$dataFolder/TimeConv$i.dat";
  open(OUT, '>', $outFile);
  for(my $j=0; $j<$N_steps; $j++){
    my $index = $i*$N_steps + $j;
    print OUT "Steps: $timeSteps[$index] DoFs: $refinements[$index] Error: $error1[$index] Error: $error2[$index]\n";
  }
  close(OUT);
}

#Gnuplot File (Time)
my $gplFile = "$dataFolder/Time.gpl";
open(OUT, '>', $gplFile);
print OUT "set logscale xy\n";
print OUT "plot ";
for(my $i=0; $i<$N_refs; $i++){
  my $index = $i*$N_steps;
  print OUT "\"TimeConv$i.dat\" using 2:6 title \"$refinements[$index] DoFs\"";
  if($i != $N_refs-1){
    print OUT ", ";
  }
}
close(OUT);


# Spatial Convergence
for(my $i=0; $i<$N_steps; $i++){
  my $outFile = "$dataFolder/SpatialConv$i.dat";
  open(OUT, '>', $outFile);
  for(my $j=0; $j<$N_refs; $j++){
    my $index = $j*$N_steps + $i;
    print OUT "Steps: $timeSteps[$index] DoFs: $refinements[$index] Error: $error1[$index] Error: $error2[$index]\n";
  }
  close(OUT);
}

#Gnuplot File (Space)
$gplFile = "$dataFolder/Space.gpl";
open(OUT, '>', $gplFile);
print OUT "set logscale xy\n";
print OUT "plot ";
for(my $i=0; $i<$N_steps; $i++){
  print OUT "\"SpatialConv$i.dat\" using 4:6 title \"$timeSteps[$i] Time Steps\"";
  if($i != $N_refs-1){
    print OUT ", ";
  }
}
close(OUT);

my $outputFile = "$dataFolder/Convergence.dat";
my $totalSize = $N_refs*$N_steps;
open(OUT, '>', $outputFile);
for(my $p=0; $p<$totalSize; $p++){
  print OUT "Steps: $timeSteps[$p] DoFs: $refinements[$p] Error: $error1[$p] Error: $error2[$p]\n";
  if($p % $N_steps == 0){
    print OUT "\n";
  }
}
close(OUT);



