class PhysicsBase
{
  public:
    virtual void assemble_this_system (...,
                                       std::map<std::string, PhysicsBase*> &other_physics) = 0;
    virtual void initialize () = 0;
  protected:
     DoFHandler *dof_handler;
     ConstraintMatrix *cm;
     unsigned int my_block_number;
     BlockSparseMatrix *system_matrix;
     BlockSparseMatrix *system_rhs;
};

class Neutronics : public PhysicsBase
{
  public:
    virtual void initialize () 
    {
       dof_handler.distribute_dofs(fe_space);
       ...resize matrices, etc...
    }

private:
  FESystem fe_space;
};



void
Neutronics::assemble_this_system (...,
                                  std::map<std::string, PhysicsBase*> &other_physics)
{
 ... usual stuff ...
  for (it = other_physics.begin(); ...)
    if (it->first == "thermal"))
      {
       ... assemble coupling terms in system_matrix.block (my_block_number,
                                                           other_physics["thermal"].my_block_number) ...
      }
   else if (it->first == "elastic")
      ...;
   else
     Assert (false, ExcNotImplemented());
}



class Super
{
  std::vector<PhysicsBase*> physics;

  FESystem   *global_fe;
  DoFHandler dof_handler;
  ConstraintMatrix cm;

  void initialize () {
    ... create physics objects...

    global_fe = new FESystem (fe_1, 1,
                              fe_2, 1,
                              fe_3, 1);
    dof_handler.distribute_dofs (*global_fe);
    DoFTools::make_hanging_n_c (cm);
