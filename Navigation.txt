00poissonExample
==================================================
Copied from Deal.ii example problem with no modifications

01diffusionExample
==================================================
Adds a diffusion term in the system matrix from 00poissonExample

02twoPhysics-SingleMesh-hAMR
==================================================
Builds a 2x2 block matrix with two physics (neutron diffusion, linear
heat conduction). The block matrix is stored using component masks so
that the block physics are not completely separated. Both physics are
built on the same mesh with the same DoFhandler. Refinement is based
on the neutron flux by use of a component mask.

03twoPhysics-NewtonSolve
===================================================
Builds same system as 02twoPhysics except that the linear problem is
constructed in a NewtonSolve framework. Thus the Newton Iterations should
converge in no more than two iterations.

04twoPhysics-NewtonSolve-Blocks
===================================================
Performs the same calculation as 03twoPhysics but the system matrix
is now stored in a block matrix format.

05twoPhysics-NonLinear-Multimesh-hAMR
===================================================
Introduces nonlinear thermal conductivity in the temperature equation.
Also allows each physics component to be solved on an independent mesh.
Linear coupling in temperature equation. 

06Verification-Coupled-hAMMR
===================================================
Introduces MMS and produces convergence tables for each solution

07Optimize-Nonlinear-TwoPhysic
===================================================
This cleans up the previous project 06Verification by using a parameter
file to control program behavior, and moving MMS solutions to separate files.
Also the assembly is broken into assebling different physics components. A
direct solver is also introduced to the linear solve function with a switch
controlled by the parameter file.

08RungeKutta
===================================================
Introduces time dependence for the first time. Calculates the solution error
at the end of the time interval for multiple time steps.

09hAMR-Test
===================================================
This was made when I thought I was having trouble with hAMR. I was using a
cosine solution and the convergence rate for hAMR was about the same as uniform
refinement. I think I changed the manufactured solution and learned which type
of solution should be used for convergence plots.

10hpAMR-Test
==================================================
This was an attempt to get hpAMR to work for the two Physics problem, but
it seemed much more difficult than was worth. The multiple meshes and DoF
handlers made using hpAMR difficult.

11MultiPhysics
==================================================
This was taking 08RungeKutta and making it general for more than two physics.
This also changed the physical model; what is actually modeled is two group
neutron diffusion with a nonlinear fast absorption cross section. This also
adds refining spatially in time when the spatial error has deviated
significantly.

12PropagatingMesh
==================================================
I can't find a large difference between 11&12, but 12 is documented more clearly.

13RestartMesh
==================================================
This takes 12 and adds in the recycling. The problem I have run into is that
one physic will dominate the time evolution. The physic changing more rapidly
will dominate the slower solution because of the limit on number of cycles.

13RestartMesh2 is just a copy of the previous directory so I could run more problems simultaneously

ThesisResults
===================================================
Broken into different directories based on the solution being tested.
  Maxwellian: Initially smooth solution with growing Gaussian peak
  Maxwellian2: Same as above but with additional parameter to make peak
               narrower and larger.
  QuadraticTimeDep: Used to test time integrators. Quadratic in space and
                    sinusoidal in time.
  StationaryGauss: Used to test spatial adaptivity. Gaussian in sapce
  TravelingGauss: Used to test the propagating mesh feature (using same
                  spatial mesh for several time steps).
