#ifndef EXACT_SOLNS_HH
#define EXACT_SOLNS_HH

using namespace dealii;

/**
 * @brief Exact Solution for Physic 1
 * @details Exact Solution for the first physic in a two coupled physic problem.
 *          The solution has the form 
 *  @f$ U(\vec{x},t) = C_u \prod_{i=1}^d \left[ 1 - \left(\frac{x_i}{L} \right)^2 \right] e^{\left[x_i - x_i^0(t)\right]^2} @f$.
 *
 * @author Kevin Dugan, 2013
 */
template <int dim>
class Solution1 : public Function<dim>
{
  public:
    /**
     * @brief Constructor for exact solution to Physic 1
     * 
     * @param params Parameters from the file parameters.hh
     */
    Solution1 (const Parameters::AllParameters params) : Function<dim>(),
                                                           params(params) {}
    virtual ~Solution1 () {}

    /**
     * @brief Exact Solution value
     * @details Value of the exact solution at the point <code>p</code>.
     */
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    /**
     * @brief Parameter object.
     * @details Object that stores all relevent data for this project. [Constants, Time Integrator Parameters, etc. ]
     */
    const Parameters::AllParameters params;
};

/**
 * @brief Exact Solution for Physic 2
 * @details Exact Solution for the second physic in a two coupled physic problem.
 *          The solution has the form 
 *  @f$ U(\vec{x},t) = C_u \prod_{i=1}^d \left[ 1 - \left(\frac{x_i}{L} \right)^2 \right] e^{\left[x_i - x_i^0(t)\right]^2} @f$.
 *
 * @author Kevin Dugan, 2013
 */
template <int dim>
class Solution2 : public Function<dim>
{
  public:
    /**
     * @brief Constructor for exact solution to Physic 2
     * 
     * @param params Parameters from the file parameters.hh
     */
    Solution2 (const Parameters::AllParameters params) : Function<dim>(),
                                                           params(params) {}
    virtual ~Solution2 () {}

    /**
     * @brief Exact Solution value
     * @details Value of the exact solution at the point <code>p</code>.
     */
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;

    /**
     * @brief Parameter object.
     * @details Object that stores all relevent data for this project. [Constants, Time Integrator Parameters, etc. ]
     */
    const Parameters::AllParameters params;
};


//==========================================================================================
//==========================================================================================
//                                       Implementations
//==========================================================================================
//==========================================================================================

template <int dim>
double Solution1<dim>::value (const Point<dim> &p,
                              const unsigned int) const
{
  double time = this->get_time();

  double value;
  if(dim == 1){
    if(params.exact_solution_type == Parameters::AllParameters::gaussian){
      double x0 = params.xPosition1*cos(params.solutionSpeed1 * time);
      value = params.C_phi1 * (-(p[0]/params.length)*(p[0]/params.length) + 1)
                            * exp (-(p[0] - x0)*(p[0] - x0));
    }else if(params.exact_solution_type == Parameters::AllParameters::quadratic){
        value = params.C_phi1 * (sin(params.solutionSpeed1 * time) + 1.0) * (1.0 - (p[0]/params.length)*(p[0]/params.length));
    }else if(params.exact_solution_type == Parameters::AllParameters::maxwellian){
        double x0 = params.xPosition1*cos(params.solutionSpeed1 * time);
        value = params.C_phi1 * (1.0 - (p[0]/params.length)*(p[0]/params.length))
                              * (1.0 + time * exp(-params.growthSpeed1 * time) * exp (-(p[0] - x0)*(p[0] - x0)));
    }else
      AssertThrow(false, ExcMessage("Solution Type Not Implemented"));
  }else if (dim == 2){
    if(params.exact_solution_type == Parameters::AllParameters::gaussian){
      double x0 = params.xPosition1*cos(params.solutionSpeed1 * time);
      double y0 = params.yPosition1*sin(params.solutionSpeed1 * time);
      value = params.C_phi1 * (-(p[0]/params.length)*(p[0]/params.length) + 1)
                            * (-(p[1]/params.length)*(p[1]/params.length) + 1)
                            * exp (-(p[0] - x0)*(p[0] - x0) - (p[1] - y0)*(p[1] - y0));
    }else if(params.exact_solution_type == Parameters::AllParameters::quadratic){
      value = params.C_phi1 * (sin(params.solutionSpeed1 * time) + 1.0) * (1.0 - (p[0]/params.length)*(p[0]/params.length)) * (1.0 - (p[1]/params.length)*(p[1]/params.length));
    }else if(params.exact_solution_type == Parameters::AllParameters::maxwellian){
        double x0 = params.xPosition1*cos(params.solutionSpeed1 * time);
        double y0 = params.yPosition1*sin(params.solutionSpeed1 * time);
        value = params.C_phi1 * (1.0 - (p[0]/params.length)*(p[0]/params.length))
                              * (1.0 - (p[1]/params.length)*(p[1]/params.length))
                              * (1.0 + time * exp(-params.growthSpeed1 * time)
                              * exp (-(p[0] - x0)*(p[0] - x0) - (p[1] - y0)*(p[1] - y0)));
    }else
      AssertThrow(false, ExcMessage("Solution Type Not Implemented"));
  }

  return value;
}

template <int dim>
double Solution2<dim>::value (const Point<dim> &p,
                              const unsigned int) const
{
  double time = this->get_time();

  double value;

  if(dim == 1){
    if(params.exact_solution_type == Parameters::AllParameters::gaussian){
      double x0 = params.xPosition2 * cos(params.solutionSpeed2 * time);
      value = params.C_phi2 * (-(p[0]/params.length)*(p[0]/params.length) + 1)
                            * exp (-(p[0] - x0)*(p[0] - x0));
    }else if(params.exact_solution_type == Parameters::AllParameters::quadratic){
      value = params.C_phi2 * (sin(params.solutionSpeed2 * time) + 1.0) * (1.0 - (p[0]/params.length)*(p[0]/params.length));
    }else if(params.exact_solution_type == Parameters::AllParameters::maxwellian){
        double x0 = params.xPosition2 * cos(params.solutionSpeed2 * time);
        value = params.C_phi2 * (1.0 - (p[0]/params.length)*(p[0]/params.length))
                              * (1.0 + time * exp(-params.growthSpeed2 * time)
                              * exp (-(p[0] - x0)*(p[0] - x0)));
    }else
      AssertThrow(false, ExcMessage("Solution Type Not Implemented"));
  }else if(dim == 2){
    if(params.exact_solution_type == Parameters::AllParameters::gaussian){
      double x0 = params.xPosition2 * cos(params.solutionSpeed2 * time);
      double y0 = params.yPosition2 * sin(params.solutionSpeed2 * time);
      value = params.C_phi2 * (-(p[0]/params.length)*(p[0]/params.length) + 1)
                            * (-(p[1]/params.length)*(p[1]/params.length) + 1)
                            * exp (-(p[0] - x0)*(p[0] - x0) - (p[1] - y0)*(p[1] - y0));
    }else if(params.exact_solution_type == Parameters::AllParameters::quadratic){
      value = params.C_phi2 * (sin(params.solutionSpeed2 * time) + 1.0) * (1.0 - (p[0]/params.length)*(p[0]/params.length)) * (1.0 - (p[1]/params.length)*(p[1]/params.length));
    }else if(params.exact_solution_type == Parameters::AllParameters::maxwellian){
        double x0 = params.xPosition2 * cos(params.solutionSpeed2 * time);
        double y0 = params.yPosition2 * sin(params.solutionSpeed2 * time);
        value = params.C_phi2 * (1.0 - (p[0]/params.length)*(p[0]/params.length))
                              * (1.0 - (p[1]/params.length)*(p[1]/params.length))
                              * (1.0 + time * exp(-params.growthSpeed2 * time)
                              * exp (-(p[0] - x0)*(p[0] - x0) - (p[1] - y0)*(p[1] - y0)));
    }else
      AssertThrow(false, ExcMessage("Solution Type Not Implemented"));
  }

  return value;
}


#endif // EXACT_SOLNS_HH
