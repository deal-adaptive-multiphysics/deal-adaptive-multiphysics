#ifndef MATERIAL_PROPERTIES_HH
#define MATERIAL_PROPERTIES_HH

using namespace dealii;

//===============================================================================
// Boundary Values
//===============================================================================

template <int dim>
class BoundaryValuesPhi : public Function<dim>
{
  public:
    BoundaryValuesPhi () : Function<dim>() {}
    virtual ~BoundaryValuesPhi () {}
    virtual double value (const Point<dim> &p,
                          const unsigned int component = 0) const;
};

template <int dim>
double BoundaryValuesPhi<dim>::value (const Point<dim> &,
                                   const unsigned int /*component*/) const
{
  return 0.0;
}

template <int dim>
class BoundaryValuesT : public Function<dim>
{
  public:
    BoundaryValuesT () : Function<dim>() {}
    virtual ~BoundaryValuesT () {}
    virtual double value (const Point<dim> &p,
                          const unsigned int component = 0) const;
};

template <int dim>
double BoundaryValuesT<dim>::value (const Point<dim> &,
                                   const unsigned int /*component*/) const
{
  return 0.0;
}

//===============================================================================
// Butcher Tableau
//===============================================================================
enum method_time {SDIRK22, SDIRK33, BE, CN, RK4};
void init_butcher_tableau(const method_time                  method,
                          unsigned int                      &n_stage,
                          std::vector<std::vector<double> > &A,
                          std::vector<double>               &B,
                          std::vector<double>               &C)
{

  if(method == SDIRK22){
    n_stage = 2;
    double gamma = 1.0 - 1.0/sqrt(2.0);
    A = std::vector<std::vector<double> > (n_stage, std::vector<double> (n_stage, 0.0));
    A[0][0] = gamma;
    A[1][0] = 1 - gamma;
    A[1][1] = gamma;
    B = std::vector<double> (n_stage, 0.0);
    B[0] = 1 - gamma;
    B[1] = gamma;
    C = std::vector<double> (n_stage, 0.0);
    C[0] = gamma;
    C[1] = 1.0;
  } else if (method == SDIRK33) {
      n_stage = 3;
      double gamma = 0.435866521508459;
      A = std::vector<std::vector<double> > (n_stage, std::vector<double> (n_stage, 0.0));
      A[0][0] = gamma;
      A[1][0] = (1.0-gamma)/2.0;
      A[1][1] = gamma;
      A[2][0] = (-6.0*gamma*gamma + 16.0*gamma -1.0)/4.0;
      A[2][1] = ( 6.0*gamma*gamma - 20.0*gamma +5.0)/4.0;
      A[2][2] = gamma;
      B = std::vector<double> (n_stage, 0.0);
      B[0] = (-6.0*gamma*gamma + 16.0*gamma -1.0)/4.0;
      B[1] = ( 6.0*gamma*gamma - 20.0*gamma +5.0)/4.0;
      B[2] = gamma;
      C = std::vector<double> (n_stage, 0.0);
      C[0] = gamma;
      C[1] = (1.0+gamma)/2.0;
      C[2] = 1.0;
  } else if (method == BE) {
      n_stage = 1;
      A = std::vector<std::vector<double> > (n_stage, std::vector<double> (n_stage, 0.0));
      A[0][0] = 1.0;
      B = std::vector<double> (n_stage, 0.0);
      B[0] = 1.0;
      C = std::vector<double> (n_stage, 0.0);
      C[0] = 1.0;
  } else if (method == CN) {
      n_stage = 2;
      A = std::vector<std::vector<double> > (n_stage, std::vector<double> (n_stage, 0.0));
      A[0][0] = 0.0;
      A[1][0] = 0.5;
      A[1][1] = 0.5;
      B = std::vector<double> (n_stage, 0.0);
      B[0] = 0.5;
      B[1] = 0.5;
      C = std::vector<double> (n_stage, 0.0);
      C[0] = 0.0;
      C[1] = 1.0;
  } else if (method == RK4) {
      AssertThrow(false, ExcMessage("RK4 Not Supported"));
      n_stage = 4;
      A = std::vector<std::vector<double> > (n_stage, std::vector<double> (n_stage, 0.0));
      A[1][0] = 0.5;
      A[2][1] = 0.5;
      A[3][2] = 1.0;
      B = std::vector<double> (n_stage, 0.0);
      B[0] = 1.0/6.0;
      B[1] = 1.0/3.0;
      B[2] = 1.0/3.0;
      B[3] = 1.0/6.0;
      C = std::vector<double> (n_stage, 0.0);
      C[1] = 0.5;
      C[2] = 0.5;
      C[3] = 1.0;
  } else {
      AssertThrow(false, ExcMessage("Time Integrator Not Supported"));
  }

}

#endif // MATERIAL_PROPERTIES_HH
